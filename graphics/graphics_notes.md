General Notes
=============

[Color Gamut](https://www.viewsonic.com/library/photography/what-is-color-gamut/)
-----------
Range of colors within a spectrum that can be reproduced on an output device.
*Ex: 16.7 million colors.*

[Variable Rate Shading](https://www.tomshardware.com/reviews/variable-rate-shading-vrs-definition-nvidia-graphics,6342.html)
------------------------
Allows the GPU to use varying amounts of processing power within the same frame
to render different parts of the image.

With VRS, a GPU can use its full shading processing power for more complex parts
of an image and less power for simpler parts of the image.

Optimization
------------
[Overdraw](https://thegamedev.guru/unity-gpu-performance/overdraw-optimization/)
> When the **same pixels are drawn more than once (in a frame)**:
**Example:**
- The first draw call comes from the street in front of you.
- The second draw shows the kids crossing the street.
- The third draw is about the fogged windshield that makes it dangerous to drive.
- And the last draw comes from the clouded glasses you’re wearing.
