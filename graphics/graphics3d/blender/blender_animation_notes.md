Blender Animation
=================
- `<Alt>` + `r` to reset pose rotation

Humanoid Animations
-------------------
- [Idle Animation](https://youtu.be/L3FJVQbnllc?t=548)
- [Walking Animation](https://www.youtube.com/watch?v=gFf5eGCjUUg)
- [Running Animation](https://www.youtube.com/watch?v=L3FJVQbnllc)
