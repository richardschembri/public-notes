Blender
=======

Shortcuts
---------
### Basics
| Function                | Shortcut               |
| :---------------------- | :--------------------- |
| **Select**              | `<Right Click>`        |
| **Pan**                 | `<Middle click>`       |
| **Zoom**                | `<Mouse wheel>`        |
| **Add object**          | `<Shift>`+`a`          |
| **Delete**              | `x`                    |
| **Search for function** | `<Spacebar>`           |
| **Toolbar**             | `t`                    |
| **Properties**          | `n`                    |
| **Save file**           | `<Ctrl>`+`s`           |
| **Render image**        | `<F12>`                |
| **Render animation**    | `<Ctrl>`+`<F12>`       |
| **Stop render**         | `<Esc>`                |
| **Save render**         | `<F3>`                 |
| **Show last render**    | `<F11>`                |
| **Undo**                | `<Ctrl>`+`z`           |
| **Redo**                | `<Ctrl>`+`<Shift>`+`z` |

### General
| Function               | Shortcut                       |
| :--------------------- | :----------------------------- |
| **Duplicate**          | `<Shift>` `d`                  |
| **Move to layer**      | `m`                            |
| **Mirror**             | `<Ctrl>`+`m`                   |
| **Hide**               | `h`                            |
| **Unhide**             | `<Alt>`+`h`                    |
| **Move origin point**  | `<Ctrl>`+`<Shift>`+`<Alt>`+`c` |
| **Parent to**          | `<Ctrl>`+`p`                   |
| **Clear parent**       | `<Alt>`+`p`                    |
| **Track to**           | `<Ctrl>`+`t`                   |
| **Clear track**        | `<Alt>`+`t`                    |
| **Reset 3D cursor**    | `<Shift>`+`c`                  |
| **Turn widget on/off** | `<Ctrl>`+`<Spacebar>`          |
| **Add to group**       | `<Ctrl>`+`g`                   |
 	
### Movements
| Function               | Shortcut                           |
| :--------------------- | :--------------------------------- |
| **Move**               | `g`                                |
| **Rotate**             | `r`                                |
| **Scale**              | `s`                                |
| **Precise movement**   | `<Shift>`+`hold`                   |
| **Increment movement** | `<Ctrl>`+`hold`                    |
| **Lock to axis**       | `<Middle click>` / `x` / `y` / `z` |
 	
### Navigation
| Function               | Shortcut                           |
| :--------------------- | :--------------------------------- |
| **Top view**           | `Numpad 7`                         |
| **Front view**         | `Numpad 1`                         |
| **Side view**          | `Numpad 3`                         |
| **Camera view**        | `0`                                |
| **Zoom to object**     | `Numpad .`                         |
| **Fly mode**           | `<Shift>`+`f`                      |
 	
### Selection
| Function                 | Shortcut                           |
| :---------------------   | :--------------------------------- |
| **Select object**        | `<Right click>`                    |
| **Select multiple**      | `<Shift>`+`<Right click>`          |
| **(De-)Select all**      | `a`                                |
| **Select object behind** | `<Alt>`+`<Right click>`           |
| **Select linked**        | `l`                               |
| **Select all linked**    | `<Ctrl>`+`l`                      |
| **Box select**           | `b`                               |
| **Circle select**        | `c`                               |
| **Lasso tool**           | `<Ctrl>`+`<Click>`                |
| **Inverse selection**    | `<Ctrl>`+`i`                       |

### Fly mode
| Function               | Shortcut                           |
| :--------------------- | :--------------------------------- |
| **Start fly mode**     | `<Shift>`+`f`                      |
| **Accelerate**         | `<Mouse wheel up>`                 |
| **Decelerate**         | `<Mouse wheel down>`               |
| **Pan**                | `<Middle click>`                   |
| **Fly forward**        | `w`                                |
| **Fly backwards**      | `s`                                |
| **Fly left**           | `a`                                |
| **Fly right**          | `d`                                |
| **Fly up**             | `r`                                |
| **Fly down**           | `f`                                |

### Switching modes
| Function                  | Shortcut                           |
| :---------------------    | :--------------------------------- |
| **Edit/Object mode**      | `<Tab>`                            |
| **Vertex paint mode**     | `v`                                |
| **Weight paint mode**     | `<Ctrl>`+`<Tab`                    |
| **Switching workspace**   | `<Ctrl>`+(`←`/`→`)                 |
| **Logic editor**          | `<Shift>`+`<F2>`                   |
| **Node editor**           | `<Shift>`+`<F3>`                   |
| **Console**               | `<Shift>`+`<F4>`                   |
| **3D viewport**           | `<Shift>`+`<F5>`                   |
| **F-curve editor**        | `<Shift>`+`<F6>`                   |
| **Properties**            | `<Shift>`+`<F7>`                   |
| **Video sequence editor** | `<Shift>`+`<F8>`                   |
| **Outliner**              | `<Shift>`+`<F9>`                   |
| **UV/Image editor**       | `<Shift>`+`<F10>`                  |
| **Text editor**           | `<Shift>`+`<F11>`                  |

#### [Weight Paint Mode](https://www.youtube.com/watch?v=za7BDv0-QsQ)
> This is used to see what functionality affects which vertices, like assigning bones or
> assigning a modifier

| Function                     | Shortcut                           |
| :---------------------       | :--------------------------------- |
| **Adjust Weight**            | `<Ctrl>`+`f`                       |
| **Adjust Strength**          | `<Shift>`+`<Tab`                   |
| **Adjust values dropdown**   | `<Right click>`                    |

##### Armature Weight Paint
1. Enter `Object Mode`
2. Select Model
3. Select Armature
4. Weight Mode is now available

### Modeling
| Function                  | Shortcut                            |
| :---------------------    | :---------------------------------  |
| **Make face**             | `f`                                 |
| **Subdivide**             | `w`                                 |
| **Extrude**               | `e`                                 |
| **Separate**              | `p`                                 |
| **Rip**                   | `v`                                 |
| **Create loop cut**       | `<Ctrl>`+`r`                        |
| **Propositional editing** | `o`                                 |
| **Select edge loop**      | `<Alt>`+`<Right click>`             |
| **Make seam/sharp**       | `<Ctrl>`+`e`                        |
| **Merge vertices**        | `<Alt>`+`m`                         |
| **Mirror**                | `<Ctrl>`+`m`                        |
| **Shrink/Flatten**        | `<Alt>`+`s`                         |
| **Knife**                 | `k` then `<Click>`                  |
| **Fill**                  | `<Alt>`+`f`                         |
| **Beauty fill**           | `<Shift>`+`<Alt>`+`f`               |
| **Add subdivision level** | `<Ctrl>`+`( `1` / `2` / `3` / `4` ) |
| **Rip vertices and fill** | `<Alt>`+`v`                         |

### Editing curves
| Function                    | Shortcut                           |
| :---------------------      | :--------------------------------- |
| **Close path**              | `<Alt>`+`c`                        |
| **Add handle**              | `<Ctrl>`+`<Click>`                 |
| **Subdivide**               | `w`                                |
| **Tilt**                    | `<Ctrl>`+`t`                       |
| **Clear tilt**              | `<Alt>`+`t`                        |
| **Change handle to bezier** | `h`                                |
| **Change handle to vector** | `v`                                |
| **Reset to default handle** | `<Shift>`+`h`                      |
 	

### Sculpting
| Function                  | Shortcut                           |
| :---------------------    | :--------------------------------- |
| **Change brush size**     | `f`                                |
| **Change brush strength** | `<Shift>`+`f`                      |
| **Rotate brush texture**  | `<Ctrl>`+`f`                       |

### Animation
| Function                      | Shortcut                           |
| :---------------------        | :--------------------------------- |
| **Play/Stop animation**       | `<Alt>`+`a`                        |
| **Play animation in reverse** | `<Alt>`+`<Shift>`+`a`              |
| **Next frame**                | `→`                                |
| **Previous frame**            | `←`                                |
| **Forward 10 frames**         | `↑`                                |
| **Back 10 frames**            | `↓`                                |
| **Jump to start point**       | `<Shift>`+`←`                      |
| **Jump to end point**         | `<Shift>`+`→`                      |
| **Scroll through frames**     | `<Alt>`+`<Mouse wheel>`            |
| **Insert keyframe**           | `i`                                |
| **Remove keyframe**           | `<Alt>`+`<i>`                      |
| **Jump to next keyframe**     | `<Ctrl>`+`<Page up>`               |
| **Jump to previous keyframe** | `<Ctrl>`+`<Page down>`             |
| **Paste mirrored frames**     | `<Shift>`+`<Ctrl>`+`v`             |

### Node editor
| Function                | Shortcut                           |
| :---------------------  | :--------------------------------- |
| **Add node**            | `<Shift>`+`a`                      |
| **Cut links**           | `<Ctrl>`+`<Left mouse>`            |
| **(Un-)Hide node**      | `h`                                |
| **Make group**          | `<Ctrl>`+`g`                       |
| **Ungroup**             | `<Alt>`+`g`                        |
| **Edit group**          | `<Tab>`                            |
| **Move background**     | `<Alt>`+`<Middle mouse>`           |
| **Zoom in background**  | `v`                                |
| **Zoom out background** | `<Alt>`+`v`                        |
| **Properties**          | `n`                                |

### Armatures
| Function                  | Shortcut                           |
| :---------------------    | :--------------------------------- |
| **Add bone**              | `e` or `<Ctrl>`+`<Click>`          |
| **Rotate**                | `<Ctrl>`+`r`                       |
| **Recalculate roll**      | `<Ctrl>`+`n`                       |
| **Align bones**           | `<Ctrl>`+`<Alt>`+`a`               |
| **Move to bone layers**   | `m`                                |
| **View bone layers**      | `<Shift>`+`m`                      |
| **Set bone flag**         | `<Shift>`+`w`                      |
| **Switch bone direction** | `<Alt>`+`f`                        |
| **Scroll hierarchy**      | `]` / `[`                          |
| **Select hierarchy**      | `<Shift>`+( `]` / `[` )            |
| **Select connected**      | `l`                                |

### Pose mode
| Function                | Shortcut                           |
| :---------------------  | :--------------------------------- |
| **Apply pose**          | `<Ctrl>`+`a`                       |
| **Clear pose rotation** | `<Alt>`+`r`                        |
| **Clear pose location** | `<Alt>`+`l`                        |
| **Clear pose scale**    | `<Alt>`+`s`                        |
| **Copy pose**           | `<Ctrl>`+`c`                       |
| **Paste pose**          | `<Ctrl>`+`v`                       |
| **Add IK**              | `<Shift>`+`i`                      |
| **Remove IK**           | `<Ctrl>`+`<Alt>`+`i`               |
| **Add to bone group**   | `<Ctrl>`+`g`                       |
| **Relax pose**          | `<Alt>`+`e`                        |

### Timeline
| Function                  | Shortcut                           |
| :---------------------    | :--------------------------------- |
| **Set start frame**       | `s`                                |
| **Set end frame**         | `e`                                |
| **Show all frames**       | `<Home>`                           |
| **Add marker**            | `m`                                |
| **Move marker**           | `<Right click>`+`{drag}`           |
| **Toggle frames/seconds** | `<Ctrl>`+`t`                       |
 	
### Video Sequence Editor
| Function                   | Shortcut                           |
| :---------------------     | :--------------------------------- |
| **Next strip**             | `<Page up>`                        |
| **Previous strip**         | `<Page down>`                      |
| **Split strip**            | `k`                                |
| **Lock strip**             | `<Shift>`+`l`                      |
| **Unlock strip**           | `<Shift>`+`<Alt>`+`l`              |
| **Copy strip**             | `<Ctrl>`+`c`                       |
| **Paste strip**            | `<Ctrl>`+`v`                       |
| **Separate images**        | `y`                                |
| **Snap strip to scrubber** | `<Shift>`+`s`                      |

### Advanced
| Function                    | Shortcut                           |
| :---------------------      | :--------------------------------- |
| **Append file**             | `<Shift>`+`<F1>`                   |
| **Full screen mode**        | `<Alt>`+`<F11>`                    |
| **Maximize sub window**     | `<Ctrl>`+`↑`                       |
| **Change active camera**    | `<Ctrl>`+`o`                       |
| **Use render buffer**       | `j`                                |
| **Only render selected**    | `w`                                |
| **Only render portion**     | `<Shift>`+`b`                      |
| **Save over default scene** | `<Ctrl>`+`u`                       |
| **Make screen cast**        | `<Ctrl>`+`<F4>`                    |

--------------------------------------------------------------------------------

Import Reference Image
----------------------
> Doesn't work with gifs!

1. `<shift>`+`a` to bring up the **Add Menu**
2. Image -> Reference
3. It is a good idea to put in a new collection "Background" by pressing `M`

Tools
-----
#### Modifiers
##### Mirror
- **Clipping** - both halfs are merge when colliding to eachother
