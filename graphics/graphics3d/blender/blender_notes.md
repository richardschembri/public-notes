Blender
=======

Vertex groups
-------------
- Found under `Object Properties` *(Green Triangle)*

[Weight Painting](https://www.youtube.com/watch?v=za7BDv0-QsQ)
----------------------------------------------------------
- A visual way to assign `vertex groups`
- This is used to see what functionality affects which vertices (Ex. Rigging, Modifiers, etc)
- Best use solid shading mode to view paint better	
- You can **Add** and **Substract** weights
- All changes are **real time**

Modeling
--------
### Clothing
- [Regular Clothes](https://www.youtube.com/watch?v=B_6xXNTB7dc)
- [Armour](https://www.youtube.com/watch?v=rzau6SFcNOk)
- [Weapons](https://www.youtube.com/watch?v=01gbRTk1Nbs)
