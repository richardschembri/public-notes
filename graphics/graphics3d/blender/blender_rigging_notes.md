Blender Rigging
===============
> Refer to [[blender_notes|Blender]] for short cuts and mode switching

Parent Armature to Model
------------------------
1. Select **Model**
2. Select **Armature**
3. Right Click or `<Ctrl>`+`p`
4. Armature Deform -> With Automatic Weights

Rigify
------
### Enable Rigify
1. Edit -> Prefrences -> Add Ons
2. Search for `Rigify`
3. Tick option

Weight Painting
---------------
> Used to help bind `bones` to `vector`s

### Key bindings
| Function               | Shortcut                       |
| :--------------------- | :----------------------------- |
| **Select Bone**        | `<Ctrl>`+`<Right click>`       |

Guides / Tutorials
------------------
[Rigging a Low Poly Person](https://youtu.be/srpOeu9UUBU)
