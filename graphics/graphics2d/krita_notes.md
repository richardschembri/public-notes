Krita Notes
===========

Quick Notes
-----------

| Action                  | Method                               |
| ----------------------- | :----------------------------------- |
| **Increase icon size**  | `right-click` on toolbar             |
| **Color Palette**       | Settings -> Dockers -> Color Palette |
| **Draw Shapes/Vectors** | Create vector layer                  |
| **Vectors Fill/Stroke** | Tool Options (On upper right side)   |

