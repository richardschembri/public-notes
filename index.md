# Table of contents

## ⌨️ Command Line

- [General terminal](cli_notes/terminal_notes)
- [Tmux (Terminal multiplexer)](cli_notes/tmux_notes)
- [fzf](cli_notes/fzf_notes)
- [rsync](cli_notes/rsync_notes)
- [curl](cli_notes/curl_notes)
- [WeeChat](cli_notes/weechat_notes)
- [ImageMagick](cli_notes/imagemagick_notes)
- [Grep](cli_notes/grep_notes)
- [Key Binds/Key Input](cli_notes/keybind_notes)
- [xrandr](cli_notes/xrandr_notes)

### 🔨 Productivity

- [Caldav Notes](cli_notes/caldav_notes)
- [⚔ Taskwarrior](cli_notes/taskwarrior_notes)
- [Taskell Notes](cli_notes/taskell_notes)
- [Graphviz(dot) notes](cli_notes/graphviz_notes)

#### 💸 Finance

- [A guide on how to use Ledger](cli_notes/ledger/ledger_guide)
- [Ledger notes](cli_notes/ledger/ledger_notes)
- [Stock Trading](finance/stock_trading_notes)

### Networking Tools

- [A list of alternatives to depracted networktools](cli_notes/networking/depracted-net-tools)
- [ss](cli_notes/networking/ss)
- [arp-scan(list all IPs in LAN)](cli_notes/networking/arp-scan)

### 🐧 OS Specific

- [Archlinux](cli_notes/archlinux_notes)
- [OSX](cli_notes/osx_notes)
- [OpenSUSE](cli_notes/opensuse_notes)
- [Btrfs Snapper](cli_notes/snapper_notes)
- [Windows Subsystem for Linux (WSL)](cli_notes/windows/wsl_notes)

#### System Config

- [🤘 Guix](sys_config/guix_notes)

### ✂️ Office

- [Vim](cli_notes/vim_notes)
- [Helix](cli_notes/helix_notes)
- [Pandoc (Document convertor)](cli_notes/pandoc_notes)
- [scim (Spreadsheet TUI)](cli_notes/scim_notes)
- [Markdown](cli_notes/markdown_notes)

#### 📔LaTeX

- [LaTeX (Document generation) notes](cli_notes/latex_notes/latex_notes.md)

### 🗄File Management

- [Vifm (TUI File manager with vim bindings)](cli_notes/vifm_notes)

### Development

- [Openstf (Open Smartphone Test Farm)](cli_notes/openstf)
- [Git notes](cli_notes/git_notes)

### DevOps

#### Automated testing

- [Jupyter Notebook](cli_notes/jupyter_notes)

#### Containerization

- [Docker](cli_notes/containerization/docker_notes)
- [Podman](cli_notes/containerization/podman_notes)

---

## 💾 Programming

> [Quick Reference](https://learnxinyminutes.com/)

```sh
curl cheat.sh/<lang>/:learn
```

- [Programming Terminology](programming_notes/programming_terminology)

- [Design Patterns](programming_notes/designpattern_notes)
- [Dependency Injection](programming_notes/design_patterns/dependency_injection)

### Data Structures and Algorithms

- [Data Structures and Algorithms Basics](programming_notes/DSA_notes/DSA_basics)
- [Algorithmic Performance](programming_notes/DSA_notes/algorithmic_performance)

#### Algorithms

- [Adjacency list algorithm](programming_notes/DSA_notes/algorithm_adjacencylist)
- [Minmax algorithm](programming_notes/DSA_notes/algorithm_minmax)
- [Graph algorithms](programming_notes/DSA_notes/algorithm_graph)

#### Data Structures

- [Adjacency list](programming_notes/DSA_notes/datastructure_adjacencylist)

### 📚 Database

- [MySQL](database/mysql_notes)
- [SQL](programming_notes/sql_notes)

### 📺 Graphics

- [Graphics General](programming_notes/graphics/graphics_notes)
- [Render Pipeline](programming_notes/graphics/renderpipeline_notes)

### 🐢 Source control

- [Perforce](programming_notes/source_control/perforce_notes)

### 🗨️ Languages

#### Compiled

- [C#](programming_notes/csharp_notes)
- [C](programming_notes/clang/clang_notes)
  - [OpenGL](programming_notes/clang/opengl_notes)
- [C++](programming_notes/cpp_notes)
- [Haskell](programming_notes/haskell_notes)
- [Rust](programming_notes/rust_notes)

#### Markup

- [HTML5](programming_notes/html5_notes)
- [TOML](programming_notes/toml_notes)

#### 📜 Scripting

- [BASH](programming_notes/shell/bash_notes)
- [ZSH](programming_notes/shell/zsh_notes)
- [nushell](programming_notes/shell/nushell_notes)
- [Javascript](programming_notes/javascript_notes)
- [Lua](programming_notes/lua_notes)
- [Python](programming_notes/python_notes)
- [PHP](programming_notes/php/php_notes)
  - [PHP Debugging](programming_notes/php/debugging)
  - [Laravel](programming_notes/php/laravel_notes)

### 📞 Phone

- [Android](programming_notes/android_notes)
- [iOS](programming_notes/ios_notes)
- [Flutter](programming_notes/flutter/flutter_notes)

### 🌎 Web Services

#### Automation

- [IFTTT](programming_notes/web_services/automation/ifttt_notes)
- [GAS(Google Apps Script)](programming_notes/web_services/automation/gas_notes)
- [Zapier](programming_notes/web_services/automation/zapier_notes)

##### Self Hosted

- [Node-RED](cli_notes/node-red)

### 🕹️ Game Engines

- [Vulkan](programming_notes/vulkan_notes)
- [USD(Universal Scene Description)](programming_notes/usd_notes)

#### Unity 3D

- [Unity Engine](programming_notes/unity/unity_notes)
- [Unity Shader](programming_notes/unity/unity_shader_notes)
- [Unity Maths](programming_notes/unity/unity_maths_notes)
- [Unity Editor](programming_notes/unity/unity_editor)
- [Unity Linux](programming_notes/unity/unity_linux_notes)
- [Unity AI Tools](programming_notes/unity/unity_aitools_notes)
- [Unity XR](programming_notes/unity/unity_xr_notes)
- [Unity Vim](programming_notes/unity/unity_vim)
- [Unity USD(Universal Scene Description)](programming_notes/unity/unity_usd_notes)

#### Unreal

- [Unreal](programming_notes/unreal/unreal_notes)
- [Unreal UI](programming_notes/unreal/unreal_ui_notes)
- [Unreal Wii](programming_notes/unreal/unreal_wii_notes)
- [Unreal USD(Universal Scene Description)](programming_notes/unreal/unreal_usd_notes)

#### 🤖Godot

- [Godot](programming_notes/godot/godot_notes)
- [Godot Sprite Animation](programming_notes/godot/godot_spriteanimation_notes)

### 🥽 XR: AR/VR/MR

- [AR/VR/MR General Notes](programming_notes/AR-VR-MR_notes)
- [XR UI Notes](programming_notes/XRUI_notes)
- [XR Ergonomics](xr_notes/xr_ergonomics_notes)
- [XR DevTools](xr_notes/xr_devtools_notes)

#### Magic Leap

- [Magic Leap](programming_notes/magic_leap/magic_leap_notes)

### 🪙Crypto

#### Ethereum

- [Crypto Gas](programming_notes/dapp_notes/ethereum_notes/gas_notes)
- [Solidity](programming_notes/dapp_notes/ethereum_notes/solidity_notes)
-

### Machine Learning

- [ML Basics](programming_notes/machine_learning/mlbasics_notes)

### Services

- [Github](programming_notes/services_notes/github_notes)

### ⌚Smartwatch

- [Pebble](programming_notes/smartwatch_notes/pebble_notes)

---

## 📺 Graphics

- [General](graphics/graphics_notes)

### 2D

- [Krita](graphics/graphics2d/krita_notes)

### 3D

- [Resources](graphics/graphics3d/resources)

#### Blender

- [Blender](graphics/graphics3d/blender/blender_notes)
- [Blender Cheatsheet](graphics/graphics3d/blender/blender_cheathseet)
- [Blender Rigging](graphics/graphics3d/blender/blender_rigging_notes)
- [Blender Animation](graphics/graphics3d/blender/blender_animation_notes)

---

## 🌐 Networking

- [Networking](networking/networking_general)
- [SSH server setup](networking/ssh_server_setup)
- [Bitlbee](networking/irc/bitlbee)

---

## 🧮 Maths

- [Mathematical Functions](maths/math_functions)
- [ Vector Maths](maths/math_vector.md)

---

## 🪟 Desktop Enviroment

### Window Managers

- [Awesome WM](de/awesomewm)

---

## 🌐 Self Hosting

- [Nextcloud](self_hosting/nextcloud_notes)

### Project Management

- [Redmine](self_hosting/project_management/redmine_notes)

---

## ☁️ Cloud Services

### Amazon

- [AWS](cloud-services/amazon/aws)

### Google

- [Google Smarthome](cloud-services/google/actions/smarthome)

---

## 🔑 Security

### Applications

- [KeePass](security/keepass)

---

## 📷 Photography

- [General](photography/photography_notes)

---

## 🗨️ Language

### 🇯🇵 Japanese

- [Nutrition](language/ja/ja_nutrition)
- [Learning](language/ja/ja_learning)

---

## 🖱️ Hardware

- [📺 Monitors](hardware/monitor_notes)
- [⌨️ Keyboards](hardware/keyboard_notes)

---

## 👕 Clothing

- [🪖 Protective Clothing](clothing/clothing_protective_notes.md)
- [☀️ Summer Clothing](clothing/clothing_summer_notes.md)

---

## 🏎️ Driving

- [🏎️ Driving Notes](/driving/driving_notes.md)
