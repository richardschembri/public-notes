# 🤘 Guix Notes

https://systemcrafters.net/craft-your-system-with-guix/introduction/
https://github.com/engstrand-config/guix-dotfiles/tree/main/engstrand
https://boilingsteam.com/i-love-arch-but-gnu-guix-is-my-new-distro/index.html
https://forum.systemcrafters.net/t/guix-on-steam-deck/520

https://gist.github.com/peanutbutterandcrackers/844c211a91137c19607ae75b59fa116f

- Guix installs packages and updates in a “transactional” way, meaning that the
  changes aren’t applied to your system until installation completes successfully.

## 🔍 Find Packages

- [Toys](https://toys.whereis.みんな)

## 📄 Reference dotfiles

- [dustinlyons](https://github.com/dustinlyons/guix-config/tree/main)
- [mrh](https://codeberg.org/mrh/dotfiles/src/branch/trunk)

## 🌐 Foreign Distro

Installing guix as a package manager on a non guix distro (ex: openSUSE)

- [Getting Started (Covers foreign distro aswell)](https://guix.gnu.org/manual/en/html_node/Getting-Started.html)

### 📦 Installation

#### 1. Make installed apps main:

```sh
GUIX_PROFILE="$HOME/.config/.guix-profile" && source "$GUIX_PROFILE/etc/profile"
```

#### 2. Update Repos:

```sh
guix pull
```

#### 3. Upgrade Packages:

```sh
guix upgrade
```

#### 4. Update root user because of daemons:

```sh
sudo -i guix pull
```

Rolling back latest change/operation (example an install or upgrade)

```sh
guix package --roll-back
```

```sh
guix package --switch-generation=42
```

Avoid Package Compilation by checking if there are builds for latest source code

```sh
guix weather ungoogled-chromium
```

--------------------------------------------------------------------------------

## 📺 Channels

Tell guix where to find the packages

Check channel configuraiton

```sh
guix describe
```

Use custom list of channels

```sh
guix pull --channels=$HOME/.config/guix/my-channels.scm
guix describe --format=channels > ~/.config/guix/channels.scm
```

--------------------------------------------------------------------------------

## [🚩 Foriegn Binaries](https://zie87.github.io/posts/guix-foreign-binaries/)

### patch-elf

- Modifies ELF executables dynamic linker and `RPATH`

#### Example
The below `ld-linux` should point to `/gnu/store` so is incorrect
```sh
> patchelf -- patchelf --print-interpreter ./yazi-x86_64-unknown-linux-gnu/yazi
/lib64/ld-linux-x86-64.so.2
```

Locate where it really easy
```sh
> guix locate -u ld-linux-x86-64.so.2
guix locate: traversing local profile manifests...
indexing 907 packages
gcc-toolchain@11.4.0 /gnu/store/01h765yncwxm4rnn720fd1dg2cqla85l-gcc-toolchain-11.4.0/lib/ld-linux-x86-64.so.2
gcc-toolchain@14.2.0 /gnu/store/9p1z62lff9czkr95n3sal97rjw5lm7wl-gcc-toolchain-14.2.0/lib/ld-linux-x86-64.so.2
fhs-union-64@0.0     /gnu/store/nfw16yr04cw7y5prqbbvwhnzsd6psgw7-fhs-union-64-0.0/lib/ld-linux-x86-64.so.2
```

------------


/run/current-system/profile/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
