# 🐧 Linux Notes

## Executable and Linked Format (ELF)
- a format used for storing binaries, libraries, and core dumps on disks in Linux
	and Unix-based systems.

### rpath
Add a directory to the runtime library search path. This is used when linking an
**ELF** executable with shared objects. All `-rpath` arguments are concatenated
and passed to the runtime linker, which uses them to locate shared objects at runtime.
