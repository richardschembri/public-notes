Trigonometry
============
Angles
------
- **Acute**: `< 90°`
- **Obtuse**: `> 90°`
- **Perpendicular**: `= 90°`

Triangles
---------
- **Scalene**: **No** sides are the same length
- **Isosceles**: **Two** sides are the same length
- **Right angle**: One interior angle is 90°
> Sum(angles) = 180°

### Right Angle Triangles
- **hypotenuse**: Longest side (opposite of right angle)
- **adjacent**: Adjacent to the angle you want to find
- **opposite** : Opposite to the angle you want to find

sin(θ) = opposite/hypotenuse => θ = sin⁻¹(opposite/hypotenuse = asin(opposite/hypotenuse
cos(θ) = adjacent/hypotenuse => θ = cos⁻¹(adjacent/hypotenuse) = acos(adjacent/hypotenuse)
tan(θ) = opposite/adjacent => θ = tan⁻¹(opposite/adjacent) = atan(opposite/adjacent) 
> where θ is the angle in radians
> radian to degrees = val * 180/PI
