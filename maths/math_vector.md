 Vector Maths
============

Geometric Vectors
-----------------
- **magnitude**: vector length
- **direction**: 
- **unit vector**/**normalized vector**(**v-hat**): vector of length 1
- **zero vector**: vector with magnitude of 0
- A vector does not have position/location in space:
  Two vectors with the same magnitude and direction are equal **no matter*
  where they are drawn.
- In games **vector are used** for:
  - Represent **Direction**
  - Represent **Change** *ex: moving through space*
    - Vectors can be used to represent change to other Vectors 
    *ex: Add a second vector to a `velocity` vector as `acceleration`*

### Vector Arithmetic Operations
#### `+` `-` Addition and Subtraction
If we think of a vector `u` as an agent that changes position 
then `u = v + w` combines the position change effect of `v` and `w`
#### `*` Scalar multiplication
- Changes the length of a **vector** by multiplying it by a single **real**(scalar) value
- Multiplying a **vector** by 2, makes it twice as long
- Multiplying a **vector** by a negative value changes the length and points the vector in the
  opposite direction

Real Vector Spaces
------------------
- **linear space** or **vector space**
- A collection of vectors that meet the rules for **addition** and **scalar multiplication**
- *real vector spaces* drawn from `R`(The set of real numbers) 
- **real venguctor space** set `V` is over `R`
- Scalar => Element of `R`
- **closure*
  - **additive closure**: For any `u` and `v` in `V`, `u + v` is in `V`
  - **multiplicative closure**: For any `a` in `R` and `v` in `V`, `av` is in `V`
- $R^2$: All ordered pairs of real numbers
  - Informally represents 2D vector space: $R^2$ = {(x,y)|x,y ∈ R}
  - ∈: Is a member of
  - (1.0, -0.5) is a different member of the set from (-0.5, 1.0)
  - Define **addition**: (x0, y0) + (x1, y1) = (x0 + x1, y0 + y1)
  - Define **scalar multiplication**: a(x0, y0) = (ax0, ay0)
- $R^4$: Represents 4D vector space which can be useful for certain computer graphics concepts
### Subspaces
- Subspace most contain **0**
  - A subspace of $R^3$:
    - (x0, y0, 0) + (x1, y1, 0) = (x0 + x1, y0 + y1, 0)    
    - a(x0, y0, 0) = (ax0, ay0, 0)
    - $R^2$ can be embedded in $R^3$: (x,y) -> (x, y, 0)

Linear Combinations and Basic Vectors
-------------------------------------

