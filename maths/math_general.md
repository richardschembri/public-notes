Maths General
=============

Bitwise operations
------------------

### Bit Masks
A & MASK == MASK 

### Bit Shift for Bit Packing
Shift bits to fit into 1 variable

Real Numbers
------------
###  Fixed-Point numbers
#### Radix Point: 
- A binary value point (like decimal point) 
  - A decimal number has **radix 10**
  - A binary number has **radix 2** 
  - It is always **fixed**

Fixed point is used for scaling

#### A-dot-B
- The standard nomenclature of fixed point format is A-dot-B
- **A**: Number of integral bits (left of the radix point)
- **B**: Number of fractionl bits (to the right of the radix point)
- The scaling factor is $2^-B$
- Regular **32 bit** integers could be referred to **32-dot-0** with scaling factor of $2^0$ = 1


### Approximations
#### Example
`m` *bits* represent the **integer** value and `n` *bits* represent the **fractional** value.
The **smallest** possible fraction is 1/$2^n$ and the **largest** is 2/$2^n$. We **can't**
represent 3/$2^n+1$ it falls between the two values so we'll either **round up** or **round down**

#### Precision and Error
Imagine function `Rep(A)` is  a function that is the closes representation of `A`
`Rep(A)` != `A`
`Rep(A)` - `A` != 0

##### Absolute Error
By making `|Rep(A) - A|` we can *measure* the **error** in the **approximation** 
`AbsError = |Rep(A) - A|`

It **doesn't quantify** the scale at which the error affects computation
A system of measurement in `km` accurate enough for distance between earth
and sun (**149,597,871km**) but not an apple (**0.00011km**) which would be 
rounded to **0km**.

##### Relative Error
Takes the scale of the value into account
`RelError = |(Rep(A) - A) / A|`
 
`RelErrorSun = |1km / 149,597,871km| ≈ 7 * $10^-9$`
`RelErrorApple = |0.00011km / 0.00011km| = 1.0`

**Example of 4-dot-4**
Representable values are separated by a **step size** of `1/16`
No more than 1/32 from a representable value.
`0 ≤ |Rep(A) - A| ≤ 1/32`
*Relative Error* is not well bounded
`0 ≤ |(Rep(A) - A)/A| ≤ 1/32|A|`
*Relative Error* grows in inverse proportion to the **magnitude** of *A*.
- *Absolute Error* is **bounded**
- *Relative Error* can **vary greatly**

###  Floating-Point Numbers
#### Scientific Notation
1. A decimal number, called the **mantissa**:
    `1.0 ≤ |mantissa| < 10.0`
2. An integer, called the **exponent**

The **exponent** and **mantissa** create the number:
`mantissa * $10^exponent$`

| Incorrect     | Correct       |
| :------------- |:-------------- |
| 11.02 * $10^3$ | 1.102 * $10^4$ |
| 0.92 * $10^-2$ | 9.2 * $10^-3$  |

####  Restricted Scientific Notation (base-10)
- **mantissa** must be written with: 
  - A single nonzero integer digit.
  - A fixed number of fractional digits (`M` digits)
- **exponent** must be written with fixed number of digits (`E` digits) 
- **mantissa** and **exponent** each have individual signs
##### Example
M =3, E = 2
`±|1.1|2|3|*$10^±|1|2|$`  
Any value can be represented by uniquely by six decimal digits and two signs
```
(exponents) * (mantissas) * (exponent signs) * (mantissa signs)
= ($10^2$) * (9 * $10^3$) * (2) * (2)
= 3,600,000
```
**mantissas** leading digit must be nonzero [1,9]
 `9 * 10 * 10 * 10 = 9,000 possible mantissas`
**Minimum** and **Maximum** *exponents*:
   `±($10^E$ - 1) = ±($10^2$ - 1) = ±99`
The *largest* **mantissa** value is:
   `10.0 - ($10^-M$) = 10.0 - ($10^-3$) = 10.0 - 0.001 = 9.999`
The *smallest* allowed nonzero value is 1.000 due to the requirement for normalization

- **Maximum** representable value: `9.999 * $10^99$`
- **Maximum** representable value: `-9.999 * $10^99$`
- **Smallest** positive value: `1.000 * $10^-99$`

#### Binary Scientifi Notation (base-2)
**Restricted Scientific Notation:**
`SignM * mantissa * $2^SignE*exponent$`
- **exponent**: *E-bit* integer
- **mantissa sign**: SignM
- **exponent sign**: SignE
**Mantissa** is a 1-dot-M fixed-point number whose most significant is always 1:
  `1.0 ≤ Mantissa ≤ (2.0 - 1/$2^M$)`
