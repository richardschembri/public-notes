# if statement
``` php
<?php
if ($a > $b) {
    echo "a is bigger than b";
} elseif ($a == $b) {
    echo "a is equal to b";
} else {
    echo "a is smaller than b";
}
?>
```

# switch statement
``` php
switch ($i) {
    case 0:
        echo "i equals 0";
        break;
    case 1:
        echo "i equals 1";
        break;
    case 2:
        echo "i equals 2";
        break;
}
```

## Shorthand
``` php
ps_admin = ($user['permissions'] == 'admin') ? true : false;
```

# for loop
``` php
for ($x = 0; $x <= 10; $x++) {
    echo "The number is: $x <br>";
}
```

# NULL or empty

|	|empty(==null)  | is\_null(===null)  | isset | array\_key\_exists |
| :---- |:-------------:| :-----------------:| :----:| :-----------------:|
| ϕ 	| T		| T		     | F     | F 		  |
| null	| T		| T		     | F     | T 		  |
| ""  	| T		| F		     | T     | T 		  |
| []  	| T		| F		     | T     | T 		  |
| 0  	| T		| F		     | T     | T  		  |
| false	| T		| F		     | T     | T 		  |
| true	| F		| F		     | T     | T 		  |
| 1  	| F		| F		     | T     | T  		  |
| ¥0  	| F		| F		     | T     | T 		  |

# String
## replace
``` php
str_replace(find,replace,string,count);
```
_Example:_
``` php
echo str_replace("world","Peter","Hello world!");
```

# Methods
## parameter by reference
_Example:_
``` php
function foo(&$var)
{
    $var++;
}

$a=5;
foo($a);
// $a is 6 here
```

# Arrays
## remove from array // unset
``` php
$array = [0 => "a", 1 => "b", 2 => "c"];
unset($array[1]); // [0 => "a", 2 => "c"]
```

## count / length / size
``` php
count ( mixed $array_or_countable [, int $mode = COUNT_NORMAL ] ) : int
array_key_exists ( mixed $key , array $array )
```
## array\_key\_exists
``` php
array_key_exists ( mixed $key , array $array )
```
_Example :_
``` php
array_key_exists('first', $search_array);
```
## array\_column
> Return the values from a single column in the input array
```
array_column ( array $input , mixed $column_key [, mixed $index_key = NULL ] ) : array
```
_Example :_
``` php
<?php
// Array representing a possible record set returned from a database
$records = array(
    array(
        'id' => 2135,
        'first_name' => 'John',
        'last_name' => 'Doe',
    ),
    array(
        'id' => 3245,
        'first_name' => 'Sally',
        'last_name' => 'Smith',
    ),
    array(
        'id' => 5342,
        'first_name' => 'Jane',
        'last_name' => 'Jones',
    ),
    array(
        'id' => 5623,
        'first_name' => 'Peter',
        'last_name' => 'Doe',
    )
);

$first_names = array_column($records, 'first_name');
print_r($first_names);
?>
```

## array\_sum
``` php
 array_sum ( array $array ) : number
```

## in\_array
``` php
in_array ( mixed $needle , array $haystack [, bool $strict = FALSE ] ) : bool
```
_Example :_
``` php
<?php
$os = array("Mac", "NT", "Irix", "Linux");
if (in_array("Irix", $os)) {
    echo "Got Irix";
}
if (in_array("mac", $os)) {
    echo "Got mac";
}
?>
```

## array\_search
``` php
array_search ( mixed $needle , array $haystack [, bool $strict = FALSE ] ) : mixed
```
_Example :_
``` php
$array = array(0 => 'blue', 1 => 'red', 2 => 'green', 3 => 'red');

$key = array_search('green', $array); // $key = 2;
$key = array_search('red', $array);   // $key = 1;
```

### Multi Dimensional Array / array\_column
_Example :_
``` php
$userdb=Array
(
    (0) => Array
        (
            (uid) => '100',
            (name) => 'Sandra Shush',
            (url) => 'urlof100'
        ),

    (1) => Array
        (
            (uid) => '5465',
            (name) => 'Stefanie Mcmohn',
            (pic_square) => 'urlof100'
        ),

    (2) => Array
        (
            (uid) => '40489',
            (name) => 'Michael',
            (pic_square) => 'urlof40489'
        )
);

$key = array_search(40489, array_column($userdb, 'uid'));
```
## array\_merge
Append/add one array to another
``` php
$a = array('a', 'b');
$b = array('c', 'd');
$merge = array_merge($a, $b);
```

## Difference in array / array\_diff
``` php
$array1 = array("a" => "green", "red", "blue", "red");
$array2 = array("b" => "green", "yellow", "red");
$result = array_diff($array1, $array2); // Array ( [1] => blue )
```

> Pass value you wish to delete and the array to delete from
``` php
function array_delete( $value, $array)
{
    $array = array_diff( $array, array($value) );
    return $array;
}
```

## Array to string / json\_encode
``` php
$arr = array('a' => 1, 'b' => 2, 'c' => 3, 'd' => 4, 'e' => 5);
echo json_encode($arr);
```
## Clone array

``` php
$a = array(1,2);
$b = $a; // $b will be a different array
$c = &$a; // $c will be a reference to $a
```
# Exceptions
``` php
throw new Exception('Division by zero.');
```

