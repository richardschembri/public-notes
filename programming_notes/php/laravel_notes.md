# vim-laravel
> https://vimawesome.com/plugin/vim-laravel-face-rejection
`:Artisan` == `!php artisan`

Command	Applies to...
`:{E,S,V,T}asset`	Anything under `assets/`
`:Ebootstrap`		Bootstrap files in `boostrap/`
`:Echannel`		Broadcast channels
`:Ecommand`		Console commands
`:Econfig`		Configuration files
`:Econtroller`		HTTP controllers
`:Edoc`			The README.md file
`:Eenv`			Your `.env` and `.env.example`
`:Eevent`		Events
`:Eexception`		Exceptions
`:Efactory`		Model factories
`:Ejob`	Jobs
`:Elanguage`		Messages/translations
`:Elib`			All class files under `app/`
`:Elistener`		Event listeners
`:Email`		Mailables
`:Emiddleware`		HTTP middleware
`:Emigration`		Database migrations
`:Enotification`	Notifications
`:Epolicy`		Auth policies
`:Eprovider`		Service providers
`:Erequest`		HTTP form requests
`:Eresource`		HTTP resources
`:Eroutes`		HTTP routes files
`:Erule`		Validation rules
`:Eseeder`		Database seeders
`:Etest`		All class files under `tests/`
`:Eview`		Blade templates

# Command Line
## Migrations
> https://laravel.com/docs/5.7/migrations
``` sh
php artisan make:migration create_users_table
```

``` sh
php artisan make:migration create_users_table --create=users
php artisan make:migration add_votes_to_users_table --table=users
```
``` sh
php artisan migrate
```
## Seeder
### Generate seeder
``` sh
php artisan make:seeder UsersTableSeeder
```
### Run seed
``` sh
php artisan db:seed
php artisan db:seed --class=UsersTableSeeder
php artisan db:seed --class=UsersTableSeeder
php artisan db:seed --class=ProcessesSeeder
```

## make CRUD
### Model
``` sh
php artisan make:model Category
```
### Controller
``` sh
php artisan make:controller Admin\CategoryController
```
### Views
> Not supported, have to create manually
1. index.blade.php
2. create.blade.php
3. edit.blade.php

# Code
## Eloquent
### Filter / Search
#### `whereIn` / `whereNotIn`
> Equivalent to SQL
``` php
$products = ProductPricesInventoryTax::whereIn('product_id', $products)
    ->get();
```
``` php
$users = DB::table('users')
                    ->whereIn('id', [1, 2, 3])
                    ->get();
```
#### Group By
``` php
$builder->groupBy('column1', 'column2', ...);
```

### Any / Exists
``` php
DB::table('orders')->where('finalized', 1)->exists();
```

## Migration
### Add Column(s)
``` sh
php artisan make:migration add_columns_to_users_table --table=users
```

``` php
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedInteger('department_id');
            $table->string('description', 10000)->default('');
            $table->timestamp('last_logged_in')->nullable();
        });
    }

    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn([
                'department_id',
                'description',
                'last_logged_in',]);
        });
    }
```

## Page / Pagination
``` php
$users = DB::table('users')->paginate(15);
```
### Links / Page Navigation (HTML)
```
{{ $users->links() }}
```

# Packages
## (Debugbar)[https://github.com/barryvdh/laravel-debugbar]
``` php
Debugbar::info($object);
Debugbar::error('Error!');
Debugbar::warning('Watch out…');
Debugbar::addMessage('Another message', 'mylabel');
```

# References
## To Check
* (Restful API)[https://www.toptal.com/laravel/restful-laravel-api-tutorial]
