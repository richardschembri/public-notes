Python Notes
============

[Execute OS Commands](https://www.geeksforgeeks.org/python-execute-and-parse-linux-commands/)
---------------------
```python
import os
cmd = 'ls -l'
os.system(cmd)
```

[Read Input](https://www.onlinetutorialspoint.com/python/python-read-input-keyboard.html)
------------

Commands
--------
### Install project requirements
```python
pip install -r requirements.txt
```

Links
-----
https://www.geeksforgeeks.org/python-program-to-check-if-string-is-empty-or-not/
