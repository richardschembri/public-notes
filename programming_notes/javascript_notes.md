Javascripts
===========
[Date](https://www.w3docs.com/snippets/javascript/how-to-convert-a-string-into-a-date-in-javascript.html)
----------------------------------------------
``` javascript
let str = "2020-04-10T17:14:00";
let date = new Date();
console.log("UTC string:  " + date.toUTCString());
console.log("Local string:  " + date.toString());
console.log("Hours local:  " + date.getHours());
console.log("Hours UTC:   " + date.getUTCHours());
```

String Format
-------------
``` javascript
const five = 5;
const ten = 10;
console.log(`Fifteen is {five + ten}`);
```

[`Object.assign`](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign)
-------------------------------------------------------
**Copies** all enumerable own **properties** from one or more source objects **to** a
**target** object. It returns the modified target object.

``` javascript
Object.assign(target, ...sources)
```
### Parameters
#### target
The target object — what to apply the sources’ properties to, which is returned after it is modified.

#### sources
The source object(s) — objects containing the properties you want to apply.

### Return value
The target object.

--------------------------------------------------------------------------------

jQuery
------
###  Starts With Selector
``` html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>attributeStartsWith demo</title>
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
  </head>
  <body>

  <input name="newsletter">
  <input name="milkman">
  <input name="newsboy">

  <script>
  $( "input[name^='news']" ).val( "news here!" );
  </script>

  </body>
</html>
```

Ajax
----
[Jquery AJAX: No 'Access-Control-Allow-Origin' header is present on the requested resource](https://stackoverflow.com/questions/47523265/jquery-ajax-no-access-control-allow-origin-header-is-present-on-the-requested)

### `method` vs `type`
#### `method`
```
The HTTP method to use for the request (e.g. "POST", "GET", "PUT"). (version added: 1.9.0) 
```
#### `type`
```
An alias for method. You should use type if you're using versions of jQuery prior to 1.9.0.
```

