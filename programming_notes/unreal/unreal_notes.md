# Unreal

<!--ts-->

- [Unreal](#unreal)
  - [🏎 Quick Notes](#-quick-notes)
  - [Unreal Terminology](#unreal-terminology)
    - [Unity Terminology Map](#unity-terminology-map)
  - [🛠 Project Setup](#-project-setup)
  - [🪵 Materials](#-materials)
    - [Normal Maps](#normal-maps)
    - [🪵🛠️ Material Editor](#️🪵🛠️-material-editor)
      - [🪵🌟 Material Instance](#🪵🌟-material-instance)
      - [🪵🗻 Landscape Material](#🪵🗻-landscape-material)
  - [³ Post process volume](#-post-process-volume)
  - [🗺 Blueprints](#🗺-blueprints)
    - [Class Settings](#class-settings)
    - [<a href="https://www.linkedin.com/pulse/ue-blueprints-depth-part-1-graphs-functions-macros-macro-laaksonen" rel="nofollow">Logic</a>](https://www.linkedin.com/pulse/ue-blueprints-depth-part-1-graphs-functions-macros-macro-laaksonen)
      - [Function](#function)
      - [Macro](#macro)
      - [Macro Library](#macro-library)
      - [Collapsed Nodes/Graph](#collapsed-nodesgraph)
  - [💻 Source Code](#💻-source-code)
    - [include](#include)
    - [UPROPERTY(&lt;args&gt;)](#upropertyargs)
    - [UFUNCTION(&lt;args&gt;)](#ufunctionargs)
    - [string](#string)
    - [Casting](#casting)
    - [Components](#components)
      - [Create a child component](#create-a-child-component)
    - [Movement Component](#movement-component)
      - [UProjectileMovementComponent](#uprojectilemovementcomponent)
    - [ Spawning](#-spawning)
      - [ Spawning Actors](#-spawning-actors)
    - [ Events](#-events)
      - [Hit Events](#hit-events)
    - [🗄Data](#data)
      - [Data Asset](#data-asset)
      - [Data Table](#data-table)
    - [Forward Declaration](#forward-declaration)
    - [Inheritance](#inheritance)
    - [Movements](#movements)
  - [ Timers](#-timers)
    - [ World Timer Manager](#-world-timer-manager)
      - [FTimerManager](#ftimermanager)
        - [FTimerHandle](#ftimerhandle)
        - [SetTimer](#settimer)
  - [🎛 Widgets (User Interface)](#-widgets-user-interface)
  - [🪞Reflection](#reflection)
    - [Plabar Reflection](#plabar-reflection)
  - [💡Lighting](#💡lighting)
    - [Lumen](#lumen)
      - [Software Ray Tracing Mode](#software-ray-tracing-mode)
    - [Sky Light](#sky-light)
    - [Exposure Compensation](#exposure-compensation)
  - [🕸 Meshes](#-meshes)
    - [Nanite](#nanite)
  - [⌨ Shortcuts](#-shortcuts)
    - [IDE](#ide)
    - [General](#general)
    - [Navigation](#navigation)
    - [Transform/Object](#transformobject)
    - [Material Editor](#material-editor)
    - [Painting](#painting)
  - [♟ Actor Class](#-actor-class)
    - [Pawn: Actor](#pawn-actor)
    - [Character: Pawn](#character-pawn)
      - [Event Graph](#event-graph)
  - [🕹 Input](#-input)
    - [Input Action](#input-action)
    - [Input Mapping Context](#input-mapping-context)
    - [Axis Mappings (Depracted)](#axis-mappings-depracted)
  - [🔨 Export/Build](#-exportbuild)
    - [UnrealBuildTool (UBT)](#unrealbuildtool-ubt)
      - [<a href="https://forums.unrealengine.com/t/solved-upgrade-c-project-from-ue-4-24-to-4-25-under-linux/144950/10" rel="nofollow">Build Project Linux</a>](https://forums.unrealengine.com/t/solved-upgrade-c-project-from-ue-4-24-to-4-25-under-linux/144950/10)
    - [<a href="https://docs.unrealengine.com/5.0/en-US/android-development-basics-for-unreal-engine/" rel="nofollow">Android</a>](https://docs.unrealengine.com/5.0/en-US/android-development-basics-for-unreal-engine/)
      - [<a href="https://docs.unrealengine.com/4.27/en-US/SharingAndReleasing/Mobile/Android/Setup/AndroidStudio/" rel="nofollow">Setup</a>](https://docs.unrealengine.com/4.27/en-US/SharingAndReleasing/Mobile/Android/Setup/AndroidStudio/)
        - [SDKConfig](#sdkconfig)
        - [Windows AndroidSetup erro](#windows-androidsetup-erro)
        - [Windows JAVA_HOME issue](#windows-java_home-issue)
        - [<a href="https://forums.unrealengine.com/t/no-androidmanifest-xml-found-what-can-i-do/251841" rel="nofollow">Missing AndroidManifest error</a>](https://forums.unrealengine.com/t/no-androidmanifest-xml-found-what-can-i-do/251841)
      - [Export Formats](#export-formats)
  - [Setup](#setup-1)
    - [Binary](#binary)
    - [<a href="https://docs.unrealengine.com/5.2/en-US/downloading-unreal-engine-source-code/" rel="nofollow">Linux</a>](https://docs.unrealengine.com/5.2/en-US/downloading-unreal-engine-source-code/)
    - [Windows](#windows)
      - [dotnet not found](#dotnet-not-found)
  - [🔗 Links](#-links)

<!-- Created by https://github.com/ekalinin/github-markdown-toc -->
<!-- Added by: richard, at: Tue 16 Jul 22:11:49 JST 2024 -->

<!--te-->

## 🏎 Quick Notes

- Projects can be opened directly from `uproject` files.
- **Height Map:** A texture that where each pixel represents elevation data
- File -> Generate Visual Studio Project
- Pawn is an Actor that can be possessed by the player
- F8 - Fly around mode
- `bool HasDamage(float& OutDamage)`: Out variable
- `void PrintDamage(const float& Damage)`: Pass by reference but do not allow modifications.
- `UPhysicsHandleComponent* GetPhysicsHandle() const;`: const meants the function will not modify anything.
- `Simulate` plays game but viewing from unreal editor camera

## Unreal Terminology

[Official Documentation](https://dev.epicgames.com/documentation/en-us/unreal-engine/unreal-engine-terminology)

- **🌍 `World`**: Holds a list of levels

### Unity Terminology Map

| Unity             | Unreal      |
| :---------------- | :---------- |
| GameObject        | Actor       |
| Component         | Component   |
| Assets            | Content     |
| GameObject Parent | Folder      |
| Inspector         | Details     |
| Scene             | Map / Level |
| Sub Scene         | Sublevel    |

## 🛠 Project Setup

1. Edit -> Project Settings -> Maps & Modes
2. Default Modes -> Default GameMode: <Your project GameMode>
3. Default Maps -> Editor Startup Map: <Your map>
4. Default Maps -> Game Default Map: <Your map>

## 🪵 Materials

### Normal Maps

Unreal uses **Direct X** `Normal maps` **not** **Open GL**
When importing `Normal maps` **Open GL** go to `Texture` -> `sRBG` and click `Flip Green Channel`

### 🪵🛠️ Material Editor

Base Texture(for `Base Color`, `Normal`, etc) -> `Texture Sample`

#### 🪵🌟 Material Instance

- Allows to change `Material` properties in **real time**
- `<Right Click>` on nodes and convert them to properties to be able to **access and modify** them

#### 🪵🗻 Landscape Material

- Composed of **multiple mini materials**
- Used to **paint a landscape**

## ³ Post process volume

Volume in scene where post processing takes effect (Can be toggled to effect entire scene)

## 🗺 Blueprints

- **Access:** `Graphs` -> `EventGraph`
- `Event Tick` -> `Update`
- You can drag component into blueprint
- **Naming Convension:** `BP_MyBlueprint`
- You can toggle the visibillity of `variable`s in order to turn them into `parameter`s
- Levels/Maps have their own blueprint(like everything else).
- `Child Blueprint`: Inherits from parent(like classes)
- Click on object then rightclick blueprint to get a reference to it
- You can create `Blueprints` from c++ classes by right clicking the cpp class and selecting _Create Blueprint class based on MyClass_

### Class Settings

- You can change the **Parent Class**

### [Logic](https://www.linkedin.com/pulse/ue-blueprints-depth-part-1-graphs-functions-macros-macro-laaksonen)

#### Function

- Like regular functions

#### Macro

- Like function except it does not have local variables.
- 1 exec input multiple exec outputs.
- Only accessible withing blueprint.
- Can have delayed execution.

#### Macro Library

- Container Blueprint holds a collection of Macros thank can be usable in all Blueprints in project.
- Add as nodes in other blueprints.
- Do not use to modify blueprint variables.
- Use for input/output.

#### Collapsed Nodes/Graph

- Cosmetic way to make **Blueprint** more clear to read.

## 💻 Source Code

### `include`

Copy contents of header file inside the current file

### `UPROPERTY(<args>)`

- `VisibleInstanceOnly`: Make `variable` visible only on instances(Inside the Scene/Map) of blueprint in **Details** panel.
- `VisibleDefaultsOnly`: Make `variable` visible only on base blueprint(not an instance) in **Details** panel.
- `VisibleAnywhere`: Make `variable` visible in **Details** panel.
- `EditInstanceOnly`: Expose `variable` only on instances(Inside the Scene/Map) of blueprint in **Details** panel.
- `EditDefaultsOnly`: Expose `variable` only on base blueprint(not an instance) in **Details** panel.
- `EditAnywhere`: Make `variable` editable in **Details** panel.
- `BlueprintReadWrite`: Make variable editablein Blueprint **EventGraph**.
- `BlueprintReadOnly`: Make variable viewable in Blueprint **EventGraph** but not editable.
- `Category`: Group the properties (Like `Header` in Unity)
- `meta = (AllowPrivateAccess = "true")`: Makes private "variables" use **Details** panel and **Event graph** macros like `BlueprintReadWrite` or `EditDefaultsOnly`. So basically the variable is hidden from other CPP classes but exposed to blueprints/details.

### `UFUNCTION(<args>)`

### `string`

Unreal engine uses `TEXT` as a string

```cpp
TEXT("My String")
```

### Casting

```cpp
Tank = Cast<ATank>(UGameplayStatics::GetPlayerPawn(this, 0));
```

### Components

#### Create a child component

Example of creating a child capsule component

```cpp
CapsuleComp = CreateDefaultSubobject<UCapsuleComponent>(TEXT("Capsule Collider"));
```

Attaching components to each other

```cpp
BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Base Mesh")):
BaseMesh->SetupAttachment(CapsuleComp);
```

`SetupAttachment` is part of `USceneComponent`

### Movement Component

Handles movement in unreal engine

#### `UProjectileMovementComponent`

A **Movement Component** for projectiles

###  Spawning

####  Spawning Actors

```cpp
UWorld::SpawnActor<AProjectile>(ProjectileClass, Loc, Rot);
```

### `TSubclassOf` / Template

Tells the editor's property window to list of classes derived from the type
**Example:**

```cpp
UPROPERTY(EditDefaultsOnly, Category=Damage)
TSubclassOf<UDamageType> DamageType; // Only list classes of type UDamageType
```

###  Events

#### Hit Events

### 🗄Data

#### Data Asset

C++ class that holds data that will pulled. Set `UPROPERTY(EditAnywhere)` to have values editable from the editor

Can have functions for when you want for example calculate values

When creating a DataAsset you can select DataAsset class

Easier to manage than DataTable since multiple developers can work on different instances of DataAsset whereas in a datatable everyone would need to edit the same file.

#### Data Table

Like `Data Asset` with multiple rows

### Forward Declaration

To avoid using header files to get a reference use the `class` keyword inside the header file.

```cpp
class UCapsuleComponent* CapsuleComp;
```

Implement it inside the cpp file.
Including in header file will have it copied to other header files that include itself

Use "F" prefix for struct names

### Inheritance

Right Click on class -> Create C++ class derived from **MyClass**

### Movements

- `AddActorLocalOffset`:
  - Moves the Actor in a local direction
  - Inherited by `Pawn`

##  Timers

###  World Timer Manager

- `TimeManager.h`
- `GetWorldTimerManager`

#### `FTimerManager`

Manages timers

##### `FTimerHandle`

Unique handle that can be used to distinguish timers that have identical delegates
**Example:**

```cpp
FTimerHandle FireRateTimerHandle;
```

##### `SetTimer`

```cpp
GetWorldTimerManager().SetTimer(FireRateTimerHandle, this, &ATower::CheckFireCondition, FireRate, true);
```

> `this` the class to handle the timer
> `ATower::CheckFireCondition` is the function/delegate to be called

## 🎛 Widgets (User Interface)

## 🪞Reflection

### Plabar Reflection

## 💡Lighting

Basic lighting in scene -> `DirectionalLight`

**Baking:** Lightmass

### 💡Lumen

> Real time Global Illumination system (color reflection off surfaces onto another)
> Accessed by placing a `PostProcessVolume` in the level

#### Software Ray Tracing Mode

> Accessed via Project Settings

- **Detail Tracing:** As accurate as possible
- **Global Tracing:** More lightweight

### Sky Light

Project Sky unto the scene

### Exposure Compensation

By default when moving around the level exosure drastically changes (Ex. getting very dark).

To avoid this add a `Post Processing Volume` and set _Metering mde_ to _Auto Exposure Basic_.

## 🕸 Meshes

### Nanite

Makes meshes as dense as possible depending on view distance

## ⌨ Shortcuts

### IDE

| Function            | Shortcut                     |
| ------------------- | :--------------------------- |
| **Live Code Build** | `<Ctrl>` + `<Alt>` + `<F11>` |

### General

| Function      | Shortcut |
| ------------- | -------- |
| **Increase**  | `]`      |
| **Decrease**  | `[`      |
| **Eject out** | `<F8>`   |

### Navigation

| Function                     | Shortcut                     |
| :--------------------------- | :--------------------------- |
| **Move**                     | Hold `<Left Click>` + `wsad` |
| **Hide Widgets**             | `g`                          |
| **Set viewport bookmarks**   | `<Ctrl>` + number            |
| **Go to viewport bookmarks** | number                       |
| **Move with object**         | `<Shift>` + move object      |

### Transform/Object

| Function         | Shortcut                               |
| :--------------- | :------------------------------------- |
| **Focus**        | `f`                                    |
| **Zoom in/out**  | `<Alt>` + `<Left Click>` + `<Scroll>`  |
| **Pivot around** | `<Alt>` + `<Right Click>` + `<Scroll>` |
| **Move**         | `w`                                    |
| **Rotate**       | `e`                                    |
| **Scale**        | `s`                                    |
| **Duplicate**    | `<Ctrl>` + `w`                         |
| **Duplicate**    | `<Alt>` + _drag_                       |
| **Snap**         | `n`                                    |
| **Add Material** | Drag material to Object                |

### Material Editor

| Function        | Shortcut                   |
| --------------- | :------------------------- |
| **Copy note**   | `<Ctrl>` + `w`             |
| **Group nodes** | `<Left mouse drag>` -> `c` |

### Painting

| Function | Shortcut |
| **Erasor/Rubber** | `<Shift>` + `<Left mouse drag>` -> `c` |

## ♟ Actor Class

> Similar to Unity's `GameObject`

- Can be placed in the world
- Can have a visual representation (Mesh, etc)

### ♟ Pawn: Actor

- Is a controllable Actor
- `Auto Possess Player`: Allows player to control the _Pawn_
- Can be possessed by a `Controller`
- Handles movement input

### ♟ Character: Pawn

- Has Character specific stuff(`Character Movement Componenet`, etc)
- Movement modes (flying)
- Best for bipedal characters

#### 📈Event Graph

- `Event Possessed`

## 🕹 Input

`Content Browers -> <Right Click> -> Input`

### Input Action

**Naming Convetion:** `IA_MyAction` where **IA** means **I**nput **A**ction

### Input Mapping Context

**Naming Convetion:** `IM_MyMappingName` where **IM** means **I**nput **M**apping
Add `Input Action`s inside it

> **Negate:** Means inverse/opposite

### Axis Mappings (Depracted)

Edit -> Project Settings -> Input -> Bindings
Bind in `SetupPlayerInputComponent` in `Pawn` class

---

## 🔨 Export/Build

### UnrealBuildTool (UBT)

#### [Build Project Linux](https://forums.unrealengine.com/t/solved-upgrade-c-project-from-ue-4-24-to-4-25-under-linux/144950/10)

> May be needed when this message is displayed: `Please rebuild from an IDE instead`

```sh
/path/to/UnrealEngine/GenerateProjectFiles.sh /path/to/myproject.uproject
```

### [Android](https://docs.unrealengine.com/5.0/en-US/android-development-basics-for-unreal-engine/)

#### [Setup](https://docs.unrealengine.com/4.27/en-US/SharingAndReleasing/Mobile/Android/Setup/AndroidStudio/)

##### SDKConfig

Check for the correct versions for the config in:
`<Path to Unreal>/Engine/Extras/Android/SetupAndroid`

Be sure that newer versions of the Android SDK/NDK are not installed

##### Windows AndroidSetup erro

Might need to update the batfile to use the following path

```powershell
SDKMANAGER=%STUDIO_SDK_PATH%\cmdline-tools\latest\bin\sdkmanager.bat
```

##### Windows `JAVA_HOME` issue

If the java.exe is not being located. It is probably because `JAVA_HOME` is pointing to the Android SDK's `jre` folder instead of `jbr`.

Change the environment variable to point to `C:\Program Files\Android\Android Studio\jbr`

##### [Missing AndroidManifest error](https://forums.unrealengine.com/t/no-androidmanifest-xml-found-what-can-i-do/251841)

Delete folder “Intermediate” from your project folder

#### Export Formats

| Format | Alpha Compression | Platform                     | Notes                                                                                                                         |
| :----- | :---------------: | :--------------------------- | :---------------------------------------------------------------------------------------------------------------------------- |
| ETC1   |         N         | All Android devices          | Recommend using an RGB and a separate alpha texture if need alpha to get better compression                                   |
| ETC2   |         Y         | All OpenGL 3.x class devices |                                                                                                                               |
| ATC    |         Y         | Qualcomm Adreno GPUs         |                                                                                                                               |
| DTX    |         Y         | Nvidia Tegra GPUs            |                                                                                                                               |
| PVRTC  |         Y         | PowerVR GPUs                 |                                                                                                                               |
| ASTC   |         Y         | Some devices                 | Latest Texture compression format allowing more quality control by specifying block size. Will be required for Vulkan Level 1 |

---

## Setup

### Binary

- **Linux**: https://docs.unrealengine.com/5.2/en-US/installing-unreal-engine/

### [Linux](https://github.com/EpicGames/UnrealEngine/blob/release/Engine/Build/BatchFiles/Linux/README.md)

1. `cd UnrealEngine`
2. `./Setup.sh`
3. `./GenerateProjectFiles.sh`

Then run this command **WITHOUT** `sudo`

```sh
./Engine/Build/BatchFiles/RunUAT.sh BuildGraph -target="Make Installed Build Linux" -script=Engine/Build/InstalledEngineBuild.xml -set:WithDDC=false -set:HostPlatformOnly=true
```

If you are getting an ICU package error, you can either
A: add this environment variable: DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
or
B: install the libicu50 package

If you are getting SSL certificate errors, add these environment variables:
`SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt`
`SSL_CERT_DIR=/dev/null`

To add a environment variables just type this on the terminal:
`export NAME_OF_THE_VARIABLE`

So it would go like this:

```sh
export DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
export SSL_CERT_FILE=/etc/ssl/certs/ca-certificates.crt
export SSL_CERT_DIR=/dev/null
```

Now that terminal window has those variables for dotnet.

I ran into a problem doing this on a NTFS partition got a I/O error, so try to do
it in a EXT4 partition, I also ran out of storage and the process failed, running
the command by Ethorbit gave me an error, so I just ran:

sudo make

### Windows

#### `dotnet not found`

- Install latest [.NET SDK](https://dotnet.microsoft.com)
- Open **Edit the system environment variables**
- Add the following to the `path` variable : `%ProgramFiles%\dotnet\dotnet.exe`

---

## 🔗 Links

- [Unreal Engine C++](https://www.youtube.com/watch?v=LsNW4FPHuZE&t=14880)
- [UE5 C++](https://www.youtube.com/watch?v=nvruYLgjKkk)
- [UE VR Hands Template](https://github.com/DanielRBowen/UE-VRHands-Template)
- [Unreal Engine 5 with Vim or Emacs on Linux](https://neunerdhausen.de/posts/unreal-engine-5-with-vim)
