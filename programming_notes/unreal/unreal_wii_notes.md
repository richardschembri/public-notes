Unreal Wii Notes
================


Plugins
-------
- [UEWiiUsePlugin](https://github.com/tsky1971/UEWiiUsePlugin)
- [UE4-Wiimote-Plugin](https://github.com/xoyojank/UE4-Wiimote-Plugin)
	- [Forum post with guide](https://forums.unrealengine.com/t/wiimote-plugin-for-ue4/34417/19)

### Wii Controller Libraries
[Wii Use](https://github.com/wiiuse/wiiuse)
