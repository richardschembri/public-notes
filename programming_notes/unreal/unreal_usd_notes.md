[Unreal USD(Universal Scene Description)](https://docs.unrealengine.com/5.0/en-US/universal-scene-description-in-unreal-engine/)
=======================================

Formats
-------
- `.usd`
- `.usda`
- `.usdc`
- `.usdz`
