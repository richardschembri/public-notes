# Common `include`
``` clang
#include <stdio.h> // For printf, scanf
#include <string.h> // For strcpy
```

## `printf`
> Prints out to console and supports string formatting
``` clang
printf("What is your name? ");
```
## `scanf`
> Reads user console input
``` clang
scanf(" %s %s", firstName, lastname);
printf("Your Name is %s %s \n\n", firstName, lastname);
```


# Constants
``` clang
#define MYNAME "Richard Schembri"
```

# Variables
## local
* Cannot be returned from a function because it is saved in the stack
# Arrays
* Cannot be returned from functions
* are not modifyable lvalue

## "Returning" Arrays
### Option 1:
Dynamically allocate the memory inside of the function (caller responsible for deallocating ret)
#### Function definition
```clang
char *foo(int count) {
    char *ret = malloc(count);
    if(!ret)
        return NULL;

    for(int i = 0; i < count; ++i) 
        ret[i] = i;

    return ret;
}
```
#### Function call
``` clang
int main() {
    char *p = foo(10);
    if(p) {
        // do stuff with p
        free(p);
    }

    return 0;
}
```
### Option 2:

## String Literal
``` clang
char *func(){
	return "Returned string";
}
```

# Structs
``` clang
struct dog{
	const char *name;
	const char *breed;
	int avgHeightCm;
	int avgWeightKg;
};

void main(){
	struct dog fufi = {"Fufi", "Doberman", 90, 20};
}
```

## typedef
``` clang
typedef struct dog{
	const char *name;
	const char *breed;
	int avgHeightCm;
	int avgWeightKg;
} dog;

void main(){
	dog fufi = {"Fufi", "Doberman", 90, 20};
}
```

# Union
``` clang
typedef union{
	short individual;
	int pound;
	int ounce;
} amount;

amount orangeAmt = {.ounce = 16};
orangeAmt.individual = 4;// Same as above line

typedef struct{
	char *brand;
	amount theAmount;
} orangeProduct;

orangeProduct productOrdered = {"Chiquita",
				.theAmount.ounce = 16}

```

# Union vs Struct
> Basically a matter of memory usage
``` clang
union foo {
  int a;   // can't use both a and b at once
  char b;
} foo;

struct bar {
  int a;   // can use both a and b simultaneously
  char b;
} bar;

union foo x;
x.a = 3; // OK
x.b = 'c'; // NO! this affects the value of x.a!

struct bar y;
y.a = 3; // OK
y.b = 'c'; // OK
```

# While through array
``` clang
char * randomString = "Just some random stuff";
while (*randomString){
	putchar(*randomString++);
}
// Iterates until it finds null which is interpreted as a 0/false
```

# Static
## variables
* local variables decleration statements are only run once.
``` clang
static void print_num(void){
	static int y = 0; // Only called once
	printf("%d\n", y)
	y = y + 1;
}
```

* static variables are allocated in **data segment** of the function instead of the **stack**.
* global variables will be accessed only by the elements inside the same **c** file
## functions
* static functions will be accessed only by the elements inside the same **c** file

# extern
Tells the compiler that the variable is being declared somewhere else
``` clang
// main.c

void caller(void); // function prototype of caller

int x = 10; // global variable
int main(void){
	caller();
	return 0;
}

// caller.c
extern int x;
void caller(void){
	printf("global variable x = %d\n", x); // outputs 10
}

```
