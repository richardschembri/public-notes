[clang OpenGL Notes](https://www.geeksforgeeks.org/getting-started-with-opengl/)
==================

Setup
-----
### Linux
#### Packages
`freeglut`
| Distro | Package |
| :----- | :------ |
| OpenSUSE | `freeglut-devel` |
| Ubuntu | `freeglut3-dev` |

Build/Compile
-------------
### Linux
```sh
gcc filename.c -lGL -lGLU -lglut 
```
