[Universal Scene Description](https://graphics.pixar.com/usd/release/index.html)
=============================

USD Variants
------------
Author variants of an object. For example *Geometric* and *Shading* variants.

USD Payloads
------------
Selectively load/unload subsets of a scene. Especially usefully for **resource heavy** scenes.

File Formats
------------
### [USDZ](https://www.macworld.com/article/673115/what-is-usdz.html)
- `USD` **zero-compression unencrypted zip archive**
- Supported by **Apple** `ARKit 2` and `Measure app`
- Holds number of **files** and required **tools** to render them

