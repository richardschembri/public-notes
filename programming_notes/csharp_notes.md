C# notes
========
> C# is the language I mostly used throughout my career so these are
> notes for quick reference.

Access Modifiers
----------------
### protected
Accessible for inherited classes, otherwise `private`.
## internal
`public` only for classes inside the assembly, otherwise private.
## protected internal 
Means `protected` **OR** internal:
methods become accessible for inherited classes **AND** for any classes
inside the assembly.
## private protected
A `public` class that has a `protected` `property` that is only available
internally
## extern
``` csharp
[DllImport("avifil32.dll")]
private static extern void AVIFileInit();
``` 

Method Parameters
-----------------
### params
``` csharp
public class MyClass
{
    public static void UseParams(params int[] list)
    {
        for (int i = 0; i < list.Length; i++)
        {
            Console.Write(list[i] + " ");
        }
        Console.WriteLine();
    }

    public static void UseParams2(params object[] list)
    {
        for (int i = 0; i < list.Length; i++)
        {
            Console.Write(list[i] + " ");
        }
        Console.WriteLine();
    }

    static void Main()
    {
        UseParams(1, 2, 3, 4);
        UseParams2(1, 'a', "test");}}
        UseParams2();
        int[] myIntArray = { 5, 6, 7, 8, 9 };
        UseParams(myIntArray);

        object[] myObjArray = { 2, 'b', "test", "again" };
        UseParams2(myObjArray);
        UseParams2(myIntArray);
    }
}
``` 
## out
It is like the `ref` keyword, except that `ref` requires that the
variable be initialized before it is passed.
## in
It is like the `ref` or `out` keywords, except that in arguments
**cannot be modified** by the called method.
``` csharp
void InArgExample(in int number)
{
    // Uncomment the following line to see error CS8331
    //number = 19;
}
```
Reference Types
---------------
### `dynamic`
Like `var` but bypasses compile-time type checking. Instead, these
- Operations are resolved at run time.
- Uses reflection internally.
- Can only invoke public members of an object.
## delegate
A reference type that can be used to encapsulate a named or an
anonymous method.
### Multicast Delegate
A delegate that points to and eventually fires off several methods.

``` csharp
delegate double MathAction(double num);

class DelegateTest
{
    // Regular method that matches signature:
    static double Double(double input)
    {
        return input * 2;
    }

    static void Main()
    {
        MathAction ma = Double;

        double multByTwo = ma(4.5);
        Console.WriteLine("multByTwo: {0}", multByTwo);

        MathAction ma2 = delegate(double input)
        {
            return input * input;
        };

        double square = ma2(5);
        Console.WriteLine("square: {0}", square);

        MathAction ma3 = s => s * s * s;
        double cube = ma3(4.375);

        Console.WriteLine("cube: {0}", cube);
    }
}
```
Contextual Keywords
-------------------
### partial (Type)
Allow for the definition of a class, struct, or interface to be split into multiple files.
_File1.cs_:
``` csharp
namespace PC
{
    partial class A
    {
        int num = 0;
        void MethodA() { }
        partial void MethodC();
    }
}
```
_File2.cs_:
``` csharp
namespace PC
{
    partial class A
    {
        void MethodB() { }
        partial void MethodC() { }
    }
}
```
### `when`
_Example 1_:
``` csharp
public static async Task<string> MakeRequest()
{ 
	var client = new System.Net.Http.HttpClient();
	var streamTask = client.GetStringAsync("https://localHost:10000");
	try {
		var responseText = await streamTask;
		return responseText;
	} 
	catch (HttpRequestException e) when (e.Message.Contains("301")) {
		return "Site Moved";
	}
	catch (HttpRequestException e) when (e.Message.Contains("404")) {
		return "Page Not Found";
	}
	catch (HttpRequestException e) {
		return e.Message;
	}
}
```
_Example 2_:
``` csharp
private static void ShowShapeInfo(Object obj)
{
	switch (obj)
	{
		case Shape shape when shape.Area == 0:
			Console.WriteLine($"The shape: {shape.GetType().Name} with no dimensions");
			break;
		case Square sq when sq.Area > 0:
			Console.WriteLine("Information about the square:");
			Console.WriteLine($"   Length of a side: {sq.Side}");
			Console.WriteLine($"   Area: {sq.Area}");
			break;
		case Rectangle r when r.Area > 0:
			Console.WriteLine("Information about the rectangle:");
			Console.WriteLine($"   Dimensions: {r.Length} x {r.Width}");
			Console.WriteLine($"   Area: {r.Area}");
			break;
		case Shape shape:
			Console.WriteLine($"A {shape.GetType().Name} shape");
			break;
		case null:
			Console.WriteLine($"The {nameof(obj)} variable is uninitialized.");
			break;
		default:
			Console.WriteLine($"The {nameof(obj)} variable does not represent a Shape.");
			break;   
	}
}
```

## where (generic type constraint)
The following example shows the types that can now be specified as a
base class:
``` csharp
public class UsingEnum<T> where T : System.Enum { }
public class UsingDelegate<T> where T : System.Delegate { }
public class Multicaster<T> where T : System.MulticastDelegate { }
```
``` csharp
public interface IMyInterface { }

namespace CodeExample
{
    class Dictionary<TKey, TVal>
        where TKey : IComparable<TKey>
        where TVal : IMyInterface
    {
        public void Add(TKey key, TVal val) { }
    }
}
```

### new

The `new()` Constraint lets the compiler know that any type argument
supplied must have an accessible parameterless--or default--
constructor.
_For example_:
``` csharp
public class MyGenericClass<T> where T : IComparable<T>, new()
{
    // The following line is not possible without new() constraint:
    T item = new T();
}
```

Attributes
----------
Used for adding metadata, like compiler instruction or other information (comments, description, etc).
### Types of Pre-define attributes
- AttributeUsage
- Conditional
- Obsolete

Unsafe/Unmanaged Code
---------------------
- We can define an unsafe context in which pointer can be used.
- The CLR does not verify its safety.
- The CLR will only execute the unsafe code if it is within a fully trusted assembly.
- We can define Methods, types, and code blocks as unsafe
- In some cases, unsafe code may increase the application’s performance by removing array bounds checks
- Unsafe code is required in order to call native functions that require pointers
- Using unsafe code brings security and stability risks
- In order to compile unsafe code, the application must be compiled with /unsafe
- Cannot be executed in an un-trusted environment. For example, we cannot run unsafe code directly from the Internet.
## Compilation
- For **compiling** unsafe code, we have to specify the `/unsafe` command-line switch with command-line compiler.
### Visual Studio
1. Open project properties
2. Click on the Build tab
3. Select the option “Allow unsafe code”

Satellite Assembly
------------------
When we write the code, a multicultural or multilingual application in .NET and want to distribute the core application separately from the localized modules,the localized assemblies that modify the core application.

Array
-----
- **Length** is the property of array object and using it is the most effective way to determine the count of elements in the array.
- **Count()** is a LINQ extension method that does effectively the same. It applies to arrays because arrays are enumerable objects. It's preferred to use Length because Count() is likely to be more expensive.
- **Rank** is the property that returns the number of dimensions (different thing entirely). When you declare an array int[,] myArray = new int[5,10]; the Rank of it will be 2 but it will hold a total of 50 elements.

Ineritence vs Composition
-------------------------
### Ineritence
Inheritance is a hierarchy of relationships between objects. For example, car is
a vehicle. So, The below denotes **is-a** relationship between objects. "Car is
a vehicle".
``` csharp
class Vehicle {

//.....

}

class Car : Vehicle {

//......

}

```

Composition
-----------

Composition denotes **is-a-part-of** relationship between objects. For example,

``` csharp
class Engine {
//....
}

class Car {
	Engine engine = new Engine();
//.....
}
```

--------------------------------------------------------------------------------

Garbage Collection
------------------
## Destructor + IDisposable
- ≈(it's almost equal to) base.Finalize(), 
- The destructor is converted into an override version of the Finalize method that executes the destructor’s code and then calls the base class’s Finalize method. 
- Its totally non deterministic you can't able to know when will be called because depends on GC.
- If a class contains no managed resources and no unmanaged resources, it doesn’t need to implement **IDisposable** or have a destructor.
 -If the class has only managed resources, it should implement **IDisposable** but it doesn’t need a destructor. (When the destructor executes, you can’t be sure managed objects still exist, so you can’t call their Dispose methods anyway.)

If the class has only unmanaged resources, it needs to implement IDisposable and needs a destructor in case the program doesn’t call Dispose.

The Dispose method must be safe to run more than once. You can achieve that by using a variable to keep track of whether it has been run before.

The Dispose method should free both managed and unmanaged resources.

The destructor should free only unmanaged resources. (When the destructor executes, you can’t be sure managed objects still exist, so you can’t call their Dispose methods anyway.)

After freeing resources, the destructor should call GC.SuppressFinalize, so the object can skip the finalization queue.

--------------------------------------------------------------------------------

LINQ
----
### [Aggregate](https://stackoverflow.com/questions/11030109/aggregate-vs-sum-performance-in-linq)
```csharp
// Result of each iteration is stored in result
source.Aggregate(0, (result, element) => result + element);  
```
is the same as
```csharp
source.Sum();
```

--------------------------------------------------------------------------------

References
----------
* (What is the difference between 'protected' and 'protected internal'?)[https://stackoverflow.com/questions/585859/what-is-the-difference-between-protected-and-protected-internal]
* (C# Keywords)[https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/]
* [C# interview questions](https://hackr.io/blog/c-sharp-interview-questions)
