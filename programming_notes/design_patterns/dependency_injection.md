[Dependency Injection Pattern](https://www.youtube.com/watch?v=tTJetZj3vg0)
===============================================================================
Constructor Injection
---------------------

```csharp
public class PersonFactory
{
    Person GetPersion(){
        var logger = new Logger();
        // Inject Logger depedency via parameter
        return new Persion("Gerald", logger);
    }
}

public class Person
{
    public Person(string name, Logger logger){
        logger.Log($"{name} was created") ;
    }
}
```

## Method Injection

```csharp
public class PersonFactory
{
    Person GetPerson()
    {
        var logger = new Logger();
        
        var person = new Person();
        person.DoComplexThings(logger);
        
        return person;
    }
}

public class Person
{
    public void DoComplexThings(Logger logger)
    {
        logger.Log("42|1337|much complex|such math");
    }
}
```

\newpage

Interfaces
----------
- New only once (Share the instance of an object with multiple classes)
- Easily swappable implementations

```csharp
public interface ILogger{
    void Log(string message);
}

public class FileLogger : ILogger{
    public void Log(string message){
        File.AppendText(message);
    }
}

public class ConsoleLogger : ILogger{
    public void Log(string message){
        Console.Log(message);
    }
}

public class Person
{
    public Person(string name, ILogger logger){
        logger.Log($"{name} was created") ;
    }
}
```

### Unit Testing
```csharp
public class MockLogger : ILogger{
    public void Log(string message){
        // Nothing to do, just for testing
    }
}
```

\newpage

[Container](https://rubikscode.net/2018/04/09/exploring-dependency-injection-in-c-and-top-3-di-containers-part-2/)
---------------------------------------------------
- Used to be called IoC containers (Investion of Control)
- Are used to make DI easier to use.

### RRR (**R**egister **R**esolve **R**elease)
#### Register
- **Define** which **object** of which class or interface will **be created** once
    **dependency** is encountered. 
- **Define** a **lifespan** of that **instance**. Either:
    - Use the same instance for all other dependencies
    - A new object be created for every dependency of that object
    - Etc

#### Resolve
- **Ask** the container **for an object** of the certain class. 
- A container will **resolve** all **dependencies, and** give out a fully **initialized
    object** that is ready for use.

#### Release
- Release an object from the container(**delete** it)

#### Example
```csharp
public interface IService1
{
    void Service1Method();
}

public interface IService2
{
    void Service2Method();
}

public class Service1 : IService1
{
    public void Service1Method()
    {
        // Service Method Implementation
    }
}
```
\newpage
```csharp
public class Service2 : IService2
{
    public void Service2Method()
    {
        // Service Method Implementation
    }
}

// Client class with constructor injection
public class Client
{
    private readonly IService1 _service1;
    private readonly IService2 _service2;

    public Client(IService1 service, IService2 service2)
    {
        _service1 = service;
        _service2 = service2;
    }

    public void InitiateServiceMethods()
    {
        _service1.Service1Method();
        _service2.Service2Method();
    }
}
```

##### Castle Windsor
> Castle Windsor is still one of the most popular DI Frameworks

```csharp
// Declare container
var container = new WindsorContainer();

// Register objects
container.Register(Component.For<Client>());

// Any time the container finds a dependency on IService1 it should create an
// object of the type Service1.
container.Register(Component.For<IService1>()
    .ImplementedBy<Service1>());

// Any time the container finds a dependency on IService2 use it's singleton or
// create one if it doesn't exist.
container.Register(Component.For<IService2>()
    .ImplementedBy<Service2>().LifestyleSingleton());

// The container will create all dependencies, properly initialize and return
// the object of the Client class.
var client = container.Resolve<Client>();
client.InitiateServiceMethods();

// Delete Client object
container.Release(client);
```
