## Setup android export
### Archlinux Setup
> Install android studio first

#### Access export settings
Editor -> Editor Settings -> General -> Export -> Android

#### Settings
**adb :** `/home/<user>/Android/Sdk/platform-tools/adb` 
**jarsigner :** `/usr/lib/jvm/java-8-openjdk/bin/jarsigner`
**debugkeystore :** `/home/<user>/.android/debug.keystore`
