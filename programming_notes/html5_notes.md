# Custom Attributes
> Should all be lowercase
`data-*`
_Example :_
```
<ul id="vegetable-seeds">
  <li data-spacing="10cm" data-sowing-time="March to June">Carrots</li>
  <li data-spacing="30cm" data-sowing-time="February to March">Celery</li>
  <li data-spacing="3cm" data-sowing-time="March to September">Radishes</li>
</ul>
```


CSS Notes
=========
### margin
```css
margin: 25px 50px 75px 100px;
```

- **top margin**:     25px
- **right margin**:   50px
- **bottom margin**:  75px
- **left margin**:   100px
