[Perforce Notes](https://www.perforce.com/manuals/p4guide/Content/P4Guide/Home-p4guide.html)
================

<!--ts-->
* [<a href="https://www.perforce.com/manuals/p4guide/Content/P4Guide/Home-p4guide.html" rel="nofollow">Perforce Notes</a>](https://www.perforce.com/manuals/p4guide/Content/P4Guide/Home-p4guide.html)
   * [ Commands](#-commands)
      * [ Start up shared server](#-start-up-shared-server)
      * [ Log in](#-log-in)
         * [ Unix](#-unix)
         * [ Windows](#-windows)
      * [🗺️  Typemap](#️🗺️--typemap)
         * [File Types](#file-types)
      * [🗺️  Workspace Mappings](#️🗺️--workspace-mappings)
      * [ Check connection](#-check-connection)
         * [Trust connection](#trust-connection)
      * [<a href="https://www.perforce.com/video-tutorials/vcs/what-perforce-streams" rel="nofollow"> Stream</a>](https://www.perforce.com/video-tutorials/vcs/what-perforce-streams)
         * [ New Stream](#-new-stream)
            * [Create a mainline stream](#create-a-mainline-stream)
               * [  Verify Stream](#--verify-stream)
            * [ Define Workspace and bind it to stream](#-define-workspace-and-bind-it-to-stream)
               * [   Verify Workspace](#---verify-workspace)
         * [  Add](#--add)
         * [  Copy](#--copy)
            * [Populate empty stream](#populate-empty-stream)
            * [ Undo copy](#-undo-copy)
      * [ File-level tasks](#-file-level-tasks)
         * [ Sync](#-sync)
            * [ Sync prior version](#-sync-prior-version)
            * [  Sync folder](#--sync-folder)
         * [  Add](#--add)
         * [  Delete](#--delete)
   * [ Shelves](#-shelves)
      * [ Suggested edits](#-suggested-edits)
      * [  P4V](#--p4v)
         * [<a href="https://www.perforce.com/manuals/p4v/Content/P4V/files.shelve.html" rel="nofollow"> Shelve Files</a>](https://www.perforce.com/manuals/p4v/Content/P4V/files.shelve.html)
         * [  Unshelve Files](#--unshelve-files)
      * [ Arrows](#-arrows)

<!-- Created by https://github.com/ekalinin/github-markdown-toc -->
<!-- Added by: richard, at: Mon 29 Apr 16:32:43 JST 2024 -->

<!--te-->

 Commands
----------

###  Start up shared server
1. Make server executable
  ```sh
  chmod +x /usr/local/bin/p4d
  chmod +x /usr/local/bin/p4
  ```
2.  Start up server
  ```sh
  p4d -r /path/to/myserver
  ```

###  Log in
####  Unix
```sh
export P4PORT=server1:1666
export P4USER=your.username
```
####  Windows
```powershell
p4 set P4PORT=server1:1666
p4 set P4USER=your.username
```

### 🗺️  Typemap
How you want to handle files with specific file extensions or within certain folders.
``` sh
p4 typemap
```

#### File Types
- `text`: Store as text, with revisions as reverse deltas.
- `binary`: Files are stored as binary, with revisions stored as complete files.
            Binary files are **compressed** on the server by default to save space.

### 🗺️  Workspace Mappings
- Map depo to workspace
- Like `gitignore`, you can set it to ignore certain directories or filetypes

##### ❌Do not add in from Unreal project
- `.vs`
- `intermediate`
- `DerivedDataCache`

###  Check connection
```sh
p4 info
```
> If `host:port` is `perforce:1666` then `P4PORT` is not set

#### Trust connection
```sh
p4 trust
```
> Saves fingerprint to filepath in `P4TRUST` if set but if unset it is saved to `~/.p4trust`

### [ Stream](https://www.perforce.com/video-tutorials/vcs/what-perforce-streams)
Is how **branching** and **merging** is done with Helix Core version control. 
- master/main -> mainline
- Streams know they are reated to eachother as opposed to branches
- Merge down/copy up

####  New Stream
##### Create a mainline stream
```sh
p4 stream -t mainline //MyCode/main
```
######   Verify Stream
```sh
p4 streams //MyCode/...
```

#####  Define Workspace and bind it to stream
```sh
p4 client -S //MyCode/main
```
######    Verify Workspace
```sh
p4 clients //MyCode/main
```
####   Add
```sh
p4 add myfile
```

####   Copy
> like git push
```sh
p4 copy -v //mysourcedepot/mainline/... //ProjectX/main/...
p4 opened
p4 submit
```
`-v`: Copy on server without syncing the newly-created files to the workspace

##### Populate empty stream
```sh
p4 populate //mysourcedepot/mainline/... //ProjectX/main/...
```

#####  Undo copy
```sh
p4 revert //ProjectX/main/...
```
###  File-level tasks
####  Sync
> Adds, updates, or deletes files
```sh
p4 sync
```
#####  Sync prior version
```sh
p4 sync //Acme/dev/jam/Jamfile#1
```
#####   Sync folder
```sh
p4 sync //Acme/dev/jam/...
```

####   Add
```sh
p4 add myfile
p4 submit
```
> View Worspace Mappings

####   Delete
```sh
p4 delete myfile
```

--------------------------------------------------------------

 Shelves
---------
- Store edits in a changelist without submitting
- Can be **unshelved** by you or other **team members**
- **Restores** an earlier version of the project in the **stream**
- Ensures that you won't accidently lose any progress
###  Suggested edits
- You can make suggested edits and self them as a *change list*
- The target team member can **unshelve** the *change list* in their workspace

###   P4V
#### [ Shelve Files](https://www.perforce.com/manuals/p4v/Content/P4V/files.shelve.html)
> enables you to store copies of open files temporarily in the shared Helix Server
repository(pending changelist) without checking them in.
> DO NOT shelve unchanged files

1.  Checkout shelved files to be shelved
1.  Right click change list
2.  Shelve Popup will open
3. Choose files to shelve
4. Separate into a new numbered changelist beforehand
5.  In general use defaults: Any shelved files will be reverted after submitting
  change list for shelving
  - Uncheck **Revert checked out files after they are shelved** if you are still
  working on the files from this change list.
6. When sheliving a change list you need to add a description
7.  Once shelved it will appear with a blue triangle with a folder icon in the
the upper left corner

####   Unshelve Files
1. Right click shelved change list
2.  Unshelve Popup will open
3.  In general use defaults: Any shelved files will be reverted after submitting
  - If other team members updated the files since the changes were shelved
  - Not to create any conflicts with these changes
4. Unshelved changelist now exists as a standard changelist

###  Arrows

| Color | Meaning |
| :---- | :------ |
| Green | More work needs to be done |
| Red | Changs ready to be propogated |
| Gray | No changes |
