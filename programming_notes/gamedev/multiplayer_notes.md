[Multiplayer Notes](https://hub.packtpub.com/getting-started-multiplayer-game-programming/)
=============================================================

Types
-----
### Peer-to-peer

- Two or more nodes are connected directly to each other
- Each **peer** serves the same function as every other one
- All consume and share the same data (synchronize)

#### Advantages
- **Fast data transmission:** Data is sent directly.
- **Simpler Setup:** Only need to handle one instance of the game (especially great
					for turn based).
- **More reliability:**  If one peer dies, the other peers can continue with no
						issues.
#### Disadvantages
- **Incoming data cannot be trusted:**  A server is more credible.
- **Fault tolerance can be very low:** Game crashing might effect other players.
- **Data duplication when broadcasting to other peers:**

### Client-server networking
- Server in charge of data and services.
- Players are not aware of each other. Just the server.
- Handle game logic on server OR on client OR hybrid(authoritative and non-authoritative servers)).
- Need to program both ends of the stack.

#### Advantages
- **Separation of concerns:** Writing individual components.
- **Centralization:** Easier to manage communications like rules, control access, etc.
- **Less work for client:** Server handles synchronizing data amongst clients.

#### Disadvantages
- **Communication takes longer to propagate:** Data needs to pass though the server
	instead of directly to the other client
- **More complexity due to more moving parts:** More to code.
- **Single point of failure and network congestion:** Server dies, game dies.


Networking protocols
--------------------
- Describes to the receiver of a message how the data is organized so that it can
	be decoded. 

### Transmission Control Protocol (TCP)
- provides built-in error checking mechanisms so that, if a packet is lost, the
	target application can notify the sender application, and any missing packets
	are sent again until the entire message is received.
- TCP is a connection-based protocol that guarantees the delivery of the full data
	in the correct order.
	
#### Flow
1. The source machine first establishes a connection with the destination machine
2. Data is transmitted in packets in such a way that the receiving application can
	then put the data back together in the appropriate order.

### User Datagram Protocol (UDP)
Transmits packets of data (called datagrams) without the use of a pre-established
connection
- very fast and frictionless way of sending data towards some target application.

### Network Sockets
Like HTTP

--------------------------------------------------------------------------------

https://forum.unity.com/threads/what-are-the-pros-and-cons-of-available-network-solutions-assets.609088/
