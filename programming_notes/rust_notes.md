# 🦀 Rust Notes

# Table of Contents

<!--ts-->

- [🚦Getting Started](🚦getting-started)
- [Idiomatic Rust](#idiomatic-rust)
  - [Idiomatic code](#idiomatic-code)
  - [📎clippy](#📎clippy)
    - [⚠️ Ignore warnings](#⚠️-ignore-warnings)
- [Documentation](#documentation)
  - [Command](#command)
  - [Syntax](#syntax)
    - [Outer documentation comment](#outer-documentation-comment)
    - [Inner documentation comment](#inner-documentation-comment)
- [Publishing](#publishing)
- [📦 Cargo](#📦-cargo)
  - [🌟 New Project](#🌟-new-project)
  - [🌟 New Project Binary](#🌟-new-project-binary)
  - [🌟 New Library](#🌟-new-library)
- [ Syntax](#-syntax)
  - [💧 Variables](#💧-variables)
    - [Variable mutability](#variable-mutability)
    - [💡 Constants](#💡-constants)
  - [? if statement](#-if-statement)
  - [ Loop](#-loop)
    - [ For Loop](#-for-loop)
      - [ range](#-range)
    - [🔢 iter](#🔢-iter)
- [Modules/Library](#moduleslibrary)
  - [Structure](#structure)
- [Binaries](#binaries)
  - [Structure - Binary using project name](#structure---binary-using-project-name)
  - [Structure - Binaries with custom names](#structure---binaries-with-custom-names)
- [Notes](#notes)
- [❌Custom Errors](#❌custom-errors)
  - [Non-Exhaustive](#non-exhaustive)
  - [Debug + Display + Error Traits](#debug--display--error-traits)
  - [thiserror](#thiserror)
  - [Non-Recoverable Error](#non-recoverable-error)
    - [panic](#panic)
  - [Recoverable Error](#recoverable-error)
    - [🤚Handle Recoverable Error](#🤚handle-recoverable-error)
    - [↩️ Return Recoverable Error](#️↩️-return-recoverable-error)
- [Unit Testing](#unit-testing)
  - [Cargo Command](#cargo-command)
  - [Souce Code](#souce-code)
  - [Doc-tests](#doc-tests)
- [🤝Integration Tests](#🤝integration-tests)
- [🏋️ Benchmarks](#️🏋️-benchmarks)
  - [Criterion](#criterion)
    - [Cargo Command](#cargo-command-1)
    - [Souce Code](#souce-code-1)
- [🌲Logging](#🌲logging)
  - [Outputting logs](#outputting-logs)
  - [env_logger](#env_logger)
- [💰Borrowing / ゙Ownership](#💰borrowing--゙ownership)
  - [Simple Variable](#simple-variable)
  - [Pass values to functions](#pass-values-to-functions)
    - [Immutable value](#immutable-value)
    - [Mutable value](#mutable-value)
  - [🧶 Strings](#🧶-strings)
    - [🍰 Slice Type &amp;str](#🍰-slice-type-str)
    - [🧶 String](#🧶-string)
    - [🈷️ grapheme](#️🈷️-grapheme)
  - [Arrays](#arrays)
  - [Vectors](#vectors)
  - [HashMap](#hashmap)
  - [Struct](#struct)
  - [Traits](#traits)
    - [Derivable Traits](#derivable-traits)
      - [Debug](#debug)
      - [Clone](#clone)
      - [Copy](#copy)
      - [Default](#default)
      - [PartialEq / Eq](#partialeq--eq)
      - [From / Into](#from--into)
  - [Enums](#enums)
    - [Storing Data into Enums](#storing-data-into-enums)
    - [Enum with genertics](#enum-with-genertics-)
    - [🎴Match](#🎴match)
    - [Result Enum](#result-enum)
  - [Closures](#closures)
    - [Closures - Functional style programming](#closures---functional-style-programming)
      - [Iterators](#iterators)
      - [into_iter](#into_iter)
      - [Iterator types](#iterator-types)
      - [drain](#drain)
  - [turbofish](#turbofish)
  - [Threads](#threads)
    - [📺 Channels](#📺-channels)
      - [Channel Types:](#channel-types)
      - [Example](#example)
- [Test](#test)
  - [Test for a pointer equivalence](#test-for-a-pointer-equivalence)
- [Links](#links)

<!--te-->

## 🚦Getting Started

> https://www.rust-lang.org/learn/get-started

```sh
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Idiomatic Rust

### Idiomatic code:

Uses language-specific features, syntax, and patterns that are **considered natural
and standard for that language**

### `rustfmt` (Rust Format)

Automatically reformats source code to make it clean and idiomatic

```rust
cargo fmt
```

Can be customized by adding `.rusfmt.toml` config at project root

### 📎clippy

> linter

```rust
cargo clippy
```

Compiles code and checks for 450 specific type of problems:

1. **Style**: Shows if there is a more idiomatic way
2. **Correctness**: Code that compiles but is useless
3. **Complexity**: Remove unecessary complexity
4. **Performance**: Warns about inefficient code

#### ⚠️ Ignore warnings

Example of an **allow** attribute

```rust
#[allow(clippy::too_many_arguments)]
fn sum(
	first: u8,
	second: u8,
	third: u8,
	fourth: u8,
	fifth: u8,
	sixth: u8,
	seventh: u8,
	eight: u8,
) -> u8 {
	42
}
```

## Documentation

> How to generate documentation of your project

### Command

```rust
cargo doc --no-deps --open
```

- `--no-deps`: only your library documentation
- `--open`: Automatically open index page of documentation in browser

### Syntax

- **Markdown** can be used

#### Outer documentation comment

- Documentation comment requires 3 slashes `///` or `/** my block comment **/`

```rust
/// Number of pieces in the puzzle
///
/// # History
///
/// This is a separate paragraph
/// - Clickable documentation link: [`PUZZLE PIECES`]
/// - Other type of link: [Spawn a thread](std::thread::spawn)
/// - We tried `7`, but this is better
pub const PUZZLE_PIECES: u32 = 42
```

#### Inner documentation comment

- Documentation comment requires 3 slashes `//!` or `/*! my block comment !*/`

```rust
//! Hi! I'm your friendly Rust Puzzle
//! Library documentation. Please
//! come in, sit down, and have a cup
//! of hot chocolate
```

## Publishing

> Anything published is PERMINANT. (Do not include passwords, etc)

1. Login crates.io with github account
2. Account settings -> API Access
3. `cargo login`
4. `cargo publish`

All published projects will get documentation added to [docs.rs](https://docs.rs/)

## 📦 Cargo

- `cargo build`: Downloads all dependencies for a project and compiles them, and
  then compiles your program.
- `cargo update`: Will fetch new versions of the crates you listed in your **cargo.toml** file.
- `cargo clean` Can be used to delete all of the intermediate work files for your project,
  freeing up a bunch of disk space.
- `cargo verify-project`: Will tell you if your Cargo settings are correct.
- `cargo install`: Can be used to install programs via Cargo.

### 🌟 New Project

> `cargo init`: Creates a new project. If you really _don't_ want to be using _git_,
> you can type `cargo init --vcs none (projectname)`.

```sh
cargo init hello-rust
```

### 🌟 New Project Binary

```sh
cargo new my-binary --bin
```

### 🌟 New Library

```sh
cargo new my-library
```

##  Syntax

- `use`: Equivalent to C\#'s `using`
- `impl GameState for State`: `State` structure implements the **trait** `GameState`.
  its are like interfaces or base classes in other languages:
  they setup a structure for you to implement in your
  own code, which can then interact with the library that
  provides them`
- `#[derive(x)]`: is a macro that says "from my basic data, please derive the boilerplate
  needed for x". The macro generates the additional code for you.
- `'a`, `'r`, etc: A lifetime approximates the span of execution during which the data a reference
  points to is valid. The Rust compiler will conservatively infer the shortest
  lifetime possible to be safe. If you want to **tell the compiler that a reference
  lives longer than the shortest estimate**, you can name it, saying that the
  output reference, for example, has the same lifetime as a given input reference
- `_myVar`: The underscore tells Rust "we know we aren't using it, this isn't a
  bug!"
- `mymacro!`: Procedural macros run like a function - they define a procedure, they
  just greatly reduce your typing.

### 💧 Variables

```rust
// declare multple variables
let (bunnies, carrots) = (8, 10);
```

#### Variable mutability

- Variables in rust are **inherently immutable**, **explicitly `mut`able**
- Immutable variables are **safer** and can be shared between threads **without locks**
- The compiler can do extra optimizations on immutable variables

#### 💡 Constants

- Must always declare the type
- Must be a value determined at compile time

```rust
const WARP_FACTOR: f64 = 9.9;
```

### ? if statement

- They are expressions therefore can return values as tail expressions
- if the if returns a value it needs a `;` at the end of the statement (afteer `}`)

```rust
msg = if num == 5 {
	"five" //no return keyword
} else if num == 4 {
	"four" //no return keyword
} else {
	"other" //no return keyword
}; // needs semicolon when returning a value
```

###  Loop

```rust
'bob: loop {
	loop {
			loop {
				break 'bob;
			}
	}
}

'susan: loop {
	loop {
			loop {
				continue 'susan;
			}
	}
}
```

####  For Loop

#####  range

```rust
for i in 0..10 {
	// Code
}
```

> If you want to include "10"

```rust
for i in 0..=10 {
	// Code
}
```

#### 🔢 iter

```rust
for num in [7, 8, 9].iter() {
	// Code
}
```

```rust
let array = [(1,2), (3,4)]
for (x, y) in array.iter() {
	// Code
}
```

## Modules/Library

### Structure

- myproject
  - src
    - lib.rs
    - mymodule
      - mysubmodule.rs
    - mymodule.rs

Sub modules are defined in modules (recursively)

## Binaries

### Structure - Binary using project name

- myproject
  - src
    - bin
      - main.rs

### Structure - Binaries with custom names

- myproject
  - src
    - bin
      - bar.rs
      - baz.rs

```rust
myproject::mymodule::mysubmodule
```

## Notes

- Rust has no classes
- Variables in rust are inherently immutable, you must explicitly make them `mut`able in order to modify them
- Reference to self must be explicitly set to `mut`able if it is being modified, repend with a `&` to make it a reference instead of a clone/copy
- Lambdas are called closures in rust
- **Tail Expression**: Rust functions always return the last value, even if the `return` keyword is not written
- Use `::` to call functions without access to self, and use `.` with access to self
- Rust `match` cases doesn't like strings

- `*const _` is used for a pointer comparison
  ```rust
  if (gameobject as *const _) != (&gameobjects[PLAYER] as *const _) {
  	println!("The {} growls!", object.name);
  }
  ```

  ## ❌Custom Errors
  - Make them enums
  ```rust
  pub enum PuzzleError {
  	WontFit(u16),
  	MissingPiece,
  }
  ```
- Group errors

  ```rust
  pub enum PuzzleError {
  	WontFit(u16),
  	MissingPiece,
  }
  ```
  - Only return YOUR errors (not from other libraries)
    - returning standard library errors is ok

### Non-Exhaustive

Cannot do a match expression without a wild card `_`

```rust
#[non_exhaustive]
pub enum PuzzleError {
	WontFit(u16),
	MissingPiece,
}
```

### Debug + Display + Error Traits

Implement in above order

```rust
#[derive(Debug)]
#[non_exhaustive]
pub enum PuzzleError {
	WontFit(u16),
	MissingPiece,
}

use std::fmt::{Display, Formatter};

// Error Message
impl Display for PuzzleError {
	fn fmt(&self, f: &mut Formatter) -> std::fmt::Result {
		use PuzzleError::*;
		match self {
			MissingPiece => write!(f, "Missing a piece"),
			WontFit(n) => write!(f, "Piece {} doesn't fit!", n),
		}
	}	
}

use std::error::Error;

// There is no derive for Error
impl Error for PuzzleError {}
```

### thiserror

Is a package that helps you avoid the above boilerplate

````rust
use thiserror::Error;

#[derive(Debug, Error)]
#[non_exhaustive]
pub enum PuzzleError {
	#[error("Piece {0} doesn`t fit!")]
	WontFit(u16),
	#[error("Missing a piece")]
	MissingPiece,
}


⚠️ Error Handling
----------------
The {?}[? operator] is a convenient way of saying "If this failed, return from the function with error immediately, otherwise give me the success value". The operator also does an extra conversion to the error type specified in the functions return value.

So `let mut file = File::create("savegame")?;` is almost equivalent to

``` rust
fn save_game(game: &Game, objects: &[Object]) -> Result<(), Box<dyn Error>> {
  ...
  let mut file = match File::create("savegame") {
      Ok(f) => f,
      Err(e) => return Err(e)
  };
  ...
}
````

### Non-Recoverable Error

No way the program can proceed

#### panic

- Do not `panic` in libraries
- Do not try to catch `panic`s

```rust
//Manually panic
panic!("Your computer is on fire");
// Same thing if result is a Result::Err
result.expect("Your commputer is on fire");
// The same but without a message
result.unwrap();
```

### Recoverable Error

#### 🤚Handle Recoverable Error

```rust
// Handling error
if let Err(e) = my_result {
	println!("Warning: {}", e)
}
let score = match get_saved_score() {
	Ok(x) => x,
	Err(_) => 0,
};
// shorter version of above
let score = get_saved_score().unwrap_or(0);
```

#### ↩️ Return Recoverable Error

```rust
fn poem() => Result<String, io::Error> {
	let file = match File::open("pretty_words.txt") {
		Ok(f) => f,
		Err(e) => return Err(e),
	};
	// Do stuff with file
}
```

##### `?`: The try operator

```rust
fn poem() => Result<String, io::Error> {
	// This is the same as the above match statement
	let file = File::open("pretty_words.txt")?;
	// Do stuff with file
}
```

```rust
pub fn autobots_rollout() => Result<Vehicle, TransformerError> {
	let optimus = Transformer::new();
	// All these have the same error types
	optimus.stand()?.transform()?.rollout()?.chase()?
}
```

##### `anyhow` package

> Alternatives: eyre, snafu

```rust
use anyhow::{Context, Result};
use puzzles::Puzzle;
use std::fs::File;

fn get_puzzle(filename: &str) -> Result<Puzzle> {
	let fh = File::open(filename)
									.with_context(|| format!("couldn't open the puzzle file {}", filename))?;
	let puzzle = Puzzle::from_file(fh).context("couldn't convert data into a puzzle")?;
	Ok(puzzle)
}

fn main() -> Result<()> {
	let puzzle = get_puzzle("puzzle.dat").context("Couldn't get the first puzzle")?;
	println!("Playing puzzle: {}", puzzle.name);
	Ok(())
}
// Error: Couldn't get the first puzzle
// Caused by:
//  0: couldn't convert data into a puzzle
//  1: Missing a piece
```

## Unit Testing

### Cargo Command

```sh
cargo test
# run specific test
cargo test test::bunny_result
```

### Souce Code

```rust
pub fn snuggle(bunnies: u128) -> u128 {
	bunnies = 8
}

// Config module that only compiles the following when running tests
// It is ignored otherwise
#[cfg(test)]
mod test{
	// import above modules
	use super::*;

	// should be run by the test runner
	// No parameters
	#[test]
	fn snuggle_bunnies_multiply() {
		assert_eq!(snuggle(2), 16);
	}

	#[should_panic]
	#[test]
	fn scared_bunny() {
		panic!("Hop hoppity hop!");
	}

	#[test]
	fn scared_result() -> Result<(), ParseIntError> {
		let num_bunnies: u64 = "four".parse()?;
		assert_eq!(num_bunnies, 4);
		Ok(())
	}
}
```

### Doc-tests

Tests in the documentation (of libraries)

> hashes in the documented code are likes of code that will not be displayed
> in the documentation
> below quotes should be "`" since it messes with markdown

```rust
/// # Example
///
/// '''
/// # use hello::snuggle;
/// let bunnies = snuggle(5);
/// assert_eq!(bunnies, 40);
/// '''
pub fn snuggle(bunnies: u128) -> u128 {
	bunnies * 8
}
```

## 🤝Integration Tests

- All integration tests are in the `tests` directory
- Tests the integration of multiple components

```rust
use hello:snuggle;

// like unit tests
#[test]
fn it_works_from_outside() {
	assert!(snuggle(4) == 32);
}
```

## 🏋️ Benchmarks

### Criterion

- All benchmarks are in the `benches` directory

#### Cargo Command

```sh
cargo bench
```

#### Souce Code

Cargo.toml

```toml
# Dev dependencies do net compiled in release file
[dev-dependencies]
criterion = { version = "0.3", features = ["html_reports"] }
# html_reports generate to target/criterion/report/index.html

# can have more than 1 "bench" sections inside cargo.toml
[[bench]]
# The name of the rust source file which will hold benchmarks
name = "snuggle_speed"
# enable or disable internal benchmarking
harness = false
```

```rust
use criterion::{black_box, criterion_group, criterion_main, Criterion};
use hello::snuggle;

pub fn snuggle_benchmark(c: &mut Criterion) {
	// put literals in black_box to avoid compiler doing optimizations
	c.bench_function("snuggle 2", |b| b.iter(|| snuggle(black_box(2))));
}

criterion_group!(benches, snuggle_benchmark);
criterion_main!(benches);
```

## 🌲Logging

- Not inbuilt in `std`

### `log` library

```rust
use log::{error, warn, info, debug, trace};

error!("Serious stuff");
warn!("Pay attention"); // defaults to name of module you are in
warn!(target: "puzzle", "Pay attention");
warn!("Pay attention, minion {}", 352);
info!("Useful info");
debug!("Extra info");
trace!("All the things");
```

#### Outputting logs

- You need a separate library to output logs (there are different types, like outputting to cloud)

##### `env_logger`

- `env_logger` which outputs to terminal
- Default log level is **ERROR**

```rust
fn main() -> Result<()> {
	env_logger::init();
	// Logs go here
}
```

## 💰Borrowing / ゙Ownership

### Simple Variable

```rust
let s1 = String::from("abc");
let s2 = s1; // s1's contents is transfered to s2 making s1 uninitialized/unusable
let s3 = s2.clone(); // copy contents of s2 into s3
```

### Pass values to functions

#### Immutable value

```rust
let s1 = String::from("abc");
do_stuff(&s1); // Pass reference so that s1 does not lose it's value

fn do_stuff(s: &String) {
	// do stuff
}
```

#### Mutable value

> You can only have 1 mutable reference to the same variable

```rust
let mut s1 = String::from("abc");
do_stuff(&mut s1); // Pass reference so that s1 does not lose it's value

fn do_stuff(s: &String) {
	s.insert_str(0, "Hi, ");
}
```

- Reference must always be valid. Ie do not reference variables that will go out of scope.

### `&mut` mutable reference

- It can only be borrowed by one variable to avoid [[programming_terminology#Data Race|Data Races]]. It can be borrowed
  again once the variable borrowing it is out of scope.
- You **cannot borrow** a **mutable** reference if it is **immutably borrowed**
  before hand

```rust
fn main() {
    let mut s = String::from("hello");
    // A variable can be immutably borrowed by multiple variables 
    let r1 = &s;
    let r2 = &s;
    
    println!("{}, {}", r1, r2);
    // r1 and r2 are now out of scope so s can now be mutably borrowed   
    let r3 = &mut s;
    println!("{}", r3);
}
```

### 🧶 Strings

There are 6 types of strings but 2 are the most important

#### 🍰 Slice Type `&str`

- A list reference to part of an array in the case of `&str` part of a **String**
- It has **pointer** and **length**

```rust
let s = String::from("hello");

let slice = &s[0..2];
let slice = &s[..2]; // Same as above
let slice = &s[1..]; // Until end of string
let slice = &s[..]; // All string
```

#### 🧶 String

- Like &str but has **capacity** as well as **length**

#### 🈷️ grapheme

A collecion of scalars for characters like **hongul**

### Arrays

```rust
let buf_infer = [1, 2, 3];
let buf_howmany = [0; 3];
let buf_typed: [u8; 3] = [1, 2, 3];
```

Arrays have **max** size of **32**

### Vectors

Dynamic, growable arrays that can store elements of the same data type.

```rust
let mut Vec<i32> = Vec::new();
v.push(2);
v.push(4);
v.push(6);
let x = v.pop(); // x is 6
println!("{}", v[1]); // prints 4
```

> Making vectors from literal values

```rust
let mut v = vec![2, 4, 6];
```

- If you mutable borrow from an object in a vector, all other objects will be considered mutably borrowed.
  The below will not work:
  ```rust
  let player = &mut objects[PLAYER];
  let borrowed_objects = &objects;
  player.move_by(1, 0, &map, objects)
  ```

### HashMap

```rust
let mut h: HashMap<u8, bool> = HashMap::new();
h.insert(5, true);
h.insert(6, false);
// return value from hashmap
let have_five = h.remove(&5).unwrap();
```

### Struct

- Do not have inheritence!!!

```rust
struct RedFox {
	enemy: bool,
	life: u8,
}

impl RedFox {
	fn new() -> Self {
		Self {
			enemy: true,
			life: 70,
		}
	}
}

let fox = RedFox::new();
let life_left = fox.life;
fox.enemy = false;
fox.some_method();
```

### Traits

- Instead of inheritence for structs
- Traits can inherit from each other
- No fields
- `impl`ement functionality to:
  - `struct`
  - `closure`
  - `enum`
  - `function`
- A `struct` can have multiple `impl` blocks
- This has real-life implications as Rust’s pointers to traits are twice the size of pointers to structs.
- The `dyn` keyword in `&mut dyn Console` highlights that Console is a `trait`
- Implementing a trait:

```rust
struct RedFox {
	enemy: bool,
	life: u32,
}

trait Noisy {
	// Must include the following
	fn get_noise(&self) -> &str;
}

impl Noisy for RedFox {
	fn get_noise(&self) -> &str { "meow?" };
}

// Function accepts any struct that has the noisy trait
fn print_noise<T: Noisy>(item: T) {
	println!("{}", item.get_noise());
}

impl Noisy for u8 {
	fn get_noise(&self) -> &str { "BYTE!" }
}

fn main() {
	print_noise(5_u8); // prints "BYTE!"
}
```

#### Derivable Traits

##### Debug

```rust
#[derive(Debug)]
pub struct Puzzle {
	pub num_pieces: u32,
	pub name: String,
}

println!("{:?}", puzzle); // Debug
// Puzzle { num_pieces: 500, name: "Draconic Equestrian" }
println!("{:#?}", puzzle); // Pretty debug
// Puzzle {
//		num_pieces: 500, 
//		name: "Draconic Equestrian" 
// }
```

##### Clone

```rust
#[derive(Clone)]
pub struct Puzzle {
	pub num_pieces: u32,
	pub name: String,
}

let puzzle2 = puzzle.clone();
```

##### Copy

- Subtrait of `Clone`
- Like `Clone` but only for values in stack not heap

```rust
#[derive(Clone, Copy)]
pub enum PuzzleType {
	Jigsaw
}
```

##### Default

- Can be derived but returns only empty values

```rust
#[derive(Default)]
```

- Implementing default

```rust
impl Default for Puzzle {
	fn default() -> Self {
			Puzzle {
				num_pieces: PUZZLE_PIECES,
				name: "Forest Lake".to_string(),
			}
	}
}

let puzzle = Puzzle {
	num_pieces: 3000,
	..Default::default() // fill rest of values with defaults
}
```

##### PartialEq / Eq

```rust
impl PartialEq for Puzzle {
	fn eq(&self, other: &Self) -> bool {
		(self.num_pieces == other.num_pieces) &&
		(self.name.to_lowercase() == other.name.to_lowercase())
	}	
}

impl Eq for Puzzle {} // For example key in hashmmap
```

##### From / Into

If you implement `From`, then `Into` isautomatically implemented

```rust
From<Puzzle> for String
Into<String> for Puzzle
```

```rust
impl From<Puzzle> for String {
	fn from(puzzle: Puzzle) -> Self {
		puzzle.name
	}
}

let puzzle = Puzzle::default();
let s = String::from(puzzle);
let t: String = puzzle.into();
```

```rust
pub fn show<T: Into<String>>(s: T) {
	println!("{}", s.into());
}
let puzzle = Puzzle::default();
show(puzzle); // puzzle has been consumed :(
```

### Enums

```rust
enum Color {
	Red,
	Green,
	Blue,
}
let color = Color::Red;
```

#### Storing Data into Enums

```rust
enum DispenserItem {
	Empty,
	Ammo(u8),
	Things(String, i32),
	Place {x: i32, y: i32},
}

impl DispenerItem {
	fn display(&self) { }
}

use DispenserItem::*;
let item = Ammo(42);
let item2 = Place {x: 24, y: 48 };
```

#### Enum with genertics <T>

> Option enum is part of the standard library
> used instead of checking for null, check for "Some value"

```rust
enum Option<T> {
	Some(T),
	None,
}
```

#### 🎴Match

```rust
// If not null
if let Some(x) = my_variable {
	println!("value is {}", x)
}

match my_variable {
	// If not null
	Some(x) => {
		println!("value is {}", x);
	},
	// If null
	None => {
		println!("no value");
	}
}

let x = match my_variable {
	Some(x) => x.squared() + 1,
	None => 42,
// Make sure to add semicolon
};

let mut x: Option<i32> = Mone;
```

```rust
let mut x = None;
x = Some(5); // Infer type
x.is_some(); // true
x.is_none(); // false
for i in x {
	println!("{}", i); // prints 5
}
```

#### Result Enum

```rust
// must use the result or compilor complains. Basically
// to make sure to handle Err (Errors)
#[must_use]
enum Result<T, E> {
	Ok(T),
	Err(E),
}
```

### Closures

> Lambdas are called closures in rust

```rust
|params| expr
|params| { expr1 ; expr2 }
```

```rust
let add = |x, y| x + y;
add(1, 2); // returns 3
```

```rust
let s = "strawberry".to_string();
let f = || {
	println!("{}", s);
};

f(); // prints strawberry

// g takes ownership of s contents
let g = move || {
	println!("{}", s);
};
```

#### Closures - Functional style programming

> Example of Linq style lambdas

##### Iterators

> faster than for loops

```rust
let mut v = vec![2, 4, 6];

v.iter() // the following are iterator adapters
	.map(|x| x * 3) // transforms x to x * 3
	.filter(|x| *x > 10)
	.fold(0, |acc, x| acc + x);
```

##### into_iter

> Takes ownership of whatever it is iterating (effectively destroying it)

```rust
let mut v = vec![6, 7, 8];
// for_each is an iterator consumer
v.into_iter().for_each(|num| println!("{}", num));
```

##### Iterator types

- `v.into_iter()`/`for _ in v`: consumes v, returns owned items
- `v.iter()`/`for _ in &v`: returns immutable references
- `v.iter_mut()`/`for _ in &mut v`: returns mutable references
- There are other types

##### drain

```rust
v.drain(..) // empty all items from v
```

### turbofish

Allows us to know what the return type can be (when it cannot be inferred)

```rust
::<>
::<i32>
.sum::<i32>()
```

### Threads

```rust
use std::thread;
fn main() {
	let handle = thread::spawn(move || {
		// do stuff in a child thread
	});
	// do stuff simultaneously in the main thread

	// wait until thread has exited
	handle.join().unwrap();
}
```

```rust
use log::{error, info};
use std::{thread, time::Duration};

fn sleep(seconds: f32) {
	thread::sleep(Duration::from_secs_f32(seconds));
}

pub mod dad {
	use super::{info, sleep};

	pub fn cook_spaghetti() -> bool {
		info!("Cooking the spaghetti...");
		sleep(4.0);
		info!("Spaghetti is ready!");
		true
	}
}

pub mod mum {
	use super::{info, sleep};

	pub fn cook_sauce_and_set_table() {
		sleep(1.0);
		info!("Cooking the sauce...");
		sleep(2.0);
		info!("Sauce is ready!");
		sleep(2.0);
		info!("Table is set!");
	}
}

fn main(){
	env_logger::init();
	let handle = thread::spawn(|| dad::cook_spaghetti());
	mum::cook_sauce_and_set_table();

	if handle.join().unwrap_or(false) {
		info!("Spaghetti time! Yum!")
	} else {
		error!("Dad messed up the spaghetti. Order pizza instead?");
	}
}
```

#### 📺 Channels

- Do not use `std::sync::mpsc` (Multiple Producer Single Consumer)
  - **Pros**:
    - In standard library `std`
    - No extra dependencies
  - **Cons**:
    - 🤌 Few features
    - Medium performance
- Use `crossbeam::channel` instead (add in `Cargo.toml`)
  - **Pros**:
    - Many features
    - 💪 High performance
  - **Cons**:
    - Not in standard library
    - Extra dependencies
- A channel is a one way queue to `Send` a value between threads

##### Channel Types:

- **⛓️ Bounded channel**
  - Fixed capacity
  - Once is full, the Sending thread will block
  - Sending thread will resume once a receiver thread pulls something from channel
    **⛓️‍💥 UnBounded channel**
  - Channel can grow indefinately
  - Good for thirsty loads

- Both channel types can have multiple receivers
- Which receiver will get the value is **undefined**
- Senders are first come first serve
- Flow can only go one direction (per channel)
- Using multiple channels to send two ways can lead to a **deadlock**

##### Example

```rust
use crossbeam::channel::{self, Receiver, Sender};
use std::{thread, time::Duration};

#[derive(Debug)]
enum Lunch {
	Soup,
	Salad,
	Sandwich,
	Hotdog,
}

fn cafeteria_worker(name: &str, orders: Receiver<&str>, lunches: Sender<Lunch>) {
	for order in orders {
		println!("{} receivers an order for {}", name, order);
		let lunch = match &order {
			x if x.contains("soup") => Lunch::Soup,
			x if x.contains("salad") => Lunch::Salad,
			x if x.contains("sandwich") => Lunch::Sandwich,
			_ => Lunch::HotDog,
		};
		for _ in 0..order.len() {
			thread::sleep(Duration::from_secs_f32(0.1))
		}
		println!("{ sends a {:?}}", name, lunch);
		if lunches.send(lunch).is_err() {
			break;
		}
	}	
}

fn main() {
	let (orders_tx, orders_rx) = channel::unbounded();
	let orders_rx2 = orders_rx.clone();
	let (lunches_tx, lunches_rx) = channel::unbounded();
	let lunches_tx2 = lunches_tx.clone();

	let alice_handle = thread::spawn(|| cafeteria_worker("alice", orders_rx2, lunches_tx2));
	let zack_handle = thread::spawn(|| cafeteria_worker("zack", orders_rx, lunches_tx));

	for order in vec!["polish dog", "ceasar salad", "onion soup", "reuben sandwich"] {
		println!("ORDER: {}", order);
		let _ = orders_tx.send(order);
	}
	drop(orders_tx);

	for lunch in lunches_rx {
		println!("Order Up! -> {:?}", lunch);
	}

	let _ = alice_handle.join();
	let _ = zack_handle.join();
}
```

## Test

### Test for a pointer equivalence

You convert ordinary Rust references to raw pointers (`*const` Object in this case)
and compare those. This is safe because we never dereference them. As a bonus, the
compiler can infer the type (Object) so you can just say `_` and it will fill it in.

```rust
assert_eq!(&gameobjects[PLAYER] as *const _, &gameobjects[0] as *const _);
```

---

## Links

- [Roguelike Tutorial - In Rust](http://bfnightly.bracketproductions.com/rustbook/chapter_0.html)

```rust
for i in 0..10 {
	// Code
}
```
