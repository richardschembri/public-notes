Magic Leap Notes
================
[Unity Setup](https://developer.magicleap.com/learn/guides/develop-setup)
-----------
> *Magic Leap ID is required*

### Magic Leap Lab
1. Install [Magic Leap Lab](https://developer.magicleap.com/downloads)
2. Launch *The Lab* and select *Unity* in the  *Package Manager*
	- Be sure to select the simulator *Zero Iteration* from the list of packages.
	- The package manager should install the Lumin SDK

### Unity(Windows)
1. Download Unity 2019.2 with Lumin Build Support
2. Download the [Unity Project Template](https://github.com/magicleap/UnityTemplate/archive/master.zip)
from [Unity getting started page](https://developer.magicleap.com/learn/guides/get-started-developing-in-unity)
3. Open template in Unity and make sure the Lumin Build target is selected.
4. Go in unity preferences the *Lumin SDK path* is set in the *External Tools*
section.(C:/Users/<user>/MagicLeap/mlsdk/v0.22.0)
5. Open *Project Settings* and navigate to **Project->Player->Other Settings**
	- Set *Color Space* to "Linear"
	- Untick **Auto Graphics API for Windows**
	- Under **Graphics APIs for Windows** add "Open GL Core" and remove
	"Direct3D11"(Direct X)
	- Restart the editor

## Prerequisites (for Unity Editor 2019.3.x)
1. **Edit** -> **Project Settings**.
2. **Magic Leap** -> **Manifest Settings**
	- GestureConfig
	- GestureSubscribe
	- LowLatencyLightwear
3. **File** -> **Build Settings** -> **Player Settings** -> **Settings for Lumin** -> **Other Settings** -> **Identification**
4. Set **Bundle Identifier**
5. **Other Settings** -> **Configuration**
6. Set **Scripting Runtime Version** to *.Net.4.x Equivalent*
7. **Magic Leap** -> **ML Remote** -> **Import Support Libraries**

## Magic Leap Package Manager
1. Launch Magic Leap Remote
> **Magic Leap Remote** is either a simulator or a zero iteration bridge
from unity to the device

# Simulator controls
<img src="img/mlremote_keybinding_movement.png" />

## [Privileges](https://developer.magicleap.com/en-us/learn/guides/privileges-in-unity)
> App Manifest
> When using Zero Iteration, all privileges are assumed to be granted
- **Auto-granted** - Automatically granted to apps
- **Sensitive** - Requires user consent on first use
- **Reality** - Requires user consent for each active session.
Check and handle privileges in source code.

------------------------------------------------------------------------

# Unity Development
> It is recommended that you use the Main Camera core component of the Magic Leap Unity Package.
``` csharp
using UnityEngine.XR.MagicLeap;
```

## Add Sound
- **Add Component** -> **MSA Source**
- **Add AudioClip**

## User Input
### Control Input
#### C6 DOF (Six Degrees of Freedom)
``` csharp
_controller = MLInput.GetController(MLInput.Hand.Left);
```

### Control Raycast
``` csharp
void Start(){
	// Start accessing the ML World Ray API.
	MLRaycast.Start();
}

// When the prefab is destroyed, stop MLWorldRays API from running.
private void OnDestroy() {
	MLRaycast.Stop();
}

// Update is called once per frame
void Update() {
	// Create a raycast parameters variable
	MLRaycast.QueryParams _raycastParams = new MLRaycast.QueryParams {
		// Update the parameters with our Camera's transform
		Position = ctransform.position,
			 Direction = ctransform.forward,
			 UpVector = ctransform.up,
			 // Provide a size of our raycasting array (1x1)
			 Width = 1,
			 Height = 1
	};
	// Feed our modified raycast parameters and handler to our raycast request
	MLRaycast.Raycast(_raycastParams, HandleOnReceiveRaycast);
}

void HandleOnReceiveRaycast(MLRaycast.ResultState state, UnityEngine.Vector3 point, Vector3 normal, float confidence) {
	if (state == MLRaycast.ResultState.HitObserved) {
		// Handle hit
	}
}
```

### Touchpad Gestures
``` csharp
void Start() {
	Input.Start();
	_controller = MLInput.GetController(MLInput.Hand.Left);
}

void OnDestroy() {
	MLInput.Stop();
}

void updateGestureText() {
	string gestureType = _controller.CurrentTouchpadGesture.Type.ToString();
	string gestureState = _controller.TouchpadGestureState.ToString();
	string gestureDirection = _controller.CurrentTouchpadGesture.Direction.ToString();}
}
```

## Hand & Mesh Tracking
> 15 Key points on each hand
<img src="handtracking_15points.png" />

|    | Description                      | C API Name	 	|
| -- |:---------------------------------| ---------------------:|
| 1  |	Pinky finger tip         	| HAND_PINKY_TIP	|
| 2  |	Pinky finger joint, bottom	| HAND_PINKY_MCP	|
| 3  |	Ring finger tip          	| HAND_RING_TIP		|
| 4  |	Ring finger joint, bottom	| HAND_RING_MCP		|
| 5  |	Middle finger tip		| HAND_MIDDLE_TIP	|
| 6  |	Middle finger joint, middle	| HAND_MIDDLE_PIP	|
| 7  |	Middle finger joint, bottom	| HAND_MIDDLE_MCP	|
| 8  |	Index finger tip		| HAND_INDEX_TIP	|
| 9  |	Index finger joint, middle	| HAND_INDEX_PIP	|
| 10 |	Index finger joint, bottom	| HAND_INDEX_MCP	|
| 11 |	Thumb tip			| HAND_THUMB_TIP	|
| 12 |	Thumb joint, middle		| HAND_THUMB_IP		|
| 13 |	Thumb joint, bottom		| HAND_THUMB_MCP	|
| 14 |	Hand, center			| HAND_HAND_CENTER	|
| 15 |	Wrist, center			| HAND_WRIST_CENTER	|

### Hand Tracking

``` csharp
public enum HandPoses { Ok, Finger, Thumb, OpenHand, Fist, NoPose };
public HandPoses pose = HandPoses.NoPose;
public Vector3[] pos;
public GameObject sphereThumb, sphereIndex, sphereWrist;

private MLHandTracking.HandKeyPose[] _gestures;

private void Start()
{
	MLHandTracking.Start();
	_gestures = new MLHandTracking.HandKeyPose[5];
	_gestures[0] = MLHandTracking.HandKeyPose.Ok;
	_gestures[1] = MLHandTracking.HandKeyPose.Finger;
	_gestures[2] = MLHandTracking.HandKeyPose.OpenHand;
	_gestures[3] = MLHandTracking.HandKeyPose.Fist;
	_gestures[4] = MLHandTracking.HandKeyPose.Thumb;
	MLHandTracking.KeyPoseManager.EnableKeyPoses(_gestures, true, false);
	pos = new Vector3[3];
}

private void OnDestroy()
{
	MLHandTracking.Stop();
}


private void Update()
{
	if (GetGesture(MLHandTracking.Left, MLHandTracking.HandKeyPose.Ok))
	{
		pose = HandPoses.Ok;
	}
	else if (GetGesture(MLHandTracking.Left, MLHandTracking.HandKeyPose.Finger))
	{
		pose = HandPoses.Finger;
	}
	else if (GetGesture(MLHandTracking.Left, MLHandTracking.HandKeyPose.OpenHand))
	{
		pose = HandPoses.OpenHand;
	}
	else if (GetGesture(MLHandTracking.Left, MLHandTracking.HandKeyPose.Fist))
	{
		pose = HandPoses.Fist;
	}
	else if (GetGesture(MLHandTracking.Left, MLHandTracking.HandKeyPose.Thumb))
	{
		pose = HandPoses.Thumb;
	}
	else
	{
		pose = HandPoses.NoPose;
	}

	if (pose != HandPoses.NoPose) ShowPoints();
}

private void ShowPoints()
{
	// Left Hand Thumb tip
	pos[0] = MLHandTracking.Left.Thumb.KeyPoints[2].Position;
	// Left Hand Index finger tip
	pos[1] = MLHandTracking.Left.Index.KeyPoints[2].Position;
	// Left Hand Wrist
	pos[2] = MLHandTracking.Left.Wrist.KeyPoints[0].Position;
	sphereThumb.transform.position = pos[0];
	sphereIndex.transform.position = pos[1];
	sphereWrist.transform.position = pos[2];
}

private bool GetGesture(MLHandTracking.Hand hand, MLHandTracking.HandKeyPose type)
{
	if (hand != null)
	{
		if (hand.KeyPose == type)
		{
			if (hand.HandKeyPoseConfidence > 0.9f)
			{
				return true;
			}
		}
	}
	return false;
}

```

### Hand Meshing
> Requires **HandMesh** privilege.
#### HandMesh Prefab
**Assets** -> **Import package** -> **Custom package** : **MagicLeap.unitypackage**
	- MagicLeap -> Examples -> Materials -> HandMeshFlatWhite.mat
	- MagicLeap -> Examples -> Materials -> HandMeshOcclusion.mat
	- MagicLeap -> Examples -> Prefabs -> HandMesh.prefab

##### Properties
- **Mesh Block Prefab**: game object with a mesh filter and mesh renderer.
- **Mesh Material**: is the material applied on the meshes.
- **Recalculate Normals**: recalculate normals to better reflect updates made to the mesh as your hands move..
