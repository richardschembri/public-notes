File Paths
----------
### adb
#### Linux
`$HOME/Android/Sdk/platform-tools/adb`

[App Bundle Target texture compression](https://developer.android.com/guide/playcore/asset-delivery/texture-compression)
---------------------------------------
| Format | Description                                                                |
| :----- | :--------                                                                  |
| DXT    | Three forms of this format are supported by OpenGL                         |
| ETC2   | Supported by all devices that support GLES3                                |
| ASTC   | Recent format designed to supercede prior formats. Great for optimization. |
