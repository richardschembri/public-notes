> References: 

# Basic function with one argument
## Syntax:
```haskell
london u = "London " ++ u
```
* `london` is the function name
* `u` is a parameter
* The syntax after `=` is the function body 
## Definition
```haskell
:t london
```
### Output
```haskell
london :: [Char] -> [Char]
```
_london has input of `Char` and outputs `Char`_

## Function Call
```haskell
london "baby"
```
### Output
```haskell
"london baby"
```
