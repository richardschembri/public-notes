# Flutter
## Command line
#### Create new app
``` sh
flutter create myapp
```
## Widget
> Blueprints for elements like buttons and images, also things like padding

![Widget Element tree](diagram-flutter-widgetelement.png "Widget Element tree")

#### Composition
> Is when you are combining widgets together

### [Stateless Widget](https://api.flutter.dev/flutter/widgets/StatelessWidget-class.html)
> An immutable configuration/blueprint of an element
It is a widget composed of children and does not have a state that it needs to track.
#### State 
A value that changes over time:
##### Example:
- A textbox which has a string that the user updates.
- A widget that is animated that might have values that change.

``` dart
class Frog extends StatelessWidget {
  const Frog({
    Key key,
    this.color = const Color(0xFF2DBD3A),
    this.child,
  }) : super(key: key);

  final Color color;
  final Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(color: color, child: child);
  }
}
```

The main app itself is a **Stateless Widget**
``` dart
class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),

      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}
```

### [Statelful Widget](https://api.flutter.dev/flutter/widgets/StatefulWidget-class.html)
It is a widget composed of children that has a state that it needs to track.
An extra step to Stateless Widget
State can live even if the Widget is changed as long as it is the same type
Able to track data over time


### [Inherited Widget](https://api.flutter.dev/flutter/widgets/InheritedWidget-class.html)
Used for a group of deeply nested widgets and you need to pass data from top to the bottom efficiently.

You can get reference to it from any widget below it.

It is immutable, you need to rebuild the widget to change value..
> It means it cannot be reassigned but can still be changed internally.

### [Keys](https://flutter.dev/docs/development/ui/widgets-intro#keys)
Used for  *Stateful widgets*, if a widget is swapped out without a key the state is applied to it even if you intended to move it somewhere else. A key ensures that the state moves with the widget.

Keys need to be set at the top of the widget sub tree you need to preserve

Flutter matches a key with the level of the tree

There are multiple types of key

## [Platform Specific Implementation / Native Code Integration](https://flutter.dev/docs/development/platform-integration/platform-channels#step-4-add-an-ios-platform-specific-implementation)
Flutter uses a flexible system that allows you to call platform-specific APIs whether available in Kotlin or Java code on Android, or in Swift or Objective-C code on iOS.

### Flutter Platform
``` dart
import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
...
class _MyHomePageState extends State<MyHomePage> {
  static const platform = const MethodChannel('samples.flutter.dev/battery');

  // Get battery level.
}
```
``` dart
  // Get battery level.
  String _batteryLevel = 'Unknown battery level.';

  Future<void> _getBatteryLevel() async {
    String batteryLevel;
    try {
      final int result = await platform.invokeMethod('getBatteryLevel');
      batteryLevel = 'Battery level at $result % .';
    } on PlatformException catch (e) {
      batteryLevel = "Failed to get battery level: '${e.message}'.";
    }

    setState(() {
      _batteryLevel = batteryLevel;
    });
  }
```
``` dart
  @override
  Widget build(BuildContext context) {
    return Material(
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ElevatedButton(
              child: Text('Get Battery Level'),
              onPressed: _getBatteryLevel,
            ),
            Text(_batteryLevel),
          ],
        ),
      ),
    );
  }
```

### iOS Platform
Open **XCode** and navigate to the directory holding your Flutter app, and select the `ios` folder inside it.

Open `AppDelegate.swift`

``` swift
@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

    let controller : FlutterViewController = window?.rootViewController as! FlutterViewController
    let batteryChannel = FlutterMethodChannel(name: "samples.flutter.dev/battery",
                                              binaryMessenger: controller.binaryMessenger)
    batteryChannel.setMethodCallHandler({
      (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
      // Note: this method is invoked on the UI thread.
      // Handle battery messages.
    })

    GeneratedPluginRegistrant.register(with: self)
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
}
```

``` swift
private func receiveBatteryLevel(result: FlutterResult) {
  let device = UIDevice.current
  device.isBatteryMonitoringEnabled = true
  if device.batteryState == UIDevice.BatteryState.unknown {
    result(FlutterError(code: "UNAVAILABLE",
                        message: "Battery info unavailable",
                        details: nil))
  } else {
    result(Int(device.batteryLevel * 100))
  }
}
```

``` swift
batteryChannel.setMethodCallHandler({
  [weak self] (call: FlutterMethodCall, result: FlutterResult) -> Void in
  // Note: this method is invoked on the UI thread.
  guard call.method == "getBatteryLevel" else {
    result(FlutterMethodNotImplemented)
    return
  }
  self?.receiveBatteryLevel(result: result)
})
```

## Dart
#### Constructor
##### Traditional Constructor
``` dart
  TextSection(Color color) {
      this._color = color;
	}
```
##### Shortened Constructor
``` dart
TextSection(this._color);
```

##### Named Constructor
``` dart
Image.asset(...)
Image.file(...)
```

#### Keywords
- `final`: A variable that can only be **set once**

#### Code Convensions
##### Imports
- Imports should be in alphabetical order

Dart has named constructors like :


-----------------
## Links

[Flutter Course - Full Tutorial for Beginners](https://www.youtube.com/watch?v=pTJJsmejUOQ)

