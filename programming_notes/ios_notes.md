iOS Notes
=========

Provisioning Profile
--------------------
1. 

Publish to iOS App store Guide
------------------------------
### Apple Developer web page 
1. Go to https://developer.apple.com and login.
2. Go to *Account -> Certificates, Identifiers and Profiles*.
3. Go to *Identifiers -> App IDs* and click the **+** symbol.
4. Fill in name and select **Explicit App ID** so that you can fill in the **Bundle ID**.
5. Tick the **App Services** that your app uses (if any).
6. Click **Continue** and then **Done**.

### iTunes Connect web page
1. Go to https://itunesconnect.apple.com and login.
2. Go to *My Apps*.
3. Click **+** -> *New App*.
4. Fill in form and click **Create**.
5. Choose **Category**.
6. Go to *Pricing and Availability* and set up necessary info (Don't forget to click save).
7. Go to *Prepare for Submission* and fill in necessary info and upload requested images.
	- For screenshots just press **command + S**
8. Save

### XCode
1. Click *Product -> Archive*
2. Click **Validate**
3. Click **Upload to App Store**
4. **Done**
5. Go to *Apple Developer web Page* and  (**+**)add the build. *Build may take time to show*
6. Click submit for review once the build is added.
7. Fill in form that appears
8. Click **Submit**

--------------------------------------------------------------------------------

[Icons and Images](https://developer.apple.com/design/human-interface-guidelines/ios/icons-and-images/image-size-and-resolution/)
--------------------------------------------------------------
### App Icon Attributes
| Attribute   | Value                                                                       |
| :---------- | :-------------------------------------------------------------------------- |
| Format      | PNG                                                                         |
| Color space | Display P3 (wide-gamut color), sRGB (color), or Gray Gamma 2.2 (grayscale). |
| Layers      | Flattened with no transparency                                              |
| Resolution  | Varies. See Image Size and Resolution.                                      |
| Shape       | Square with no rounded corners                                              |

### App Icon Sizes

| Device or context | Icon size                       |
| :---------------- | :------------------------------ |
| iPhone            | 60x60 pt (180x180 px @3x)       |
|                   | 60x60 pt (120x120 px @2x)       |
| iPad Pro          | 83.5x83.5 pt (167x167 px @2x)   |
| iPad, iPad mini   | 76x76 pt (152x152 px @2x)       |
| App Store         | 1024x1024 pt (1024x1024 px @1x) |

### Spotlight, Settings, and Notification Icons

| Device                    | Spotlight icon size             |
| :------------------------ | :------------------------------ |
| iPhone                    | 40x40 pt (120x120 px @3x)       |
| 40x40 pt                  | (80x80 px @2x)                  |
| iPad Pro, iPad, iPad mini | 40x40 pt (80x80 px @2x)         |

| Device                    | Settings icon size              |
| :------------------------ | :------------------------------ |
| iPhone                    | 29x29 pt (87x87 px @3x)         |
|                           | 29x29 pt (58x58 px @2x)         |
| iPad Pro, iPad, iPad mini | 29x29 pt (58x58 px @2x)         |

| Device                    | Notification icon size          |
| :------------------------ | :------------------------------ |
| iPhone                    | 20x20 pt (60x60 px @3x)         |
| 20x20 pt                  | (40x40 px @2x)                  |
| iPad Pro, iPad, iPad mini | 20x20 pt (40x40 px @2x)         |

