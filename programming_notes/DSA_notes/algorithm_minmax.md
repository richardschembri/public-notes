[Minmax Algorithm](https://www.youtube.com/watch?v=KU9Ch59-4vw)
===========================================================
- 2 player games
- Max = Player 1 win, Min = Player 2 win
	- Player 1 will chose paths with Max points
	- Player 2 will chose paths with Min points
- Priority: Win -> Draw -> Loss
