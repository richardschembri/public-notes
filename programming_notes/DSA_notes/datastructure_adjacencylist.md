[Adjacency list](https://en.wikipedia.org/wiki/Adjacency_list)
====================================================================

Adjancency list
---------------

Is like a `linked list` but each node has links to all **adjecent** nodes.

**Example:**
> numVerts

| 0  | 1 | 2  | 3 | 4 | 5  | 6  |
| -- | - | -- | - | - | -  | -- |
| 17 | x | 11 | 4 | 3 | 0  | 26 |
| 5  |   | 5  |   |   | 2  | 9  |
| 14 |   |    |   |   | 12 |    |
|    |   |    |   |   | 32 |    |

