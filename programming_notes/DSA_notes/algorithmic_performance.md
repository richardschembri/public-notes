# O(1)
Describes an algorithm that will always execute in the same time (or space) regardless of the size of the input data set.
``` csharp
bool IsFirstElementNull(IList<string> elements)
{
    return elements[0] == null;
}
```

# O(N)
Describes an algorithm whose performance will grow linearly and in direct proportion to the size of the input data set.
``` csharp
bool ContainsValue(IList<string> elements, string value)
{
    foreach (var element in elements)
    {
        if (element == value) return true;
    }

    return false;
}
```

# O(N^2)
Represents an algorithm whose performance is directly proportional to the square of the size of the input data set.
``` csharp
bool ContainsDuplicates(IList<string> elements)
{
    for (var outer = 0; outer < elements.Count; outer++)
    {
        for (var inner = 0; inner < elements.Count; inner++)
        {
            // Don't compare with self
            if (outer == inner) continue;

            if (elements[outer] == elements[inner]) return true;
        }
    }

    return false;
}
```

# O(2^N)
``` csharp
int Fibonacci(int number)
{
    if (number <= 1) return number;

    return Fibonacci(number - 2) + Fibonacci(number - 1);
}
```

