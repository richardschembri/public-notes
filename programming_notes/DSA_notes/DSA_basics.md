Data Structures
===============
Different ways of storing data on your computer (ex. Arrays, Hashtables, Linked Lists, etc)

Data Structure Operations
-------------------------
- Insertion
- Deletion
- Traversal
- Searching
- Sorting

Static vs Dynamic
-----------------
### Static
#### Advantages
- Compiler an allocate space during compilation
- Easy to check for overflow
- Allows random access
#### Disadvantages
- Have to manually estimate the size
- Can potentially waste space

### Dynamic
- Only uses the space needed at any time
- Makes efficient use of memory
- Storage can be returned to the system

--------------------------------------------------------------------------------

Data Structure Types
--------------------
### Array
- **static data structure**

### Linked list
- **dynamic data structure**

### Tree
- Each data item is called a node, the first data item is called the root.
- The position of the data is determined by its relationship with existing data already in the tree.
- They allow us to store data in a particular order
#### Insertion
1. Start at the root
2. Compare the new data to current node
3. If new data < current node, follow left pointer (branch)
3. Else follow the right pointer
4. UNTIL there is no node at the end of the pointer
5. Write the new data into the tree
6. Create two new (empty) branches for his new node

#### Deletion
- *Each node is not only a data item but a part of the structure as well.*
- *If we simply delete data it would be like sawing off a branch and having everything else on that branch fall off too.*
- Instead of deleting it properly, we leave it in there but label it as 'deleted'. This means that the computer will not read it when the tree is being searched through.

### Stack
- Has Stack Pointer
- LIFO(Last In First Out) / FILO(First In Last Out)
#### Stack Operations
- Push
- Pop
- Peek

#### Insertion
1. Check to see if stack is full
2. If the stack is full report an error and stop
3. Increment Stack Pointer
4. Insert new data item into cell pointed to by the Stack Pointer

#### Deletion
1. Check to see if stack is empty
2. If the stack is empty report an error and stop
3. Copy data item in the cell pointed to by the Stack Pointer
4. Decrement the Stack Pointer and stop

### Queue
- Has two pointers - one to the front of the queue and one to the back
- FIFO(First In First Out)

#### Queue Operations
- Enqueue
- Dequeue
- Front
- Rear

#### Insertion
1. Check to see if queue is full
2. If the queue is full then report an error and stop
3. Increment cell pointed to by the tail pointer
4. Add new data to the cell pointed to by the tail pointer

#### Deletion
1. Check to see if queue is empty
2. If queue is empty report error and stop
3. Copy the data item pointed to by front pointer
4. Increment from pointer

#### Circular Queue Operations
- A queue in an array is a circular queue
	- All available storage can be resused

### Heap
A Heap is a special Tree-based data structure in which the tree is a complete binary tree. Generally, Heaps can be of two types:

#### Max-Heap
 In a Max-Heap the key present at the root node must be greatest among the keys present at all of it’s children. The same property must be recursively true for all sub-trees in that Binary Tree.
#### Min-Heap
 In a Min-Heap the key present at the root node must be minimum among the keys present at all of it’s children. The same property must be recursively true for all sub-trees in that Binary Tree.

### Linked List
#### Types
- Singly Linked List : `1->2->3->4->NULL`
- Doubly Linked List : `NULL<-1<->2<->3->NULL`
	- Can be implemented using a single pointer variable in every node.(XOR Linked List)
- Circular Linked List : `1->2->3->1`
#### Performance
Faster than array in deletion.
Slower than array in random access.
#### Syntax
``` csharp
public class Node {
	public Node Next {get; private set;} = null;
	object Data {get; set;}

	public Node(object data){
		this.Data = data;
	}

	public Node AppendToTail(object data){
		var endNode = new Node(data);
		var n = this;
		while (n.Next != null){
			n = n.Next;
		}
		n = endNode;
		return endNode;
	}
}
```
##### Delete
###### Singly Linked List
``` csharp
public static Node DeleteNode(Node headNode, object data){
	Node n = headNode;
	// Is head the node to delete?
	if(n.data == data){
		return head.next; // head moved to next node
	}
	while (n.next != null){
		if(n.next.data == data){
			n.next = n.next.next;
			return head;
		}
		n = n.next;
	}
}
```

--------------------------------------------------------------------------------

Search
------
### Breadth-first search (BFS)
It starts at the tree root (or some arbitrary node of a graph, sometimes referred to as a 'search key'[1]), and explores all of the neighbor nodes at the present depth prior to moving on to the nodes at the next depth level.
#### Datastructure
Queue
### Depth-first search (DFS)

#### Datastructure
Stack. DFS can also be implemented using recursion.

## Notations
### Infix notation
`A * ( B + C ) / D`

### Postfix notation
`A B C + * D/`

### Prefix notation
`/ * A + B C D`

## Least Recently Used (LRU) cache
### Datastructures Used
#### Queue
Implemented using a doubly linked list. The maximum size of the queue will be equal to the total number of frames available (cache size).The most recently used pages will be near rear end and least recently pages will be near front end.

#### Hash
With page number as key and address of the corresponding **queue** node as value

## Binary Tree
### Binary Search Tree
If inorder traversal of a binary tree is sorted, then it is a BST(**Binary Search Tree**).
The idea is to simply do inorder traversal and while traversing keep track of previous key value. If current key value is greater, then continue, else return false.

---
# Stack vs Heap
https://gribblelab.org/CBootCamp/7_Memory_Stack_vs_Heap.html
## The Stack
It's a special region of your computer's memory that stores temporary variables created by each function (including the main() function). The stack is a "LIFO" (last in, first out) data structure, that is managed and optimized by the CPU quite closely. Every time a function declares a new variable, it is "pushed" onto the stack. Then every time a function exits, all of the variables pushed onto the stack by that function, are freed (that is to say, they are deleted). Once a stack variable is freed, that region of memory becomes available for other stack variables.
### Pros and Cons
- Very fast access
- Don't have to explicitly de-allocate variables
- Space is managed efficiently by CPU, memory will not become fragmented
- Local variables only
- Limit on stack size (OS-dependent)
- Variables cannot be resized

## The Heap
The heap is a region of your computer's memory that is not managed automatically for you, and is not as tightly managed by the CPU. It is a more free-floating region of memory (and is larger). To allocate memory on the heap, you must use malloc() or calloc(), which are built-in C functions. Once you have allocated memory on the heap, you are responsible for using free() to deallocate that memory once you don't need it any more. If you fail to do this, your program will have what is known as a memory leak. That is, memory on the heap will still be set aside (and won't be available to other processes). As we will see in the debugging section, there is a tool called valgrind that can help you detect memory leaks.
- Variables can be accessed globally
- No limit on memory size
- (relatively) slower access
- No guaranteed efficient use of space, memory may become fragmented over time as blocks of memory are allocated, then freed
- You must manage memory (you're in charge of allocating and freeing variables)
- Variables can be resized using realloc()

Unlike the stack, the heap does not have size restrictions on variable size (apart from the obvious physical limitations of your computer). Heap memory is slightly slower to be read from and written to, because one has to use pointers to access memory on the heap. We will talk about pointers shortly.

Unlike the stack, variables created on the heap are accessible by any function, anywhere in your program. Heap variables are essentially global in scope.

---

# Algorithms
Operations on different data structures + sets of instructions for executing them.

## Searching Algorithms
### Serial Search
1. Check array is not empty
2. Each data element is compared to the search item
3. When the item is found, the index is returned
4. If end of the array reached without finding the item, an error message is produced

### Binary Search
1. Continually compare the middle data item in the list with the search item
2. Repeatedly split the list into two equal halves
3. Until the middle value = the search item or error if the new list is empty

## Merge Algorithm
> Assumong there are no duplicate values in the list and both actually contain data
1. Select value at the front of each list
2. While neither list is empty
3. Compare the values in the lists
4. Copy the smaller value to the new list
5. Read the next value in the list that has had a value copied
6. When one list is empty copy the remainder of the other list to the new list

## Sorting Algorithms
### Insertion Sort
#### Suitable when:
1. List is small
2. List is almost sorted in the right order
3. List contains many data items of equal value as it is stable (equal values aren't reordered)

#### Advantages
- Easy to implement
- Efficient when lots of duplicate values in list
- Can sort a list of data as it receives each data item
- Only requires a constant (set) amount of memory space

#### Disadvantages
1. Not good on large data sets
2. Not efficient when data is in reverse order

### Quick Sort
#### Suitable when:
1. is large
2. is in reverse order
3. The data is completely out of sequence

#### Advantages
- Efficient on large data sets
- Efficient on data in reverse order
- Efficient when data is completely out of sequence

#### Disadvantages
- Hard to implement
- Not good on data that is partially sorted
- Not good on data with lots of duplicate values

---
# Arrays and Memory
`int sampleArray[5] = {2, 4, 6, 8, 100};`
sampleArray = | 2 | 4 | 6 | 8 | 100 |

`sampleArray[0] = 20;
sampleArray[1] = -5;`
sampleArray = | **20** | **-5** | 6 | 8 | 100 |

>Description of Array and Memory is as you always studied
- Arrays are stored in sequence in memory
- The reason of why array has a fixed size is because the memory locations after the array may be in use.
- In order to resize you need to create a new array and copy over the values from the old one.
	- 10 -> 20 -> 40 -> 80
- **Python Array** and **Java ArrayList** actually recreate the arrays when elements are added.
---
# Classes & Objects
> What you already know
- **Object:** Collection of defined properties(variables) and Functions together.
- **Class:** A blueprint of which you can make objects.

# Linked List
- A data structure from storing a collection of items.
- Can be visualized as many boxes connected to each other.
**Basic Example:**
```
class Box{
	int data;
	Box next; // Reference(link) to next box
}
```
# Recursion
Please refer to **Recursion**

# Big O Notation
Time it takes to run your function as the size of the input grows
How much time does it take to run the function?
- How does the runtime of this function grow?
	- Big O Notation and Time Complexity

## Expressing Time using Big O
> **n** is usually the size of the input
- **Linear Time:** O(n) /
- **Constant time:** O(1)
- **Quadratic time:** O(n<sup>2</sup>)

#### How to find out if an expression is Big O:
1. Find the fastest growing term
2. Take out the coefficient 

##### Examples:

T = an + b = O(n) 
> **a** and **b** are two constants 
> **an** is the fastest growing term because **n** is a variable
> remove coefficient **a** from **an** which leaves us with just **n** -> O(**n**)

T = cn<sup>2</sup> + dn + e = O(n<sup>2</sup>)
> **c**, **d** and **e** are constants
> **cn<sup>2</sup>** is the fastest growing term -> O(**n<sup>2</sup>)**

##### Code Examples:
### Expample1
###### With real life experiments:
```
def useless_function(given_array):
	total = 0 
	return total 
```
_After running experiments the function always ran in the time of 0.115ms_

T = c = 0.115
	= 0.115 * 1 = O(1) _ where 0.115 is the coefficient _
** constant time **

###### Without real life experiments:
```
def useless_function(given_array):
	total = 0 // -> O(1)
	return total // -> O(1)
```
T = O(1) + O(1) = c<sub>1</sub> + c<sub>2</sub> // Where **c** means constant

=  c<sub>3</sub> = c<sub>3</sub> * 1 = O(1) // take out the coefficient c<sub>3</sub>

Therefore O(1) + O(1) = O(1)

### Example 2
```
def fine_sum(given_array):
	total = 0 // -> O(1)
	for each i in given_array:
		total += i // -> O(1) * n
	return total // -> O(1)
```
T = O(1) + n * O(1) + O(1)

= c<sub>1</sub> + n * c<sub>2</sub> = O(n) // Remove the coefficient of the fastest growing term

### Example 3
```
array_2d = [[1, 4, 3]
			[3, 1, 9]
			[0, 5, 2]]
```
```
def find_sum_2d(array_2d):
	total = 0 // -> O(1)
	for each row in array_2d:
		for each i in row:
			total += i // -> O(1) * n squared
	return total // -> O(1)
```
> For matter of simpilicity ignoring the overhead cause by setting up the arrays which in the end will have not effect the expression

T = O(1) + n<sup>2</sup> * O(1) + O(1)

= c<sub>1</sub> + n<sup>2</sup> * c<sub>2</sub> = O(n<sup>2</sup>)

###### What if the loops are run twice?:
```
def find_sum_2d(array_2d):
	total = 0 // -> O(1)
	for each row in array_2d:
		for each i in row:
			total += i // -> O(1) * n squared
	for each row in array_2d:
		for each i in row:
			total += i // -> O(1) * n squared

	return total // -> O(1)
```
T = O(1) + 2n<sup>2</sup> * O(1) + O(1)

= c<sub>1</sub> + 2n<sup>2</sup> * c<sub>2</sub> =  c<sub>1</sub> + (2 * n<sup>2</sup>) * c<sub>2</sub> = O(n<sup>2</sup>)

> So technically the below is true

T = O(2n<sup>2</sup>)  = O(n<sup>2</sup>)


<!--stackedit_data:
eyJoaXN0b3J5IjpbLTIxMTcxODAyMDEsLTM1MzU5NzE1NiwzNj
k2MjU5MzYsMTI0OTY2OTk0MCwxODUzOTAyMTcyLDE5NzI3ODgz
MDQsNTE3MTAwNTc0LC0xODUzNzA3NjQsLTEwNzE0Njg4NTAsLT
IwODYwNzY0MTYsMTEzMDkxMTQxOSw1MTUyMTgxOTMsLTg2NjMy
NjA2NCwyMDgxOTc3MTUxLDExMTIyMjUyOTQsMTcxNzcxNjkzNC
wtODkwNTM5NjYsLTExNzg3NjkxMDUsMTY4NTkxMTk0NCwtODcy
MTk4MTA0XX0=
-->

--------------------------------------------------------------------------------

Links
-----
- [Github Repo](https://github.com/rachitiitr/DataStructures-Algorithms)
- [DSA C#](https://github.com/abdonkov/DSA)
- [DSA C# Contrib](https://github.com/aalhour/C-Sharp-Algorithms)
- [Leet Code](https://github.com/bysz71/LeetCodeInCSharp/tree/master/LeetCodeInCSharp)
