# [Google Apps Script](https://script.google.com) - Notes

## Settings
https://console.cloud.google.com/apis/credentials/consent?authuser=1

## Testing
[[-]] [Testing from GAS HTML](https://stackoverflow.com/questions/65583967/pass-value-from-html-to-google-script-on-google-apps-script)
### [Test from debugger](://stackoverflow.com/questions/37863470/how-to-test-value-of-objects-on-a-doget-function-without-using-application)
``` javascript
function doGet(e) {
	return ContentService.createTextOutput(JSON.stringify(e));
}

function fakeGet() {
	var eventObject = 
	{
		"parameter": {
			"action": "view",
			"page": "3"
		},
		"contextPath": "",
		"contentLength": -1,
		"queryString": "action=view&page=3",
		"parameters": {
			"action": ["view"],
			"page": ["3"]
		}
	}
	doGet(eventObject);
}
```
- [Effective way to debug a Google Apps Script Web App](https://stackoverflow.com/questions/11493643/effective-way-to-debug-a-google-apps-script-web-app)
- 
## How do you view script properties in the new IDE
``` javascript
function logScriptProperties {
    let properties = PropertiesService
	.getScriptProperties()
	.getProperties();
	console.log(JSON.stringify(properties, undefined, 4));
};
```


## Google Cloud Platform
[Setting up OAuth 2.0](https://support.google.com/cloud/answer/6158849?hl=en)

## IFTTT
### [Post from IFTTT](https://www.pnkts.net/2019/09/23/ifttt-to-gas)
- [IFTTT Github Project Example](https://github.com/IFTTT/ifttt-api-example)
#### Deployment setup

- *Execute As*: Execute As
- *Who has access*: Anyone

## [Publish](https://developers.google.com/workspace/add-ons/how-tos/publish-add-on-overview)
- [How to publish](https://developers.google.com/workspace/marketplace/how-to-publish)

## Links
- [Awesome List](https://github.com/contributorpw/google-apps-script-awesome-list)
- [GoogleAppsScript Subreddit](https://www.reddit.com/r/GoogleAppsScript/)
