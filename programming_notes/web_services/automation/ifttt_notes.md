# IFTTT - Notes

## [Connect API](https://platform.ifttt.com/docs/connect_api)
[[###]] [Android](https://github.com/IFTTT/ConnectSDK-Android)

![IFTTT Webhook image](img/ifttt-webhook.PNG "IFTTT Webhook")⏎
![IFTTT Webhook image](img/ifttt-webhook-contenttype.PNG "IFTTT Webhook")⏎

## GAS (Google Apps Script)
### Post JSON to GAS
- [[./gas_notes]] - GAS Deployment setup

## Post to Webservice with authentication
- https://stackoverflow.com/questions/48040879/ifttt-webhooks-maker-basic-authentication
- [IFTTT Github Project Example](https://github.com/IFTTT/ifttt-api-example)
- [IFTTT Github Project Example](https://github.com/IFTTT/ifttt-api-example)
- [Embedded Applets](https://platform.ifttt.com/docs/embedding_applets#authentication)
