Graphics Notes
==============

## [Vertex and index buffers](https://learn.microsoft.com/en-us/windows/uwp/graphics-concepts/vertex-and-index-buffers)
### Vertex buffers
- Memory Buffers that contain vertex data
- Vertices are processed to perform transformation, lighting, and clipping.
- Can contain any **vertex type** that can be rendered:
    - transformed
    - untransformed
    - lit
    - unlit
- You can process vertices to perform operations such as:
    - transformation
    - lighting
    - generating clipping flags
    - etc
- Ideal staging points for reusing transformed geometry.
- The geometry is transformed only once, and portions of it can be rendered as needed, interleaved with the required texture changes.
### Index buffers
- Memory Buffers tat contain index data, which are integer offsets into vertex buffers, used to reder primitives.


## Frame Buffer
- Where a frame is constucted behind the scenes before it replaces the current frame.
- Prevents flickering
