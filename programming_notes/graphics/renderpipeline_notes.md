Render Pipeline
===============
GPUs can do parallel processing which would allow them to process multiple vertices
at once
[3D Graphics](https://www.youtube.com/watch?v=_riranMmtvI&t=908s)
-----------
> Linear sequence of stages
### Stages
#### Stage Types
- **Fixed Function:** Programmers have less control over what operations these stages
	perform. They can only configurations.
- **Programmable:** Is code that can be uploaded to be executed by gpu (Shaders).
> The following is not showing Tesselation or Geometry shader stages
#### Stage Sequence
1. **Vertex/Index Buffer:**
	Ex: (x1,y1)
		(x2,y2)
		(x3,y3)
		etc
2. [Fixed Function]**Input Assembles:**					
	Takes input a list of numbers and puts them together as geometry
3. [Programmable]**Vertex Shaders:**
	Processes each vertex individually and *performs transformations* like *rotations* and *translations*.
4. [Fixed Function]**Rasterization:**
	Breaks up transformations into fragments for each pixel the shape overlaps.
5. [Programmable]**Fragment ShadersColor Blending:**
	Processes each fragment individually and outputs values such as color by using
	interpolated data such as textures normals and lighting.
6. [Fixed Function]**Color Blending:**
	Mixes the values from multiple fragments that correspond to the same pixel in
	the final image.
7. **Frame Buffer (image):**


