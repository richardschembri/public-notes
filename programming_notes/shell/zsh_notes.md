zsh Notes 
=========

Syntax
------

### [Associative arrays / Dictionaries](https://scriptingosx.com/2019/11/associative-arrays-in-zsh/)
#### Decleration
```sh
declare -A userinfo
```
#### Assignment
```sh
userinfo[name]="armin"
userinfo[shell]=bash
userinfo[website]="scriptingosx.com"
```
**One liner:**
```sh
userinfo=( name armin shell zsh website scriptingosx.com )
```

**Alternative:**
```sh
userinfo=( [name]=armin [shell]=zsh [website]="scriptingosx.com" )
```
#### Has Key
```sh
userinfo=( [name]=beth [shell]=zsh )
if [[ -z $userinfo[website] ]]; then echo no value; fi
```
#### Modify array
```sh
userinfo+=( [shell]=fish [website]=fishshell.com )
```
#### Clear
```sh
userinfo=( )
```
#### Get Keys
```sh
echo ${(k)userinfo}
```
#### Get Keys and Values
```sh
echo ${(kv)userinfo}
```
#### Clone array
```sh
declare -A otherinfo
otherinfo=( ${(kv)userinfo )
echo $otherinfo[name]
```
#### Iterate array
```sh
for key value in ${(kv)userinfo}; do
    echo "$key -> $value"
done
```
