Bash 
====


## Table of Contents

- [Shebang](#shebang)
- [Get Script Path](#get-script-path)
- [IFS(Internal Field Separator)](#ifsinternal-field-separator)
  - [Example 1](#example-1)
  - [Example 2](#example-2)
  - [Example 3](#example-3)
- [read](#read)
  - [Store each of the next lines you enter as values of an array:](#store-each-of-the-next-lines-you-enter-as-values-of-an-array)
  - [Specify the number of maximum characters to be read:](#specify-the-number-of-maximum-characters-to-be-read)
  - [Use a specific character as a delimiter instead of a new line:](#use-a-specific-character-as-a-delimiter-instead-of-a-new-line)
  - [Do not let backslash (\) act as an escape character:](#do-not-let-backslash-act-as-an-escape-character)
  - [Display a prompt before the input:](#display-a-prompt-before-the-input)
  - [Do not echo typed characters (silent mode):](#do-not-echo-typed-characters-silent-mode)
  - [Read stdin and perform an action on every line:](#read-stdin-and-perform-an-action-on-every-line)
- [Arguements](#arguements)
  - [Satic number of Arguements](#satic-number-of-arguements)
  - [Variable number of Arguements](#variable-number-of-arguements)
- [[Flags](https://stackoverflow.com/questions/18414054/reading-optarg-for-optional-flags)](#flags)
- [Default Values](#default-values)
  - [`-` Use a default value](#-use-a-default-value)
  - [`=` Assign a default value](#-assign-a-default-value)
  - [`?` Display an error if unset or null](#-display-an-error-if-unset-or-null)
  - [`+` Use an alternate value](#-use-an-alternate-value)
- [`if` statement](#if-statement)
- [if `true` (boolean condition)](#if-true-boolean-condition)
- [[compare strings](https://stackoverflow.com/questions/2237080/how-to-compare-strings-in-bash)](#compare-strings)
- [`if` Statement flags](#if-statement-flags)
- [[File/Directory IF statement](https://linuxize.com/post/bash-check-if-file-exists/)](#filedirectory-if-statement)
  - [File](#file)
  - [Directory](#directory)
- [[Yes/No Prompt](https://www.shellhacks.com/yes-no-bash-script-prompt-confirmation/)](#yesno-prompt)
- [Generate folder path](#generate-folder-path)
- [[if operators](https://stackoverflow.com/questions/20449543/shell-equality-operators-eq)](#if-operators)
- [Generate file](#generate-file)
- [[for loop](https://linuxize.com/post/bash-for-loop/)](#for-loop)
  - [Number Range](#number-range)
- [[Array](https://linuxhandbook.com/bash-arrays/)](#array)
  - [Declare](#declare)
    - [[Declare empty array](https://linuxhint.com/declare-empty-array-bash/)](#declare-empty-array)
  - [Append Element](#append-element)
  - [Delete Array](#delete-array)
  - [Print out all the array elements at once:](#print-out-all-the-array-elements-at-once)
  - [Array contains value](#array-contains-value)
- [Angle Brackets `<>`](#angle-brackets-)
  - [`>`](#)
  - [`>>`](#)
  - [`<`](#)

Command Line
------------
``` sh
chmod +x filename.sh
```

--------------------------------------------------------------------------------

Syntax
------
### Shebang
``` sh
#!/usr/bin/env sh
```

### Get Script Path
``` sh
SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
```

### [IFS(Internal Field Separator)](https://www.mybluelinux.com/bash-guide-to-bash-ifs-variable/)
Sets field seperator inside bash

#### Example 1
```sh
cars=(Toyota Honda Renault "Maruti Suzuki")
for car in ${cars[*]}
do
  echo $car
done
```
**Output**
```sh
Toyota
Honda
Renault
Maruti
Suzuki
```
**If prepended with the following:**
```sh
IFS=$'\n' # Set delimiter to break line
```
**Output**
```sh
Toyota
Honda
Renault
Maruti Suzuki
```

#### Example 2
> Set newline and colon as a field separator
```sh
IFS=$'\n':
```

#### Example 3
**domaints.txt**
```sh
mybluelinux.biz|2.54.1.1|/home/httpd|ftpuser1
mybluelinux.com|2.54.1.2|/home/httpd|ftpuser2
mybluelinux.org|2.54.1.3|/home/httpd|ftpuser3
```
```sh
#!/bin/bash

file=/tmp/domains.txt

# set the Internal Field Separator to |
IFS='|'

while read -r domain ip webroot ftpusername
do
        printf "*** Adding %s to httpd.conf...\n" $domain
        printf "Setting virtual host using %s ip...\n" $ip
        printf "DocumentRoot is set to %s\n" $webroot
        printf "Adding ftp access for %s using %s ftp account...\n\n" $domain $ftpusername
	
done < "$file"
```

### read
BASH builtin for retrieving data from standard input.
 
```sh
read variable
```

#### Store each of the next lines you enter as values of an array:
```sh
read -a array
```

#### Specify the number of maximum characters to be read:
```sh
read -n character_count variable
```

#### Use a specific character as a delimiter instead of a new line:
```sh
read -d new_delimiter variable
```

#### Do not let backslash (\) act as an escape character:
```sh
read -r variable
```

#### Display a prompt before the input:
```sh
read -p "Enter your input here: " variable
```

#### Do not echo typed characters (silent mode):
```sh
read -s variable
```

#### Read stdin and perform an action on every line:
```sh
while read line; do echo "$line"; done
```

### Arguements
#### Satic number of Arguements
``` sh
FILE1=$1
wc $FILE1
```

``` sh
sh stats.sh songlist.txt  
```

#### Variable number of Arguements
``` sh
for FILE1 in "$@"
do
wc $FILE1
done
```

``` sh
sh stats.sh songlist1 songlist2 songlist3
```

### [Flags](https://stackoverflow.com/questions/18414054/reading-optarg-for-optional-flags)
``` sh
makereport -u jsmith -p notebooks -d 10-20-2011 -f pdf
```

``` sh
while getopts u:d:p:f: option
do
case "${option}"
in
u) USER=${OPTARG};;
d) DATE=${OPTARG};;
p) PRODUCT=${OPTARG};;
f) FORMAT=${OPTARG};;
esac
done
```
- Arguments followed by a colon are required 
- Any arguments not handled by `getops` can still be captured with the regular `$1`, `$2`, and `$3` variables.

> https://www.golinuxcloud.com/bash-getopts/
``` sh
# cat single_arg.sh
#!/bin/bash

function usage {
        echo "./$(basename $0) -h --> shows usage"
}

# list of arguments expected in the input
optstring=":h"

while getopts ${optstring} arg; do
  case ${arg} in
    h)
      echo "showing usage!"
      usage
      ;;
    :)
      echo "$0: Must supply an argument to -$OPTARG." >&2
      exit 1
      ;;
    ?)
      echo "Invalid option: -${OPTARG}."
      exit 2
      ;;
    *)
      echo "Default option"
      exit 2
      ;;
  esac
done
```

### Default Values
Default value handling is done by parameter of the form: ${parameter:[-=?+]word} such as:

#### `-` Use a default value
``` sh
${parameter:-word}
```
#### `=` Assign a default value
``` sh
${parameter:=word}
```
#### `?` Display an error if unset or null
``` sh
${parameter:?word}
```
#### `+` Use an alternate value
``` sh
${parameter:+word} to 
```
### `if` statement
``` sh
if [ <some test> ]
then
<commands>
elif [ <some test> ]
then
<different commands>
else
<other commands>
fi
```

### if `true` (boolean condition)

``` sh
isdirectory() {
  if [ -d "$1" ]
  then
    true
  else
    false
  fi
}

if isdirectory $1; then echo "is directory"; else echo "nopes"; fi
```

### [compare strings](https://stackoverflow.com/questions/2237080/how-to-compare-strings-in-bash)
``` sh
if [ "$x" = "valid" ]; then
  echo "x has the value 'valid'"
fi
```

### `if` Statement flags
– `if -z`: to check if **string** has **zero** length
– `if -s`: to check if **file size** is **greater** than **zero**
– `if -n`: to check if **string length** is **not zero**
– `if -f`: to check if **file exists** and is a regular file

### [File/Directory IF statement](https://linuxize.com/post/bash-check-if-file-exists/)
#### File
``` sh
FILE=/etc/resolv.conf
if test -f "$FILE"; then
    echo "$FILE exists."
fi

FILE=/etc/resolv.conf
if [ -f "$FILE" ]; then
    echo "$FILE exists."
fi

FILE=/etc/resolv.conf
if [[ -f "$FILE" ]]; then
    echo "$FILE exists."
fi

test -f /etc/resolv.conf && echo "$FILE exists."
[ -f /etc/resolv.conf ] && echo "$FILE exists."
[[ -f /etc/resolv.conf ]] && echo "$FILE exists."
```
#### Directory
``` sh
FILE=/etc/docker
if [ -d "$FILE" ]; then
    echo "$FILE is a directory."
fi

[ -d /etc/docker ] && echo "$FILE is a directory."
```

### [Yes/No Prompt](https://www.shellhacks.com/yes-no-bash-script-prompt-confirmation/)
``` sh
read -p "Are you sure? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    exit 1
fi
```

https://stackoverflow.com/questions/15668170/if-statement-and-calling-function-in-if-using-bash

### Generate folder path
``` sh
FILE=/etc/docker
if [ -d "$FILE" ]; then
    echo "$FILE is a directory."
fi

[ -d /etc/docker ] && echo "$FILE is a directory."
```

### [if operators](https://stackoverflow.com/questions/20449543/shell-equality-operators-eq)


`=` and `==` are for string comparisons
`-eq` is for numeric comparisons
`-eq` is in the same family as `-lt`, `-le`, `-gt`, `-ge`, and `-ne`

```sh
$ a=foo
$ [ "$a" = foo ]; echo "$?"       # POSIX sh
0
$ [ "$a" == foo ]; echo "$?"      # bash-specific
0
$ [ "$a" -eq foo ]; echo "$?"     # wrong
-bash: [: foo: integer expression expected
2
```

### Generate file
``` sh
touch /my/other/path/here/cpedthing.txt
```

### [for loop](https://linuxize.com/post/bash-for-loop/)
#### Number Range
```sh
for i in {0..3}
do
  echo "Number: $i"
done
```
> Increment by 5
```sh
for i in {0..20..5}
do
  echo "Number: $i"
done
```


### [Array](https://linuxhandbook.com/bash-arrays/)
#### Declare
```sh
distros=("Ubuntu" "Red Hat" "Fedora")
```

##### [Declare empty array](https://linuxhint.com/declare-empty-array-bash/)
```sh
declare -a distros
```

#### Append Element
```sh
distros+=("Kali")
```

#### Delete Array
```sh
unset distros
```

#### Print out all the array elements at once:
```sh
echo ${files[*]}
```
> output
```sh
f1.txt f2.txt f3.txt f4.txt f5.txt
```

#### Array contains value
```sh
if [[ " ${array[*]} " =~ " ${value} " ]]; then
    # whatever you want to do when array contains value
fi

if [[ ! " ${array[*]} " =~ " ${value} " ]]; then
    # whatever you want to do when array doesn't contain value
fi
```

### Angle Brackets `<>`

#### `>`
Redirect output from a command to a file (**overwrite**). 
#### `>>`
Redirect output from a command to a file (**append**). 
#### `<`
Redirect input from a file to a command
```bash
diff <(command1) <(command2)
```

-------------------------------------------------------------------------------

Links
-----
- [Args](https://unix.stackexchange.com/questions/440206/how-to-properly-parse-shell-script-flags-and-arguments-using-getopts)
- [Args2](https://www.golinuxcloud.com/bash-getopts/)
- [Helps](https://stackoverflow.com/questions/5474732/how-can-i-add-a-help-method-to-a-shell-script)`` 
