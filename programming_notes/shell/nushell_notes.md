Nushell Notes
=============
Syntax
------
### Shebangs
```nu
#!/usr/bin/env nu
```



### to json
```nu 
/home/richard〉let foo = 7
/home/richard〉{foo: $haha bar: $foo} | to json
{
  "foo": 9,
  "bar": 7
}
````
