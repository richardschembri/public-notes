Powershell Notes 
================

### [grep equivalent](https://adamtheautomator.com/powershell-grep)
``` sh
Select-String -Path "Users\*.csv" -Pattern "Joe"
```
#### Select without outputting source filename
``` sh
Select-String -Path "Users\*.csv" -Pattern "Joe" | Select -ExpandProperty Line
```
#### Output to a file
``` sh
Select-String -Path "Users\*.csv" -Pattern "Joe" | Select -ExpandProperty Line | Out-File -FilePath output.csv
```

