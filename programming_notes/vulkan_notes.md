[Vulkan](https://www.youtube.com/watch?v=lr93-_cC8v4)
=================================================

Setup
-----
### [OpenSUSE](https://vulkan-tutorial.com/Development_environment#page_Linux)
```sh
sudo zypper in vulkan-tools vulkan-devel Mesa-libVulkan-devel libglfw-devel glm-devel
```
| Package         | Description                             |
| :------         | :----------                             |
| `libglfw-devel` | Cross platform graphical window library |
|                 |                                         |

> Run `vkcube` to check that the installation worked.

Download Google's unofficial binaries and copy `glslc` to your `/usr/local/bin`
Try running the command, it should show:
```sh
glslc: error: no input files
```

----------------
