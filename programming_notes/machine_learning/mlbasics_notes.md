Machine Learning Basics
=======================

Q-Learning Intuition
--------------------

### [Reinforcement Learning](https://medium.com/emergent-future/simple-reinforcement-learning-with-tensorflow-part-0-q-learning-with-tables-and-neural-networks-d195264329d0)

**Agent** -> Action -> Environment -> State,Reward -> **Agent**

### The Bellman Equation
#### Concepts
| Symbol | Meaning    | Description                                                            |
| :----- | :--------- | :--------------------------------------------------------------------- |
| s      | **S**tate  |                                                                        |
| a      | **A**ction |                                                                        |
| R      | **R**eward |                                                                        |
| γ      | Discount   | Discount the value of the state the further away you are from the goal |

> There is also a *negatice* reward when the wrong action is done

`V(s)=max(R(s,a) + γV(s'))`
Where   V	= value(valuable)
		s'= s prime = value of *next* state
		max = maximum **a**ction

Example of a maze and with each action valuability calculated

| V=0.81 | V=0.9  | V=1    | Goal   |
| V=0.73 | Wall   | V=0.9  | Death  |
| V=0.66 | V=0.73 | V=0.81 | V=0.73 |

### **M**arkov **D**ecision **P**rocess

#### Deterministic Search
100% know the result of an action. Robot wants to go up so it will go up.

#### Non-Deterministic Search
**Statistically know the result of an action. Robot wants to go forward but there is a small chance it might end up going left(Ex bouncing off a wall).

**MDP**s use maths to model descisions in situations where outcomes are partly random or partly under the control of the decision maker.

Function with added formula for probability
`V(s)=max(R(s,a) + γEs'P(s,a,s')V(s'))`

Example of a maze and with each action valuability calculated with probability also taken in consideration

| V=0.71 | V=0.74 | V=0.86 | Goal   |
| V=0.63 | Wall   | V=0.39 | Death  |
| V=0.55 | V=0.46 | V=0.36 | V=0.22 |

### Policy vs Plan


### Living Penalty
Examples of Life penalty for every action
R(s)=0
| → | →    | → | Goal  |
| ↑ | Wall | ← | Death |
| ↑ | →    | ↑ | ↓     |
Robot prefers to bounce off walls since it is the safest option(because it can bounce left or right)


R(s)=-0.04
| → | →    | → | Goal  |
| ↑ | Wall | ↑ | Death |
| ↑ | ←    | ← | ←     |
Ended up taking a longer route sometimes but still takes less turns than trying to bounce off walls


R(s)=-0.5
| → | →    | → | Goal  |
| ↑ | Wall | ↑ | Death |
| ↑ | →    | ↑ | ←     |
Actions take a considerable amount of life so short routes are prioritised

R(s)=-2.0
| → | →    | → | Goal  |
| ↑ | Wall | → | Death |
| ↑ | →    | → | ←     |
Actions take a lot of life so any extra action can lead to big death(-2.0), so robot prefers lesser death(-1.0)

### Q-Learning Intuition
**Q**: Quality or Quantifiable

`Q(s,a)=R(s,a) + γEs'(P(s,a,s')V(s'))`

You can use the equation for V(s) for V(s') which when simplified becomes:

`Q(s,a)=R(s,a) + γEs'(P(s,a,s')maxQ(s',a'))`

where max = maximum **a'**

For the sake of simplicity the equation is written like this:

`Q(s,a)=R(s,a) + γmaxQ(s',a')`

#### [Temporal Difference](https://link.springer.com/article/10.1007/BF00115009)

Action taken:
| Before   | After                   |
| `Q(s,a)` | `R(s,a) + γmaxQ(s',a')` |

So:
`Q(s,a)=R(s,a) + γmaxQ(s',a')`


`TD(a,s)=R(s,a) + γmaxQ(s',a') - Qt-1(s,a)`
The result should be zero but if it is different then something affected the result over time. (For example an enemy got close to that area)


`Qt(s,a)=Qt-1(s,a) + αTDt(a,s)`
where **α** is learning rate and **t** is time.
You use **α** to regulate how much to learn about the differences.

[Deep Q-Learning Intuition](https://medium.com/@awjulian/simple-reinforcement-learning-with-tensorflow-part-4-deep-q-networks-and-beyond-8438a3e2b8df)
-------------------------

`Input Layer -> Hidden Layer (Neural Network) -> Output Layer`

Neural Networks predict the next step's outcome.

Neural Networks use **weights** rather than **time**.

`L = E(Q-Target - Q)²`
Where Q-Target is taken from the Neural Network's output(s)
and **L** is Learning


`Input Layer -> Hidden Layer (Neural Network) -> Output Layer -> Softmax -> Best output(Best Q)`

### [Experience Replay](https://arxiv.org/pdf/)
Learning -> Acting

- Threshold of experiences to learn from
- Randomly select from sample/batch of experiences
- Breaks bias from processing experiences **sequencially**

