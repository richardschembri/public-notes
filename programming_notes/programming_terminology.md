> These are notes for concepts I know but terms I might forget.

`Type Inference`: The _variable_ infers the _type_ from the _value_.

## Threading/Concurrency vs Parellelism
### Multithreading
* Works on single and multi core machines as well.
* Assign threads for time consuming tasks like calling a web server, allowing
the program to do other tasks in the mean time.

### Parellelism
* Partition your compute bound work into smaller chunks that can truly run
in parallel.
* Ideally [no of chunks] > [no of cores] so the program can scale.

### Provisioning Script
* Install correct version of NodeJS, php, etc
* Set user permissions
* Install other software
* Does configure

## Copy
## Shallow copy
Some members of the copy may reference the same objects as the original.
## Deep copy
All members of the original are cloned (recursively, if necessary). There are no shared objects.

## Lifetime
Example: `static`

## Data Race
When a pointer is trying to access data whilst it is being modified by another pointer

## Dangling references
A reference to nothing, like a function returns a reference to it's local variable
which will go out of scope.
