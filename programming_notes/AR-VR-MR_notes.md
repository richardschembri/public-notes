## https://www.justinmind.com/blog/vr-design/

Design Principles
=================

Common
------
- Space
- Motion
- Scope

VR
----
### Lower Resolutions
### Print Design
- Posters are designed to be legible, follow the priciple
### UI Controls and Buttons
Make sure controls are placed in the user's immediate field of view.
### Comfort
- Avoid a lot of movement
- Wide enough space
### Audio
- 3D audio

### [Passthrough](https://www.technipages.com/vr-oculus-quest-2-what-is-passthrough)
Allows users to see their surroundings through the several cameras built into the
outside of the headset.

AR
----
### User enviroment
- Which camera will the user use? (Selfie/ Back)
### Light and Sound
- Match the light and sound of surroundings
### Motion and Safety
- Make user aware of their surroundings
### Development Tools
- Surface Plane Detection
- Light Estimation

MR
----
### Personal Space
- That put objects too close or obstruct vision
### Clear UI
- Don't overwhelm the user
- Clear and concise
### Development Tools
- Optimize Geometry and Shaders

--------------------------------------------------------------------------------

Terminology
-----------

### Foveated Rendering
Reduction in rendering quality in wearer's peripheral vision for **optimization**.
- The technique is best paired with HMD's eye tracking camera.
- Not using the eye tracking camera is called **Fixed Foveated Rendering**

### **V**irtual **R**eality **C**heck
Content Guidelinesg, technical and performance requirements for a VR application/game. 

- [Meta VRC](https://developer.oculus.com/resources/publish-prep-app)
