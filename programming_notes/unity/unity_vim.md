Unity Vim Setup
===============
> Neovim setup
[Guide](https://chrislabarge.com/posts/neovim-unity-engine/)

### In NeoVim
`:LspInstall omnisharp`
`:TSInstall c_sharp`

### In Unity
*Edit -> Preferences*
**External Script Editor Args:** `--remote-silent +$(Line) $(File)`

### Create Launch Script

```sh
#!/bin/bash

$TERMINAL -e nvr --servername unity $@
```

Other links:
https://damopewpew.github.io/nvim/wsl/godot/unity3d/vim/2020/02/29/wsl-nvim-as-external-editor.html
https://www.jhonatandasilva.com/published/1623278444
https://rudism.com/coding-csharp-in-neovim/
