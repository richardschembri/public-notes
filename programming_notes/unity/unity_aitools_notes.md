Unity AI Tools
==============

Official Tools
--------------

### [Muse](https://blog.unity.com/engine-platform/introducing-unity-muse-and-unity-sentis-ai)
Enables you to create almost anything in the Unity Editor using natural input such
as text prompts and sketches.

#### Muse Chat
Using **Muse Chat**, you can leverage AI-based search across:
- Unity documentation
- Training resources
- Support content 

**Muse Chat** helps you find relevant information including:
-  working code samples

#### Upcoming features
- Create textures 
- Create sprites 
- Fully animate a character

### [Sentis](https://blog.unity.com/engine-platform/introducing-unity-muse-and-unity-sentis-ai)
- Bridges neural networks with the Unity Runtime.
- Allows AI models to run on any device where Unity runs (Ex. Android, Nintendo Switch, etc)
- Embeds AI models into a real-time 3D engine.
- For example an NPC that can chat with the player.

Verified Tools
--------------

### Convai
- Similar to Sentis

### Inworld AI
- Similar to Sentis

### Layer AI
- 2D asset generation tool

### Leonardo.ai
- 2D asset generation tool
- 3D model texture generator

### Polyhive
- 3D texture generator

### modl:test
- Automated gameplay testing tool
- Generates heatmaps

### Zibra Effects
- Generates real time effects(simulation + realistic physics)

### LMNT
- AI voice acting
