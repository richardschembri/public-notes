External Editor
---------------
### Regenerate Project Files
Make sure that the `Engineering` package is installed in the `Package Manager`


OpenSUSE Install UnityHub
-------------------------
> As of 22/09/26

Add RHEL repo to zypper
```sh
sudo zypper ar -fep93 'https://hub.unity3d.com/linux/repos/rpm/stable' unityhub
```

The given parameters are: `-f` to refresh, `-e` to enable, `-p93` to set the priority
to *93* and `-n` to give the repo a name. 

RHEL UnityHub requires the `libuuid`, in openSUSE it is named `libuuid1` so make sure
the package is installed beforehand.

Run the following command and ignore the `libuuid` missing dependency error.
```sh
sudo zypper in unityhub
```
