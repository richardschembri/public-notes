Unity Maths
===========
- Unity works with the left hand rule coordinates
	- **Y:** Index Finger pointed up
	- **X:** Thumb forming an **L** shape with index Finger
	- **Z:** Pointing away perpendicular to **Y**

- The following are computationally expensive
	- sine
	- cosine
	- tangent
	- square root (√)

Mathf
-----
Check if there is difference in Float

```csharp
if(Mathf.Abs(floatA - floatB) > Mathf.Epsilon) {}
```
	
[Vector Maths](https://learn.unity.com/tutorial/vector-maths#5c8a4304edbc2a0020d97735)
-------------------------------------------------------
> https://www.youtube.com/watch?v=e3z91RqZPAk
Vector Maths is **NOT computationally expensive**
#### Vector Normalization
- Normalized vector keeps the **same direction** but its **length** is **1.0**(Unit Length).
- Vectors Should Be **normalized** when used **to move an object** so that you can
    move an object by `x` units .

### Vector Calculus
- **Origin :** 0, 0. Vectors are are all relative to it.
- **Magnitude :** Is the length of a vector (distance between two points)
	- `Vector2.magnitude` = `√(x² + y²)`
	- `Vector3.magnitude` = `√(x² + y² + z²)`
	- Is a way of getting the distance between two vectors
- **Direction :** `Vector A->B = B - A`
- **Distance  :** `Distance A->B = √(A->Bx² + A->By²)`
		- When comparing distances you can skip using `√`
		
#### Types
- **Unit Vector :** Any vector that has a magnitude equal to 1.
- **Normal Vector :** A normal is an object such as a line, ray, or vector that
	is perpendicular to a given object..

--------------------------------------------------------------------------------

#### Dot product
> The dot product is a fundamental way we can combine two vectors. Intuitively,
 it tells us something about how much two vectors point in the same direction.

- (Ax * Bx) + (Ay * By) + (Az * Bz) = Dot Product
- Dot Product = 0 = `Perpendicular`
- Positive Dot Product = Plane angle up
- Negative Dot Product = Plane angle down

a
|
|         b
|        /  
|       /
|      / 
|     /   ↑
|    /   a●b  
|   /     ↓
|  /
| /
|/___________________________

| Angle | Dot     |
| :---- | :------ |
| 0°    | 1       |
| 30°   | 0.866   |
| 45°   | 0.7071  |
| 60°   | 0.5     |
| 90°   | 0       |
| 120°  | -0.5    |
| 135°  | -0.7071 |
| 150°  | -0.866  |
| 180°  | -1      |

The interactions between similar dimensions (x*x, y*y, z*z)

#### Cross product
- The interactions between different dimensions (x*y,y*z, z*x, etc.)
- Gets a vector perpendicular to two Vectors
- It is denoted by the `^` character
- A ^ B = C (Perpendicular vector)
- Use the "left hand rule" to confirm the direction
	- A: Thumb
	- B: Index finger
	- C: Middle fingler
- Cm = Am * Bm * sine(angle AB) // where m = Magnitude

```sh
	 B
	 |
	 |
	C|
	↑|____ A
   <hand>
```

--------------------------------------------------------------------------------

[Sine Waves `∿∿∿`](https://www.youtube.com/watch?v=pEXdTLsEAjk&list=PLPV2KyIb3jR5yFBxSe9MsDiJgc04dDAxk&index=2)
-----------------------------------------------------
- `y = sin(x)`
- Oscilates between 1 and -1

### Visualize from Circle
- Imagine a circle of radius 1 and center 0,0
- Line of center to a point on the circle it is a hyptenuse of a triangle
- The angle of the left hand corner of the triangle is `x` and the `y` is the y-axis
	of the point on the circle

### Example Uses:
- Ocean Waves
- Move enemies
- Generate sound
- Float Animation

--------------------------------------------------------------------------------

[Forces / Dynamics](https://www.youtube.com/watch?v=HEJ_UtSbinY&list=PLPV2KyIb3jR5yFBxSe9MsDiJgc04dDAxk&index=3)
- Push and pull on an object
- Standard measurement for Force is Newtons (N)

### Useful formulas
- `a = F/m` == Acceleration = Force / Mass
	- *Example:* `a = 400N/50kg = 8m/s²`
- `d = ½a * t²` == Distance = ½Acceleration * Time²
	- *Example:* `d = ½ * 8m/s² * (1s)² = 4m`

### Gravitational Forces
- `F = ((m1 * m2)/d²) * G` == Attraction Force = ((Mass1 * Mass2) / Distance) * Gravitational Constant

--------------------------------------------------------------------------------

[Linear Interpolation](https://www.youtube.com/watch?v=DPfxjQ6sqrc?t=2100)
--------------------------------------------------------
### Example with x,y axis
```sh
      P2 / 100%
        /
       /
      /  50%     5 seconds
     /
    /
P1 / 0%
```
`Ptx = P1x + (P2x - P1x) * t`
`Pty = P1y + (P2y - P1y) * t`
Where `Pt` is point in time from 0 to 1
Assuming 0.1s per frame
`T = T + 0.1`
Where `T` is time

`Nt = Nstart + (Nend - Nstart) * t`
--------------------------------------------------------------------------------

Links
-----

- [Essential Mathematics For Aspiring Game Developers](https://www.youtube.com/watch?v=DPfxjQ6sqrc)
