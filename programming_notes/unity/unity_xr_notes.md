Unity XR notes
==============

VR
----
### Oculus
#### Oculus Interaction SDK
##### [Curved Canvas](https://developer.oculus.com/documentation/unity/unity-isdk-canvas-integration/)

1. Add OculusInegrationSampleRig
2. Add RoomEnviroment
3. Add OculusInteractionRayCanvas
4. Adjust `CanvasCyinder`'s *Curve Radius*
5. In event system remove `StandaloneInputModule` and replace it with `PointableCanvasModule`

##### Check Controller Active
```csharp
private void Update() {
	if (OVRInput.GetActiveController() == OVRInput.Controller.LTouch) {
		Debug.Log("ONLY L TOUCH is ACTIVE");
	}
	if (OVRInput.GetActiveController() == OVRInput.Controller.RTouch) {
		Debug.Log("ONLY R TOUCH CONTROLLER IS ACTIVE");
	}
	if (OVRInput.GetActiveController() == OVRInput.Controller.Touch) {
		Debug.Log("BOTH CONTROLLERS ARE ACTIVE");
	}
	if (OVRInput.GetActiveController() == OVRInput.Controller.None) {
		Debug.Log("NO CONTROLLER IS ACTIVE");
	}
}
```

### Debug APK
1. Open **Oculus Developer Hub**
2. Device Manger -> Open Logs
3. Filter for "Unity"

MR
----

### Hololens 2
#### [Build](https://www.linkedin.com/pulse/how-create-unity-app-installer-hololens-2-appx-install-ivana-tilca/)
1. Build from Unity
2. Open generated solution
3. Set Solution configuration to `Master` and `ARM64`
4. Under **Solution Explorer** right click the project -> `Publish` -> `Create App Packages`
5. Select **Sideloading**
6. Select or Create cerificate
7. Tick only `ARM64` *Architecture* and set *Solution Configuration* to `Master (ARM64)` 
8. `appx` is found in *Output location*

Open XR
-------
### [Setup](https://www.youtube.com/watch?v=UWSClCz0c_0)
1. Package Manager -> Packages:Unity Registry
2. Install **XR Plugin Management**
3. Install **Open XR Plugin** (New **Input System** is automatically installed with it)
3. Install platform specific plugin example: **Oculus XR Plugin**
4. Install **Universal RP** for low spec HMDs
4. Create `Settings/URP` folder
5. Right Click -> Create -> Rendering -> Universal Render Pipeline -> Pipeline Asset(Forward Renderer)
6. Click on the "Renderer" file
7. *Enable* Post-processing
8. Add **New Screen Space Ambient Occlusion**
9. Set **Source**: Depth
10. Adjust Settings file like shadows etc
11. Edit -> Project Settings -> Graphics
12. Set **Scriptable Render Pipeline Settings**
11. Edit -> Project Settings -> XR Plug-in Management
12. Tick **Initialise XR on Startup**
13. Tick **Open XR**
14. Click yellow triangle and click **Fix all**
15. Edit -> Project Settings -> XR Plug-in Management
16. Add to **Interaction Profiles** all relevant HMD Profiles

XR Interaction Toolkit
----------------------

### [XRRayInteractor](https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@2.0/api/UnityEngine.XR.Interaction.Toolkit.XRRayInteractor.html)
Interactor used for interacting with interactables at a distance.

#### `Attach Transform`
Where you wish to attach the ray (and take position and rotation)

### [XR Interactable](https://docs.unity3d.com/Packages/com.unity.xr.interaction.toolkit@2.0/manual/xr-grab-interactable.html)
#### Interactions

| Action  | XR Action |
| :----:  | :-------: |
| TOUCH   | HOVER     |
| GRAB    | SELECT    |
| TRIGGER | ACTIVATE  |


### [XR Device Simulator](https://www.youtube.com/watch?v=n8RWRt8IwrQ)
- Unity's simulator for using an HMD
- It is a gameobject you need to place in scene
- It conflicts with XR origin

Optimization
------------

### Lighting
`Window` -> `Rendering` -> `Lighting` 
- Disable Realtime lighting
- Enable `Auto Generate`

### Quality
`Edit` -> `Project Settings` -> `Quality` 
- Only enable levels up to `Medium`
- Select `Medium`
	- Set to 0:
		- `Pixel Light Count`
		- `Resolution Scaling Fixed DPI Factor`
	- Disable:
		- `Anti Aliasing`
		- `Realtime reflection probes`
		- `Billboards Face Camera Position`
		- `Shadows` (Disable Shadows)

