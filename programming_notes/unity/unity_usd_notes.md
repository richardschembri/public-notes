[Unity USD(Universal Scene Description)](https://blog.unity.com/technology/pixars-universal-scene-description-for-unity-out-in-preview)
========================================
> Github: https://github.com/Unity-Technologies/usd-unity-sdk

Installation
------------
**Package name:** `com.unity.formats.usd`

Import USD
----------
To import the assets make sure to click the root of the USD object and toggle
`Payload Policy` to **Load All**

USD Payloads
------------
USD assets authored with Payloads will appear empty initially. Payloads can be
loaded and unloaded using the `USD` menu.

Steaning Playback
-----------------
- USD animation caches and skeletal animation be streamed into Unity using `timeline`.
- Animation is streamed from disk on demand.

### Timeline Recorder
`Timeline USD Recorder` track can be added to capture animation over a given frame range.

Attribute Inspector
-------------------
- Used to view USD data.
- **USD** `Prim` is equivalent to unity's `GameObject`.

