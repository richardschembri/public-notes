Unity Shader
============

Coding
------

### Anotomy of a shader
```cpp
Shader "MyShader"
{
    Properties
    {
        // The properties of your shaders
        // - textures
        // - colours
        // - parameters
        // ...
    }

    SubShader
    {
        // The code of your shaders
        // - surface shader
        //    OR
        // - vertex and fragment shader
        //    OR
        // - fixed function shader
    }    
}
```

### Properties

```cpp
_PropertyName("Display Name", DataType) = Value
```

**Example:**
```cpp
Properties
{
	_MyTexture ("My texture", 2D) = "white" {}
	_MyNormalMap ("My normal map", 2D) = "bump" {}  //Grey
	_MyInt ("My integer", Int) = 2
	_MyFloat ("My float", Float) = 1.5
	_MyRange ("My range", Range(0.0, 1.0)) = 0.5

	_MyColor ("My colour", Color) = (1, 0, 0, 1)    // (R, G, B, A)
	_MyVector ("My Vector4", Vector) = (0, 0, 0, 0)    // (x, y, z, w)
}
```

**Map `properties` to `SubShader` `variables`**

```cpp
SubShader
{
    // Code of the shader
    // ...
    sampler2D _MyTexture;
    sampler2D _MyNormalMap;

    int _MyInt;
    float _MyFloat;
    float _MyRange;
    half4 _MyColor;
    float4 _MyVector;

    // Code of the shader
    // ...
}
```

### Tags
```cpp
SubShader
{
    Tags
    {
        "Queue" = "Geometry"
        "RenderType" = "Opaque"
    }
    CGPROGRAM
    // Cg / HLSL code of the shader
    // ...
    ENDCG
}
```

#### Queue
Gives control on the rendering order of each material. Queue accepts integer positive
numbers (the smaller it is, the sooner is drawn); mnemonic labels can also be used:

- **Background (1000):** used for backgrounds and skyboxes,
- **Geometry (2000):** the default label used for most solid objects,
- **Transparent (3000):** used for materials with transparent properties, such as
							glass, fire, particles and water;
- **Overlay (4000):** used for effects such as lens flares, GUI elements and texts.

--------------------------------------------------------------------------------

[Rendering Process](https://learn.unity.com/tutorial/rendering-and-shading#5c7f8528edbc2a002053b539)
------------------------------------------------------------

1. **3D Model**
	- Vertex positions (model points)
	- Vertex colours
	- Normals (Vertex directions)
	- UV data (texture mapping)

2. **Material**
	- Textures
	- Shader property values

3. **Shader**
	- CG / HLSL code
	- CG / HLSL code (alternative version)

--------------------------------------------------------------------------------

Shader Types
------------
#### Surface Shader
- Whenever the material you want to simulate needs to be affected by lights in a realistic way,
- Surface shaders hide the calculations of how light is reflected and allows to
	specify “intuitive” properties such as the albedo, the normals, the reflectivity
	and so on in a function called surf
- These values are then plugged into a lighting model which will output the final
	RGB values for each pixel. Alternatively, you can also write your own lighting
	model, but this is only needed for very advanced effects.

```cpp
CGPROGRAM
	// Uses the Lambertian lighting model
	#pragma surface surf Lambert

	sampler2D _MainTex;    // The input texture

	struct Input {
		float2 uv_MainTex;
	};

	void surf (Input IN, inout SurfaceOutput o) {
		o.Albedo = tex2D (_MainTex, IN.uv_MainTex).rgb;
	}
ENDCG
```

#### Vertex and Fragment Shader
- **Vertex and fragment shaders** work close to the way the GPU renders triangles, and
	have no built-in concept of how light should behave. 
- `vert`: The **geometry** of your **model** is first passed through a function called `vert`
	which can **alter its vertices**. 
- `frag`: Then, individual triangles are passed through another function called
		`frag` which decides the final RGB colour for every pixel.
- They are useful for 2D effects, postprocessing and special 3D effects which are
	too complex to be expressed as surface shaders.


```cpp
// Make an object uniformly red, with no lighting
Pass {
    CGPROGRAM

    #pragma vertex vert             
    #pragma fragment frag

    struct vertInput {
        float4 pos : POSITION;
    };  

    struct vertOutput {
        float4 pos : SV_POSITION;
    };

    vertOutput vert(vertInput input) {
        vertOutput o;
        o.pos = mul(UNITY_MATRIX_MVP, input.pos);
        return o;
    }

    half4 frag(vertOutput output) : COLOR {
        return half4(1.0, 0.0, 0.0, 1.0); 
    }
    ENDCG
}
```

--------------------------------------------------------------------------------

C# Access
---------

```csharp
	myRenderer.material.SetFloat("_Amount", 1f);
	myRenderer.material.SetFloat("_CutoutThresh", .29f);
	myRenderer.material.SetFloat("_Amplitude", Random.Range (100, 250));
	myRenderer.material.SetFloat("_Speed", 1f);
```

--------------------------------------------------------------------------------

[Material Parameters](https://docs.unity3d.com/Manual/StandardShaderMaterialParameters.html)
---------------------------------------------------------------

### Glossary

| Parameter                        | Description                                                                                                                                                                   |
| :--------                        | :----------                                                                                                                                                                   |
| **Albedo**                       | The base color of the surface                                                                                                                                                 |
| **Albedo Texture Map**           | Represent the colors of the surface of the object                                                                                                                             |
| **Transparency**                 | Controls the transparency level for the non opaque material                                                                                                                   |
| **Specular**                     | The direct reflections of light sources in your Scene                                                                                                                         |
| **Metallic**                     | Have control over how metallic or non-metallic a surface is                                                                                                                   |
| **Smoothness**                   | Less smooth surfaces reflect light over a wider range of angles (as the light hits the bumps in the microsurface)                                                             |
| **Normal Map(Bump mapping)**     | Add surface detail such as bumps, grooves, and scratches to a model which catch the light as if they are represented by real geometry                                         |
| **Surface Normals**              | A normal of the surface that ignores the polygon normals to have a more even shading                                                                                          |
| **Normal mapping**               | Uses a texture to store information about how to modify the surface normals across the model                                                                                  |
| **Secondary Normal Maps**        | Additional normal map for creating extra detail                                                                                                                               |
| **Heightmap**                    | (*performance-expensive*) Shifts the areas of the visible surface texture around, to achieve a kind of surface-level occlusion effect (rendering large bumps and protrusions) |
| **Occlusion Map**                | Provide information about which areas of the model should receive high or low indirect lighting                                                                               |
| **Emission**                     | Makes a material appear as a visible source of light                                                                                                                          |
| **Secondary Maps (Detail Maps)** | Allow you to overlay a second set of textures on top of the main textures(ex. albedo color map, normal map)                                                                   |
| **Detail Mask**                  | Mask off certain areas of your model to have the detail texture applied                                                                                                       |
| **The Fresnel Effect**           | Reflections that only appear around the edges of the sphere (that’s when its surface is at a grazing angle)                                                                   |
| **UV mapping**                   | Process of projecting a 2D image to a 3D model's surface for texture mapping.                                                                                                 |

--------------------------------------------------------------------------------

[Shader Graph](https://www.youtube.com/watch?v=CDbt6u4S2NE&list=PLX2vGYjWbI0RyhAsNJg4sLLKgCZsRSim2&index=2)
-------------------------------------------------------


