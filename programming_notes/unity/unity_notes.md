Unity Engine Notes
==================

Quick Notes
-----------
### [Presets](https://docs.unity3d.com/Manual/Presets.html)
Presets are assets that you can use to save and apply identical property settings
across multiple:
- components
- assets
- Project Settings windows. 

You can also use Presets to specify default settings for new components and default
import settings for assets in the Preset Manager The Preset Manager supports any
importers, components, or scriptable objects you add to the Unity Editor.

[Best Practices](https://docs.unity3d.com/2019.3/Documentation/Manual/BestPracticeGuides.html)
----------------------------------------------------------
- Always cache reference to components you need to use your scripts.
- Object pooling
- **Layers and collision matrix :** For each new layer, a new column and row are
	added on the collision matrix. This matrix is responsible for defining interactions
	between layers.
- Raycasts

### Common ways to optimize a 2D game
- use a sprite atlas
- carefully optimize the size of your images ; many people make images far too large
- try to never do anything in an Update() method
- consider using layers
- use pooling
- consider using animation over sprite sheets

### Optimization
#### Profilers
- **iOS:** *Instruments* and the *XCode Frame Debugger*
- **Android:** Snapdragon Profiler
- **Intel CPUs/GPUs:** *VTune* and *Intel GPA*
- **PS4:** *Razor suite* and *VR Trace*
- **Xbox:** *Pix tool*

### Links - Best Practices 
- [Unite 2016: Optimizing Mobile Applications](https://www.youtube.com/watch?v=j4YAY36xjwE)
 
--------------------------------------------------------------------------------

Source Code
-----------
## MonoBehaviour
- **FixedUpdate** - It is called every fixed frame-rate frame. FixedUpdate message
	for physics calculations.

### Execution order
1. **Awake** - Awake is called after all objects are initialized and when the script
	object is initialised.
2. **OnEnable**
3. **Start** - Start is called on the frame when a script is enabled just before
	any of the Update methods are called the first time.
4. **Update** - Called every frame.
5. **LateUpdate** - Called after all Update functions have been called.
6. **OnGUI** - called for rendering and handling GUI events.
7. **OnApplicationQuit**
8. **OnDisable** -
9. **OnDestroy** -

--------------------------------------------------------------------------------

Textures + Meshes
-----------------
Texture and Meshes are elements stored in GPU memory.

### Property Block

--------------------------------------------------------------------------------

[Platform dependent compilation](https://docs.unity3d.com/2019.1/Documentation/Manual/PlatformDependentCompilation.html)
--------------------------------------------------
- #if
- UNITY_EDITOR
- etc

### [C# preprocessor directives](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/preprocessor-directives)

[Assembly Definition](https://www.youtube.com/watch?v=HYqOSkHI674&t=2s)
--------------------------------------------------
- Structure code into a dll type asset
- Does not need to be recompiled every time you change the code of your project
- Add dependencies of the Assembly Definition for code that references it

[Testing](https://www.youtube.com/watch?v=qCghhGLUa-Y)
--------------------------------------------------

### [Unit Testing](https://www.youtube.com/watch?v=vLvA7ZkFGRo)
#### Quick Start
1. Window -> Gerneral -> Test Runner
2. EditMode -> **Create EditMode Test Assembly Folder**
3. **Create Test Script in current Folder**
4. Name in **lowercase**
5. Right click **Scripts** folder and *Create -> Assembly Definition*
6. In the inspector of the Test Asssembly add the Scripts Assembly to **Assembly
	Definitions References**
7. Use `[Test]` on top of functions
8. Window -> Test Runner
9. Edit Mode -> Run Selected

### Manual Testing
Just normal testing

### End to End
- Like a bot running the game
- External runner
- Hard to build and maintain

### Integration
- Testing integration between 2 system Ex: Server to Database integration

--------------------------------------------------------------------------------

Threads
-------

- Unity runs on a single thread
- Fetching the Transform reference isn't thread safe in Unity
- When using threads, we must avoid using native Unity structures like the `Mathf`
    and `Random` classes
### Examplle:
```csharp
		
Unity 3D Developer Interview Question:
Can threads be used to modify a Texture on runtime?
Can threads be used to move a GameObject on the scene?
Consider the snippet below:
class RandomGenerator : MonoBehaviour
{
public float[] randomList;

void Start()
{
randomList = new float[1000000];
}

void Generate()
{
System.Random rnd = new System.Random();
for(int i=0;i<randomList.Length;i++) randomList[i] = (float)rnd.NextDouble();
}
}
Improve this code using threads, so the 1000000 random number generation runs without spoiling performance.
Submitted by: Muhammad
No. Texture and Meshes are examples of elements stored in GPU memory and Unity doesn't allow other threads, besides the main one, to make modifications on these kinds of data.
No. Fetching the Transform reference isn't thread safe in Unity.
When using threads, we must avoid using native Unity structures like the Mathf and Random classes:
class RandomGenerator : MonoBehaviour
{
    public float[] randomList;
    void Start()
    {
        randomList = new float[1000000];
        Thread t = new Thread(delegate()
        {
            while(true)
            {
                Generate();
                // Slow down as not to spoil performace
                Thread.Sleep(16); // trigger the loop to run roughly every 60th of a second
            }
        });
        t.Start();
    }

    void Generate()
    {
        // Use instead of UnityEngine.Random
        System.Random rnd = new System.Random();
        for(int i=0;i<randomList.Length;i++) 
        {
            randomList[i] = (float)rnd.NextDouble();
        }
    }
}
```

--------------------------------------------------------------------------------

Render Pipeline
---------------
- Rendering Objects
- Culling
- Post Processing
### Ways to render
- Multi-pass
- Single-pass
- Deferred

### Choices
- HDR or LDR?
- MSAA or PPAA?
- PBR or Simple Materials?
- Lighting?
- Shadows?

--------------------------------------------------------------------------------

[Shader](https://molo17.com/2019/05/dont-let-the-shaders-scare-you-part-2-unity/)
--------------------------------------------


--------------------------------------------------------------------------------

Scriptable Objects
------------------
- Serializable Unity class
- Similar to `MonoBehaviour` but
	- **No** `Transform`
	- **No** `GameObject`
- Can save one or more in an `.asset` file
- Can save an instance at runtime

### Simple use cases
- Game config file
- Inventory
- Enemy Stat
- Audio Collection


### ScriptableObject Variables
Can be this simple:
```csharp
[CreateAssetMenu]
public class FloatVariable : ScriptableObject
{
	public float Value;
}
```

To simplify workflow  do the following
```csharp
[Serializable]
public class FloatReference
{
	public bool UseConstant = true;
	public float ConstantValue;
	public FloatVariable Variable;
}
```
### Eventable Architecture

```csharp
[CreateAssetMenu]
public class GameEvent : ScriptableObject
{
	private List<GameEventListener> listeners =
		new List<GameEventListener>();
	
	public void Raise()
	{
		for(int i = listeners.Count - 1; i >= 0; i--)
			listeners[i].OnEventRaised();			
	}
	
	public void RegisterListener(GameEventListener listener) ...
	public void UnRegisterListener(GameEventListener listener) ...
}
```

### Runtime Sets
> Avoid race conditions
public abstract class RuntimeSet<T> : ScriptableObject
{
	public List<T> Items = new List<T>();
	public void Add(T t){
		if(!Items.Contains(t)) Items.Add(t);
	}
	
	public void Remove(T t) {
		if (Items.Contains(t)) Items.Remove(t);
	}
}

### Enum
Can be used to replace Enums

--------------------------------------------------------------------------------

Prefabs
-------

### Prefab Variants
- A `prefab` that updates with changes done in the main prefab
- A `prefab variant` can have it's own `prefab variant`

--------------------------------------------------------------------------------

Persist data between scenes
--------------------------

### PlayerPrefs
Preserves data even between game executions

### Static Class Data
Data lives between scene changes

### DontDestroyOnLoad
Flags a gameobject do not be destroyed when a scene unloads. Best used with `Singlton`s
as not to have duplicates.
```csharp
GameObject.DontDestroyOnLoad(this.gameobject);
```

--------------------------------------------------------------------------------

Materials
---------
### [Physics Materials](https://docs.unity3d.com/Manual/class-PhysicMaterial.html)
#### Properties
**Dynamic Friction:** The friction used when already moving.
**Static Friction:** The friction used when an object is laying still on a surface[.](2023-02-25_..md)
