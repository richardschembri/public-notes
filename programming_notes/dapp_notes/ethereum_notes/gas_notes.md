# Gas                             
* Users have to pay **gas** every time they execute a function on a DAp      p.
* Users buy **gas** with **Ether**.
* **Gas cost** is based roughly on how much **computing resources**
        resources will be **required**.
        _Example : writing to storage > adding two integers_
* When you execute a function, _every node on the network_ needs to run
        the same function _to verify it's output_.
## Sidechain
* `sidechain`s don't necessarily need to pay gas.
* They could run with a different consensus algorithm.

## Types
Using `uint8` instead of `uint`(`uint256`) won't save you any gas.

### Exception // struct packing
There's an exception inside `struct`'s.
If you have multiple `uints` inside a `struct`, using a smaller-sized
`uint` when possible will allow Solidity to pack these variables
together to take up less storage. 

``` solidity
struct NormalStruct {
  uint a;
  uint b;
  uint c;
}

struct MiniMe {
  uint32 a;
  uint32 b;
  uint c;
}

// `mini` will cost less gas than `normal` because of struct packing
NormalStruct normal = NormalStruct(10, 20, 30);
MiniMe mini = MiniMe(10, 20, 30);
```
