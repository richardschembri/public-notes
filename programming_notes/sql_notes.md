# JOINS

## INNER JOIN
``` sql
select * from a INNER JOIN b on a.a = b.b;
select a.*, b.*  from a,b where a.a = b.b;
```
| a | b |
| - | - |
| 3 | 3 |
| 4 | 4 |

## LEFT OUTER JOIN
``` sql
select * from a LEFT OUTER JOIN b on a.a = b.b;
select a.*, b.*  from a,b where a.a = b.b(+);
```
| a | b    |
| - | ---- |
| 3 | null |
| 4 | null |
| 5 | 5    |
| 6 | 6    |

## RIGHT OUTER JOIN
``` sql
select * from a RIGHT OUTER JOIN b on a.a = b.b;
select a.*, b.*  from a,b where a.a(+) = b.b;
```

| a    | b |
| ---- | - |
| 3    | 3 |
| 4    | 4 |
| null | 5 |
| null | 6 |

## FULL OUTER JOIN
``` sql
select * from a FULL OUTER JOIN b on a.a = b.b;
```

| a    | b    |
| ---- | ---- |
| 1    | null |
| 2    | null |
| 3    | 3    |
| 4    | 4    |
| null | 5    |
| null | 6    |
