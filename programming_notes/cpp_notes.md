# C++ Notes

## Quick Notes

'(*Owner).GetActorNameOrLabel();' is equal to 'Owner->GetActorNameOrLabel();'

### Build

Single File

```cpp
g++ main.cpp -o main
```

## 🐛Debug

### Debug via cli

> Make sure that `CFLAGS += -g -O0` is set in the `MakeFile`

```sh
# 1. Compile with debug symbols
g++ -g -o myprogram myprogram.cpp
# 2. Start GDB and load your program
gdb myprogram
# 3. Set breakpoint
(gdb) break 12
# 4. Run your program
(gdb) run
# 5. Step through the code line by line:
(gdb) step
# 6. Print variable values
(gdb) print x
# 7. Continue execution until the next breakpoint or the end:
 (gdb) continue
```

### Debug via TUI

use `gdbtui`

## Keywords

auto -> var

Class and header function do no need to use param names

### Include Libraries

#### Angle branches `<>`

- To access system header files use angle brackets. (Built-into C++ Standard Library)
- No need to type out `.h`

```cpp
#include <iostream>
```

#### Double quote `""`

- To access project header files use inverted commas
- Access **External** libraries

```cpp
#include "MyClass.h"
```

### Pointer

```cpp
int a;
a = 7;
int* b = &a;

b++; // increases pointer location 
(*b)++; // dereferences "b" and increases the value at that pointer
```

### Pragma

> A `pragma` (from the Greek word meaning action) is used to direct the actions
> of the compiler in particular ways, but has no effect on the semantics of a
> program (in general)

The `#pragma` directive is the method specified by the C standard for providing
additional information to the compiler, beyond what is conveyed in the language
itself.

**Example:**

```cpp
// This pragma causes the rest of the code in the current file to be treated as
// if it came from a system header
#pragma GCC system_header
```

#### [`#pragma once`](https://en.wikipedia.org/wiki/Pragma_once)

Ignore subsequent inclusions of file

Without `#pragma once` the following would cause a compilation error,
because a struct with a given name can only be defined a single time in a given
compilation.

```cpp
// grandparent.h
#pragma once

struct foo 
{
    int member;
};
```

Removes duplicates of `include`

```cpp
// parent.h
#include "grandparent.h"
```

```cpp
// child.c
#include "grandparent.h"
#include "parent.h"
```

### define

#### ifdef (If _Defined_)

```cpp
#define luzzu

#ifdef luzzu
// Run this code if luzzu is DEFINED
#endif
```

#### ifndef (If _Not_ Defined)

```cpp
#define luzzu

#ifndef luzzu
// Run this code if luzzu is NOT defined
#endif
```

---

## [Comparisons to C#](https://www.youtube.com/watch?v=gRxi1eCV4xI)

### Advantages

- Speed
- Portable
- Custom device support

### Linker

- There is no Linker in C#
- To the C++ compiler all files are separate whereas C# automatically groups them together.
- All files are compiled into object files
- The linker puts all object files together
- You can link in static libraries so they become part of the executable.
- C# doesn't have static libraries, it only has dynamic libraries which means they are seperate from the executable. So when a C# app runs it has to read through all the DLLs.

### Memory Management

#### `delete`

You have to make sure to `delete` your objects when you are done with them.

```cpp
Person* person = new Person;
delete person;
```

Cannot be used to delete _arrays_

---

### Classes

- They are declared in the header file
- They are implemented in the cpp file

MyClass.hpp

```cpp
class MyClass {
public:
    int Add(int a, int b);
}
```

MyClass.cpp

```cpp
#include "MyClass.hpp"

int MyClass::Add(int a, int b) {
    return a + b; 
}
```

### Objects

```cpp
// This automatically calls the constructor and is allocated on Stack instead of Heap
Person p; 
// This type of object uses "." to access variables and funcitons
p.Name = "Vash";
Hobby hobby("gun slinging"); // Constructor with a parameter

// This allocates to heap
Person *p2 = new Person;
// Since it is a pointer "->" is used to access variables and funcitons
p2->Name = "Legato";
```

#### Passing objects to a function

```cpp
void ProcessPersonByValue(Person p){
    p.Name = "abc";
}

void ProcessPersonByReference(Person *p){
    p->Name = "abc";
}

int _tmain(int argc, _TCHAR* argv[]){
    
    Person p; 
    ProcessPersonByValue(p);
    ProcessPersonByReference(&p);
}
```

#### Create object in a function

```cpp
Person CreatePersonByValue(string name){
    Person p;
    p.Name = name;
    return p; // Returns a copy / note that pointers are also copied and point to the same thing    
}

Person* CreatePersonByReference(string name){
    Person* p;
    p->Name = name;
    return p; // Be sure to delete the object, in a complicated program it might be difficult to know when to delete it.
}

// You need this for the below function: #include <memory>
unique_ptr<Person> CreatePersonAutoDelete(string name)
    unique_ptr<Person> p = make_unique<Person>();
    p->Name = name;
    return p; // This is automatically deleted when the application is closed
}
```

---

### Interface

C++ doesn't have interfaces but it's `header files` can be used as interfaces.

### Variables

#### `var`

Newer versions of C++ use `auto` to infer type

````cpp
auto person = CreatePerson("Vash");
delete person;

#### `int`
It varies depending on platforms because it was not standardized.
Use these instead `int32_t`

#### `char`, `string`
C# has native support for `utf8` but due to legacy code. `wchar_t` and `wstring`
are closer to unicode support but more setup might be needed.

### Properties
There is no native support for properties in C++.

You can use the Microsoft `__declspec` to get around it
```cpp
// declspec_property.cpp
struct S {
   int i;
   void putprop(int j) {
      i = j;
   }

   int getprop() {
      return i;
   }

   __declspec(property(get = getprop, put = putprop)) int the_prop;
};

int main() {
   S s;
   s.the_prop = 5;
   return s.the_prop;
}
````

---

### Forward Declaration

Adding `class` keyword to types inside header files to avoid including it's header. The children will implement the includewhere needed.

## Other Notes

## Enums

```cpp
enum Side { left, right, top, bottom };
Side mySide = Side::top;
```

## Casting

> Cast to float

```cpp
static_cast<float>(window_height)
```

## Initalization

### Braced Initalization

```cpp
double cheese_burger{5.99};
```

**Empty braces** initializes to default value

```cpp
double cheese_burger{}; // Default value is 0
bool shouldHaveLunch{}; // Default value is false
```

## Links

- https://www.youtube.com/watch?v=GWDJyV04ghs
