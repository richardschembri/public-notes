XR UI Notes
===========

Diegetic UI
-----------
Exists inside of the world of the player's character as opposed to **Non-Diegetic UI**
overlays common in most games

### Examples:
- **Non- Diegetic:** Show fps ammo in HUD 
- **Diegetic:** Show fps ammo on character gun

dmm(Distance-intependent millimeter)
------------------------------------
*x*mm at **x** meters away
### Examples:
- 1mm at 1 meter away
- 2mm at 2 meters away
- 3mm at 3 meters away
