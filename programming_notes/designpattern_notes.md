Programming Design Patterns
============================
Naming Convention
-----------------
### [Letter case-separated words](https://en.wikipedia.org/wiki/Naming_convention_(programming)#Letter_case-separated_words)

| Formatting | Name(s)                                         |
| :--------- | :---------------------------------------------- |
| twowords   | flat case                                       |
| TWOWORDS   | upper flat case                                 |
| twoWords   | (lower) camelCase, dromedaryCase                |
| TwoWords   | PascalCase, UpperCamelCase, StudlyCase          |
| two_words  | snake_case, pothole_case                        |
| TWO_WORDS  | SCREAMING_SNAKE_CASE, MACRO_CASE, CONSTANT_CASE |
| two_Words  | camel_Snake_Case                                |
| Two_Words  | Pascal_Snake_Case                               |
| two-words  | kebab-case, dash-case, lisp-case                |
| TWO-WORDS  | TRAIN-CASE, COBOL-CASE, SCREAMING-KEBAB-CASE    |
| Two-Words  | Train-Case, HTTP-Header-Case                    |

--------------------------------------------------------------------------------

Design Patterns
---------------
### Abstract factory Pattern
Which provides an interface for creating related or dependent objects without specifying
the objects' concrete classes.[3]

### Builder Pattern
Which separates the construction of a complex object from its representation so
that the same construction process can create different representations.

### Factory method Pattern
Which allows a class to defer instantiation to subclasses.[4]

### Prototype Pattern
Which specifies the kind of object to create using a prototypical instance, and
creates new objects by cloning this prototype.

### Singleton Pattern
Which ensures that a class only has one instance, and provides a global point of
access to it.[5]

### Observer Pattern
Have a static `event` which can be **observed** by other objects

### Command Pattern
Enque a `Queue` of **commands** and execute in order.

### Component Pattern
Composing objects with multiple components (The way unity does it)

### Flyweight Pattern
Reuse and share the same data

### State Pattern
FSM, etc
