# Lua

Shebang
-------
```lua
#!/usr/bin/env lua
```

table
-----

### Initialize empty
```lua
mytable = {}
```

[Enum](http://www.unendli.ch/posts/2016-07-22-enumerations-in-lua.html)
------
```lua
Colors = {
   BLUE = 1,
   GREEN = 2,
   RED = 3,
   VIOLET = 4,
   YELLOW = 5,
}

-- the above is equivalent to:
Colors = {
   ["BLUE"] = 1,
   ["GREEN"] = 2,
   ["RED"] = 3,
   ["VIOLET"] = 4,
   ["YELLOW"] = 5,
}

-- reading the integer back
local color = Colors.RED     -- the same as Colors["RED"]
```

[Constants](https://andrejs-cainikovs.blogspot.com/2009/05/lua-constants.html)
----------------------------------------------------------------------------
> By Lua's nature, it is not possible to create constants. There is a workaround, though, using metatables:
```lua
function protect(tbl)
    return setmetatable({}, {
        __index = tbl,
        __newindex = function(t, key, value)
            error("attempting to change constant " ..
                   tostring(key) .. " to " .. tostring(value), 2)
        end
    })
end

constants = {
    x = 1,
    y = 2,
}
constants = protect(constants)

print(constants.x) --> 3
constants.x = 3 --> error: attempting to change constant x to 3
```
if statement
------------
```lua
if ( not( a and b) )
then
   print("Condition 1 is true" )
elseif( c or d)
   print("Condition 2  is true" )
else
   print("No Condition is true" )
end
```

for loop
--------
### Array
```lua
	for i=1, #panels do
		panels[i].bg = low_level_panel_color
	end
```
