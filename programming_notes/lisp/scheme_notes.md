# [Scheme Notes](https://files.spritely.institute/papers/scheme-primer.html)

Math expr are prefix functions

```scheme
(+ 1 8 10)   ; equivalent to "1 + 8 + 10" in infix notation
```

### True/False

```scheme
#t
#f
```

### Lists

```scheme
(list 1 2 3)
; or
'(1 2 3)
```

Codes that writes code:

```scheme
'(+ 2 3)
; = (+ 2 3)
`(lambda (x) (* x 2))
; = (lambda (x) (* x 2))
```

#### cons (append to list)

```scheme
REPL> '()
; => ()
REPL> (cons 'a '())
; => (a)
REPL> (cons 'a (cons 'b (cons 'c '())))
; => (a b c)
```

#### car (grab first item in list)

```scheme
REPL> (car '(a b c))
; => a

; car some code
REPL> (car '(let ((name "Horace"))
				(string-append "Hello " name "!")))
; => let
```

#### cdr (grab rest of list)

```scheme
REPL> (cdr '(a b c))
; => (b c)
REPL> (car (cdr '(a b c)))
; => b

; cdr some code
REPL> (cdr '(let ((name "Horace"))
				(string-append "Hello " name "!")))
; => (((name "Horace")) (string-append "Hello " name "!"))
```

### let

```scheme
REPL> (let ((name "Horace"))
		(string-append "Hello " name "!"))
; => "Hello Horace!"
REPL> ((lambda (name)
		 (string-append "Hello " name "!"))
		"Horace")
; => "Hello Horace!"
```

### let*

`let*` is like `let`, but allows bindings to refer to previous bindings within the expression:

```scheme
REPL> (let* ((name "Horace")
			(greeting
				(string-append "Hello " name "!\n")))
		(display greeting))   ; print greeting to screen
; prints: Hello Horace!
```

### 🖊️ format

```scheme
REPL> (define (chatty-add chatty-name . nums)
        (format #t "<~a> If you add those together you get ~a!\n"
                chatty-name (apply + nums)))
REPL> (chatty-add "Chester" 2 4 8 6)
; Prints:
;   <Chester> If you add those together you get 20!
```

### if statement

```scheme
(if #t
	"This is true"
	"This is false")
```

#### ❓ Check if string

```scheme
(string? "apple") ; #t
```

### cond (switch case)

```scheme
;; Nested "if" version
(define (goldilocks n smallest-ok biggest-ok)
  (if (< n smallest-ok)
      "Too small!"
      (if (> n biggest-ok)
          "Too big!"
          "Just right!")))

;; "cond" version
(define (goldilocks n smallest-ok biggest-ok)
  (cond
   ((< n smallest-ok)
    "Too small!")
   ((> n biggest-ok)
    "Too big!")
   (else
    "Just right!")))
```

### equal? vs eq?

```scheme
REPL> (define a-list (list 1 2 3))
REPL> (define b-list (list 1 2 3))
REPL> (equal? a-list a-list)
; => #t
REPL> (eq? a-list a-list)
; => #t
REPL> (equal? a-list b-list)
; => #t
REPL> (eq? a-list b-list)
; => #f
```
