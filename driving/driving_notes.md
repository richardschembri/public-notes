# 🏎️ Driving Notes

- Look at mirrors on straight aways
- Hold steering 9-3
- Smooth steering
- Torque accelerates Horsepower maintains

## 🕹️ Shifting
- Smooth shifting
- Do not use engine brake
- Brake first, then downshift
- Rev engine slightly higher than required
- Heel toe every down shift
- Complete downshifts before entering a corner
- Shift as little as possible
- **Double Clutch**: clutch neutral →clutch gear

### 🔢 Sequential
- Still blip to downshift

## Camber
- **Negative camber**: / \
- **Positive camber**: \  /

## Caster
- Provides self centering effect of steering
- **Kingpin angle**: Angle of suspension
- Inclination angle of kingpin from the sideす
    - **Positive Caster**: Top of kingpin inclined to the rear
	- **Negative caster**: !!NEVER USED!!
- More positive => More self-center
- More positive => Heavier steering
- More positive =>  More negative camber on outside tire during cornering

## Toe
- From above:
    - `toe-in`: Tire front are closer together / \
    - `toe-out`: Tire rear are closer together \  /
- Straight line ability
- Front wheel:
    -  `toe-in` => **initial** understeer
	   -  `toe-out` => **initial** oversteer
- Rear wheel:
	-  `toe-out` =>  Instability + Unpredictable oversteer

## Ackerman steering
- Inside wheel of turn travels on a tighter radius
- Inside of front wheel must item scrubbing avoid scrubbing
- Anti-Ackerman steering: Inside wheel turns less than outside
- Both help with initial turn-in

## 🐰 Bump steer
- Should be **avoided**. Make vehicle very unstable
- Front OR Rear wheels begin to either `toe-in` OR `toe-out` from vertical suspension movement by a bump or body roll

## 🏊 Anti-dive
- Front end dive whilst braking

## 🏋️ Anti-squat
- Rear end squats whilst accelerating

## 🎢 Ride height
- Distance between road surface and lowest point on the car
- `rake`: Front lower than rear
- Lower the car:
    - More aerodynamic
    - Lower centre of gravity

## 🦿 Spring rate
- One of most important setup factors
- Force needed to deflect spring
- 📊 **Rating:**
    - Diameter of the spring wire
    - Overall diameter of spring
    - No. of coils or length of coils
- **Factors:**
    - Aerodynamic downforce
    - Weight of car
    - Track
    - etc.
- Generally: 
    - Softest rear for max traction
    - Balanced front for handling

## 🥏 Wheel rate
- Force needed to move a wheel a given distance
- Determined by:
    - Geometry of the suspension
    - Spring mounting location
    - `Spring rate`
- Amount of leverage a suspension system applies to the the spring

## 🔄 Anti-Roll Bar (Sway Bar)
- Resist lean
- Usually steel tube or solid bar
- Alter front or rear roll resistance
- `bar sweep`: 
    1. Adjust **front** bar from full soft to hard
    2. Adjust **rear** bar from full soft to hard
    3. Note change in handling
- Generally: 
    - Improve front grip = soften front OR stiffen rear
    - Improve rear grip = soften rear OR stiffen front
    - *Sometimes above is not the case*

## 🛹 Roll Stiffness
- Total amount of resistance to the car leaning/rolling by spring + anti-roll bar
- Roll stiffness distribution % = stiffness between front and rear suspension

## ⚡ Shock Rate
- Slow down + control spring oscillations
- Works sensitive:
    - Compression = bump
    - Extension = rebound
- Velocity sensitive
- Alters `transient handling characteristics`  (car response to inputs)
- shock absorber rate determines how quickly body roll occurs

## 📐Corner Weight
- If you place four tires of a vehicle on for separate scales they will give you the corner weights
- front-to-right & front-to-left weight distribution
- Oval tracks -> bias to one side or corner

## Tires
- Reading the tires:
    - Evaluate tire temperatures -> ties pressures are correct
    - Alignment settings -> Overall handling balance
- All is designed with optimal temperature range
- Operate ❗above❗ temperature range:
    - blister
    - chunk
    - wear quickly
 