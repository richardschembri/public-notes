# 📺 Monitor Notes

- [Sources](#sources)
- [📐 Aspect Ratio](#📐-aspect-ratio)
- [📏 Resolution + PPI(Pixels Per Inch)](#📏-resolution--ppipixels-per-inch)
- [🔲 Panel Types](#🔲-panel-types)
  - [TN (Twisted Nematic)](#tn-twisted-nematic)
  - [VA (Vertical Alignment)](#va-vertical-alignment)
  - [IPS (In-Plane Switching)](#ips-in-plane-switching)
  - [OLED (Organic Light Emitting Diode)](#oled-organic-light-emitting-diode)
- [🎨 Color Gamut](#🎨-color-gamut)
- [Refresh Rate (Hz)](#refresh-rate-hz)
- [IO (Input/Output)](#io-inputoutput)
- [Sensors](#sensors)
- [Avoid](#avoid)
- [✅ Recomendation](#✅-recomendation)
  - [Specs](#specs)

## Sources

- [The BEST PRODUCTIVITY Monitor GUIDE 2022](https://www.youtube.com/watch?v=XidFrsAO4i4)

## 📐 Aspect Ratio

- **16:9**
  - **Sizes**: 24", 27"(Most popular), 32", 43"
  - Very popular
- **21:9**
  - **Sizes**: 29", 34", 38", 40"
- **Super Ultra-wide**
  - **Sizes**: 43", 49"
  - Items at the edges are hard to focus on
- **16:18**
  - LG DualUp Ergo Monitor

## 📏 Resolution + PPI(Pixels Per Inch)

- Preferable PPI > 110 (iPhone 13pro has 450PPI)
- 27", 32": 4K is ideal
- Ultrawides: 5k2k is optimal

## 🔲 Panel Types

### TN (Twisted Nematic)

Oldest type of panels

#### Good

- Cheap
- Low Input Lag

#### Bad

- Viewing Angles
- Image Quality
- Colors

### VA (Vertical Alignment)

#### Good

- Price
- Low Input Lag

#### Bad

- Viewing Angles
- Color Change

### IPS (In-Plane Switching)

#### Good

- Viewing Angles
- Colors

#### Bad

- Expensive
- Glow

### OLED (Organic Light Emitting Diode)

#### Good

- Viewing Angles
- Color Fidelity
- Thin & Light

#### Bad

- **VERY** Expensive
- Brightness

## 🎨 Color Gamut

- Colors a monitor can produce
- Wider gamut means it can produce more colors within that space
- sRPG above 90%
- DCI-p3 above 90%

## Refresh Rate (Hz)

- 60Hz standard for Productivity monitors
- 120Hz is good for reduced eye strain

## IO (Input/Output)

- Power delivery above 60 watts
- Replaces dock
- Thunderbolt3 can daisy chain another monitor
- Headphone Jack

## Sensors

- Ambient sensor for automatic brightness

## Avoid

- Speakers

## ✅ Recomendation

### Specs

- 27" ~ 32"
- 4K IPS
- Color gamut:
  - 100% sRGB
  - 95% DCI-P3
- 75Hz
- Vesa Mount
