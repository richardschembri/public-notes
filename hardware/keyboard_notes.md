# ⌨️ Keyboard Notes

## [🍒 Switches](https://switchandclick.com/mechanical-keyboard-switch-guide/)

- [Switch Comparison](https://www.rtings.com/keyboard-switch/tools/compare/gateron-g-pro-red-vs-cherry-mx-red/42180/42099)

### Switch Types

- Low Profile
- Optical: Work via light, very responsive
- Silent: Rubber padding built into them
- EC:
- Topre: Technically rubber dome but much more tactile
- Analog

### Most common

- Cherry MX Red - Linear
- Cherry MX Brown - Tactile (Has bump)
- Cherry MX Blue - Clicky

https://40s.wiki/Guides/Introduction

## 40% Keyboard

### Planck

#### Planck EZ

## 🍒 Switch Options

- Cherry MX
- Kailh

---

## Typing

### Typing Tests

[monkeytype](https://monkeytype.com/)

## Stenography

### Keycaps

- **Steno Toppers:** https://cemrajc.github.io/stenotoppers/

---

Links
Keyboard Layouts
https://evantravers.com/articles/2019/04/20/community-post-40-keyboard-layouts/
