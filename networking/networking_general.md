## PPPOE: Point-to-Point Protocol over Ethernet
Allows a single server connection to be divided between multiple clients,
using Ethernet. As a result, multiple clients can connect to the same server
from the Internet Service Provider and get access to the internet, at the
same time, in parallel. To simplify, PPPoE is a modern version of the old
dial-up connections.

PPPoE is also capable of offering essential networking features, like authentication,
encryption, and data compression.

## ONU: Optical Network Unit
It is the fiber-optic equivalent of the usual modem used to decode signals into actual data. 
