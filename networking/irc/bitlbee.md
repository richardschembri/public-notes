# bitlbee Notes

## [Config](https://wiki.archlinux.org/title/Bitlbee#Configuration)
** Config File:** `/etc/bitlbee/bitlbee.conf`
Set **bitlbee** user
``` sh
User = bitlbee
```
For daemon mode uncomment the following lines:
``` sh
DaemonInterface = 0.0.0.0
DaemonPort = 6667
```
*To only allow connections from localhost*
``` sh
DaemonInterface = 127.0.0.1
```
Ensure that the configuration directory is writeable with the user you configured:
``` sh
# chown -R bitlbee:bitlbee /var/lib/bitlbee
```

## Usage
### Start Service
``` sh
systemctl start bitlbee
```
### Check if service is running
``` sh
systemctl -t service | grep bitlbee
```
## [Facebook plugin](https://wiki.bitlbee.org/HowtoFacebookMQTT)
> This is currently **not working** with the latest version of bitlbee (Test on 21/07/03)
### Installation
- **openSUSE:** bitlbee-facebook
### Config
**It is recommended that you use an app password**. It can be be generated via
the [facebook security page](https://www.facebook.com/settings?tab=security&section=per_app_passwords&view)
- App Passwords -> Generate app passwords

``` sh
account add facebook <email> <password>
account facebook on
```
