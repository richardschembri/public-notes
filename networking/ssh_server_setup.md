## OpenSUSE
### Firewall Setup
1. Open Yast Firewall
2. Add SSH service in the zones Internal, External and Public
3. Set TCP port in the zones Internal, External and Public

### openSSH
https://en.opensuse.org/SDB:Configure_openSSH

#### sshd
##### (Optional) change port
edit /etc/ssh/sshd_config as root and change:
```` sh
#Port 22
-to-
Port 1234
````

### Public key authentication
https://en.opensuse.org/SDB:OpenSSH_public_key_authentication

#### Key generation
```` sh
$ ssh-keygen
Enter file in which to save the key (/home/your_user/.ssh/id_rsa): <Enter>
Enter passphrase (empty for no passphrase): <Enter>
Enter same passphrase again: <Enter>
Your identification has been saved...\
````
#### Upload your key
```` sh
$ ssh-copy-id user@ssh.yourserver.org
Password:
Now try logging into the machine, with "ssh 'user@ssh.yourserver.org'", and check in:
~/.ssh/authorized_keys
to make sure we haven't added extra keys that you weren't expecting.
````


### Yubikey Support
https://developers.yubico.com/yubico-pam/YubiKey_and_SSH_via_PAM.html

