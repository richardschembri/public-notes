AWS Notes
=========

[Domain Name - route53](https://aws.amazon.com/route53/)
--------------------------------------------------------
### [How to create a subdomain](https://aws.amazon.com/premiumsupport/knowledge-center/create-subdomain-route-53/)
#### Create a hosted zone for the subdomain in Route 53
Create a hosted zone with the same name as the subdomain that you want to route traffic for, such as acme.example.com. To do this:

1. Open the Route 53 console.
2. In the navigation pane, choose Hosted zones.
3. Choose Create hosted zone.
4. In the right pane, enter the name of the subdomain (such as some.example.com).
5. Note: For more information, see DNS domain name format.
6. For Type, accept the default value of Public hosted zone.
7. Choose Create hosted zone.

### [Pricing](https://aws.amazon.com/route53/pricing/)

---------------------

CLI
----

### [Configure](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)
```sh
aws configure

AWS Access Key ID [None]: AKIAIOSFODNN7EXAMPLE

AWS Secret Access Key [None]: wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY

Default region name [None]: us-west-2

Default output format [None]: json
```

### S3

#### [`sync`](https://docs.aws.amazon.com/cli/latest/reference/s3/sync.html)
Sync to local
```sh
aws s3 sync s3://mybucket .
```
=======
--------------------------------------------------------------------------------

Amazon Linux Instance
---------------------
### [Update](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/install-updates.html)
