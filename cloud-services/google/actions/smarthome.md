# Google Smart Home
## Reference
--------------------------------------------------------------------------------
### [OAuth](https://developers.google.com/assistant/smarthome/develop/implement-oauth)
#### Authorization endpoint parameters

| client_id			| The Google client ID you registered with Google.																			|
| redirect_uri		| The URL to which you send the response to this request.																	|
| state				| A bookkeeping value that is passed back to Google unchanged in the redirect URI.											|
| scope				| **Optional:** A space-delimited set of scope strings that specify the data Google is requesting authorization for.		|
| response_type		| The type of value to return in the response. For the OAuth 2.0 authorization code flow, the response type is always code.	|
| user_locale		| The Google Account language setting in RFC5646 format, used to localize your content in the user's preferred language.	|

##### Request Example
``` sh
GET https://myservice.example.com/auth?client_id=GOOGLE_CLIENT_ID&redirect_uri=REDIRECT_URI&state=STATE_STRING&scope=REQUESTED_SCOPES&response_type=code&user_locale=LOCALE
```

#### API Returns Correct Error Status Codes
If the API’s OAuth processing fails, the API should return one of these error statuses
in the response:

- If the token is missing, invalid or expired, return a **401** response
- If there was a technical problem in the API, return a **500** response

#### Issues
##### Refresh Token Issue
https://stackoverflow.com/questions/67869164/cant-figure-out-why-google-oauth-refresh-token-are-expired
https://stackoverflow.com/questions/57383523/how-to-detect-when-an-oauth2-refresh-token-expired
--------------------------------------------------------------------------------
### [Home Graph](https://developers.google.com/assistant/smarthome/concepts/homegraph)
> Use this so that Google avoids querying all 
- Smart home Actions rely on Home Graph.
- A database that stores and provides contextual data about the home and its devices.
- Stores information about:
	- Structures (Ex: Home or office)
	- Rooms (Ex: Bedroom or Living Room)
	- Devices (Ex: Speaker or Lightbulb)
- **Implicit Commands:** Controls devices that are in the same room.
- The smart home intent is determined for a particular room that is identified in Home Graph.

#### [Setup](https://developers.google.com/assistant/smarthome/develop/report-state)
- Access [HomeGraph API][1] via [Google Cloud Platform](https://console.cloud.google.com/).
- Create a new project
- Search for "HomeGraph API"
- Click "ENABLE"
- After the enabling the API, access **Credentials**
- Access *Create Credentials* -> *Service Account*
- Create Service Account:
	- Fill in "Service account details"
	- Set role to: *Service Accounts* -> *Service Account Token Creator*
	- Click "Finish"
	- Access project and access *KEYS*
	- Click "ADD KEY" and download as **JSON**

--------------------------------------------------------------------------------
### [Device Types](https://developers.google.com/assistant/smarthome/traits)
Device types harness the power of the Google Assistant's natural language processing.
For example, a device with a type `action.devices.types.LIGHT` can be turned on in different ways:
- Turn on the light.
- Turn my light on.
- Turn on my living room light
--------------------------------------------------------------------------------
### [Device Traits](https://developers.google.com/assistant/smarthome/traits)
The functionality for **Device Types** comes from the traits that you add to each one.
#### Examples
- `action.devices.traits.OnOff`
- `action.devices.traits.LightEffects`

--------------------------------------------------------------------------------
### [Intents](https://developers.google.com/assistant/smarthome/develop/process-intents)
#### [SYNC](https://developers.google.com/assistant/smarthome/reference/intent/sync)
Requests the list of devices associated with the given user and their capabilities.
`SYNC` intent request
``` js
{
	"requestId": "ff36a3cc-ec34-11e6-b1a0-64510650abcf",
	"inputs": [
		{
			"intent": "action.devices.SYNC"
		}
	]
}
```
#### [QUERY](https://developers.google.com/assistant/smarthome/develop/process-intents#handle_query_intents)
**Queries** your fulfillment for the **current states** of devices, including whether
the device is **online** and **reachable**.
Return the new state in the `EXECUTE` response.
##### Request
> Request from Google
``` js
{
	"requestId": "ff36a3cc-ec34-11e6-b1a0-64510650abcf", =
	"inputs": [
		{
			"intent": "action.devices.QUERY",
			"payload": {
				"devices": [
					{
						"id": "123",
						"customData": {
							"fooValue": 74,
							"barValue": true,
							"bazValue": "foo"
						}
					},
					{
						"id": "456",
						"customData": {
							"fooValue": 12,
							"barValue": false,
							"bazValue": "bar"
						}
					}
				] // devices
			} // payload
		}
	] // inputs
}
```
##### Response
> Response to Google
``` js
{
	"requestId": "ff36a3cc-ec34-11e6-b1a0-64510650abcf",
	"payload": {
		"devices": {
			"123": {
				"on": true,
				"online": true
			},
			"456": {
				"on": true,
				"online": true,
				"brightness": 80,
				"color": {
					"name": "cerulean",
					"spectrumRGB": 31655
				}
			}
		}
	}
}
```
#### [EXECUTE](https://developers.google.com/assistant/smarthome/develop/process-intents#handle_execute_intents)
##### Request
> Request from Google
``` js
{
	"requestId": "ff36a3cc-ec34-11e6-b1a0-64510650abcf",
	"inputs": [
		{
			"intent": "action.devices.EXECUTE",
			"payload": {
				"commands": [
					{
						"devices": [
							{
								"id": "123",
								"customData": {
									"fooValue": 74,
									"barValue": true,
									"bazValue": "sheepdip"
								} // customData
							},
							{
								"id": "456",
								"customData": {
									"fooValue": 36,
									"barValue": false,
									"bazValue": "moarsheep"
								} // customData
							}
						], // devices
						"execution": [
							{
								"command": "action.devices.commands.OnOff",
								"params": {
									"on": true
								}
							}
						] // execution
					}
				] // commands
			} // payload
		}
	] // inputs
}
```
##### Response
> Response to Google
``` js
{
	"requestId": "ff36a3cc-ec34-11e6-b1a0-64510650abcf",
	"payload": {
		"commands": [
			{
				"ids": [
					"123"
				], // ids
				"status": "SUCCESS",
				"states": {
					"on": true,
					"online": true
				} // states
			},
			{
				"ids": [
					"456"
				], // ids
				"status": "ERROR",
				"errorCode": "deviceTurnedOff"
			}
		] // commands
	} // payload
```

Sends commands to execute on smart home devices.
#### [DISCONNECT](https://developers.google.com/assistant/smarthome/reference/intent/disconnect)
Triggered to inform you when a user has unlinked their device account from Google
Assistant.
``` js
{
	"requestId": "ff36a3cc-ec34-11e6-b1a0-64510650abcf",
	"inputs": [
		{
			"intent": "action.devices.DISCONNECT"
		}
	] // inputs
}
```
--------------------------------------------------------------------------------
## Nodes

### Function Node
#### [Debug](https://discourse.nodered.org/t/how-to-debug-a-function-node/38024)
##### Log in inspector
`node.warn('My log')` or `node.error('My error log')`

--------------------------------------------------------------------------------
## [Build](https://developers.google.com/assistant/smarthome/develop/create)
### Create a smart home ACTION
1. Set up an **OAuth 2.0** server to link google account with device account.
2. [Create an Actions on Google developer project.](https://console.actions.google.com/)
3. Add authentication to your project(non google)
#### Create an **Action Project**
1. Enter project name
2. Select **Smart Home**
3. Click **Start building**
4. Name your Smart Home action (you can connect to from the Google Home app when deployed)
5. Click `Actions` in the left menu.
6. Enter a URL for your backend server that will provide **Fulfillment**.
7. Click Test. In the Simulator page, click **Start Testing** to finish your project setup
### [Intent Fullfillment](https://developers.google.com/assistant/smarthome/develop/process-intents)
#### 1. Identify the user
Assistant makes requests to your Fulfillment URL with the `ACCESS TOKEN` provided
by your OAuth 2.0 server in the Authorization header
``` sh
POST /fulfillment HTTP/1.1
Host: smarthome.example.com
Content-Type: application/json
Authorization: Bearer ACCESS_TOKEN
```
#### 2. List devices and their capabilities
- Assistant sends an `action.devices.SYNC intent` to your fulfillment URL
	``` js
	{
		"requestId": "ff36a3cc-ec34-11e6-b1a0-64510650abcf",
		"inputs": [{
			  "intent": "action.devices.SYNC"
		}]
	}
	```
- Return unique ID for each user in the `agentUserId` and their `devices`
``` js


{
	"requestId": "ff36a3cc-ec34-11e6-b1a0-64510650abcf",
	"payload": {
		"agentUserId": "1836.15267389",
		"devices": [
			{
				"id": "123",
				"type": "action.devices.types.OUTLET",
				"traits": [
					"action.devices.traits.OnOff"
				],
				"name": {
					defaultNames": [
						"My Outlet 1234"
					],
					"name": "Night light",
					"nicknames": [
						"wall plug"
					]
				},
				"willReportState": false,
				"roomHint": "kitchen",
				"deviceInfo": {
					"manufacturer": "lights-out-inc",
					"model": "hs1234",
					"hwVersion": "3.2",
					"swVersion": "11.4"
				},
				"otherDeviceIds": [
					{
						"deviceId": "local-device-id"
					}
				],
				"customData": {
					"fooValue": 74,
					"barValue": true,
					"bazValue": "foo"
				}
			}, // My Outlet 1234
			{
				"id": "456",
				"type": "action.devices.types.LIGHT",
				"traits": [
					"action.devices.traits.OnOff",
					"action.devices.traits.Brightness",
					"action.devices.traits.ColorSetting"
				],
				"name": {
					"defaultNames": [
						"lights out inc. bulb A19 color hyperglow"
					],
					"name": "lamp1",
					"nicknames": [
						"reading lamp"
					]
				},
				"willReportState": false,
				"roomHint": "office",
				"attributes": {
					"colorModel": "rgb",
					"colorTemperatureRange": {
						"temperatureMinK": 2000,
						"temperatureMaxK": 9000
					},
					"commandOnlyColorSetting": false
				},
				"deviceInfo": {
					"manufacturer": "lights out inc.",
					"model": "hg11",
					"hwVersion": "1.2",
					"swVersion": "5.4"
				},
				"customData": {
					"fooValue": 12,
					"barValue": false,
					"bazValue": "bar"
				}
			}
		] // devices
	} // payload
}

```
#### 3. Respond to queries and commands
##### QUERY
Your `QUERY` response includes a full set of states for each of the traits supported
by the requested devices.
##### EXECUTE
Similar to QUERY, a single intent can target multiple device IDs. A single `EXECUTE`
intent may also contain multiple distinct commands given to a group of devices.

#### 4. Handle unlinking events
If the user unlinks your smart home Action from Assistant, your fulfillment receives
an `action.devices.DISCONNECT` intent.
--------------------------------------------------------------------------------

<!---
Links
-->
[1]: https://developers.google.com/assistant/smarthome/concepts/homegraph
