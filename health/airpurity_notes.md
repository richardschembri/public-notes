# Air Purity Notes

## Measurements

| Symbol | Name                           | Notes           |
| ------ | ------------------------------ | --------------- |
| nm     | nanometer                      | 1 µm = 1,000 nm |
| µm     | micrometer                     |                 |
| PM     | atmospheric particulate matter | PM2.5 = 2.5 μm  |

Particulates

## Air Impurities

### Exhaust Fumes

#### Carbon monoxide

**Particle Size:** PM6 ~ PM30

##### Gas characteristics

- colorless
- tasteless
- odorless

##### Effects

- carbon monixide poisoning:
  - lightheadedness
  - confusion
  - dizziness
  - headache

#### Hydrocarbons(Benzene)

- Can cause leukemia

https://note.com/bb45_colorado/n/n1180f5c46a54

#### Sulfur Dioxide

Not PM since it is a gas

##### Gas characteristics

- colorless
- sharp + pungent smell

##### Effects

- asthma
- asthma like conditions

#### Soot

**Particle Size:** PM2.5 ~ PM0.1

##### Effects

- influenza
- asthma
- cancer

---

## Air Purifier Types

### Current

#### Sharp - Ion Plasmacluster KI-JX75

- Detects particles as small as 0.5μm & shows PM2.5 concentration
- Dust collection of over 99.97% of particles as small as 0.3 µm
  Electrostatic HEPA filter
- https://www.yodobashi.com/product/100000001004133865/
- https://kakaku.com/item/K0001096934/spec/#tab

### Options

[Corsi–Rosenthal Box](https://en.wikipedia.org/wiki/Corsi%E2%80%93Rosenthal_Box)

[Clean Air Kits](https://www.cleanairkits.com/)

[Coway AP-2015F AIRMEGA 400](https://www.yodobashi.com/product/100000001004056835/)
