Stock Trading Notes
===================
Stock 
-----
- A security
- A claim on a company's builds, equipment and their profit
- Sold to increase capital to invest in company to help it grow

### How to buy?

#### Private Company
- Small amount of owners
- Doesn't trade on a public exchange
- Can't go and simply buy
- You need to know the owner
- Need to be a **qualified investor**
    - Need to make a lot every year (ex $200k ~ $300)
    - Need a lot assets under you nam (ex. two million dollars worth)

#### Public Company
- Can buy through a broker


Private -> Public Company
-------------------------
- Get funded
- Do an **I**nitial **P**ublic **O**ffering
    - The first time that a stock of a private company is offered to the public.
    - It is very hard for regular investors to get shares at the IPO.
    - Hire an *underwriting company* to do an **IPO** (experts at pricing the stocks)
    - Brokers who get initial shares sell to their biggest clients

Market
------
- Location where to go buy and sell products (markets).
- Some are specialized (only deal with one type of product).

### Auction Market
- Sellers compete on how much to sell a product
- Buyers bid on products

### Stock Market
#### Primary Market
- The market that stocks trade in for the first time(brand new stocks)
- Usually only very big clients get access to these stocks 

#### Secondary Market
- Every other time the stocks are traded(second hand stocks)
- Basically open to everyone

Stock Exchange
--------------
- Where to buy and sell stocks ex(Some stocks are only available at certain exchanges):
    - New York Stock Exchange (Very demanding/strict)
    - NASDAQ (Less demanding/strict)
        - Mostly tech companies
    - Amex - Caters to **E**xchange **T**raded **F**unds
- Use Broker to buy/sell for you

Orders and Prices
----------------------
### Orders and Order Types
Provide the following info when placing an order:
#### Ticker
- You give to broker instead of company name.(Ex: Microsoft = MSFT)
#### Side
- Is it a **Buy** or a **Sell**
#### Type
##### Market
- Buy or Sell stock immediately
- Do when it is time sensitive but not price sensitive(you don't mind the price even if it might be bad)
##### Limit
- Buy or Sell stock at a limit price or better( buy cheaper or sell higher)
#### Price
#### Quantity

### Orders driving prices
>  Udemy lesson has notes
- Orders drive prices
- **Shares Outstanding**: Amount of shares a company has
- **Float**: Amount of shares that are currently trading on the market
- **Shares Outstanding** != **Float**
#### Priority
##### Price priority
- **Buyer**: Higher Price = Higher Priority
- **Seller**: Lower Price = Higher Priority
##### Time priority
- After **Price Priority** time is given priority (FIFO)

#### Level 2
The book holding all buyer and seller offers
#### Level 1
- **Best price**: 
    - **Bid**: Best(Highest) price that someone is willing to **buy** a stock at
    - **Ask**: Best(Lowest) price that someone is willing to **sell** a stock at
    - **Spread**: Difference between the **Bid** and the **Ask**
        - Keep in mind when making **market orders** 
- **Last price**: thestock traded at 
#### Market order
Sell at price buyer wants
#### Time and sales (ToS)
- Exchange has to report on at every excution that happens
- Report has:
    - Name of stock
    - Time of trade
    - Quantity
    - Price

### Different Players
- A single person trades different than a mutual fund
- Trading is a **zero sum game**
- What is the insitutional ownership of a stock (ex. facebook)
- Stocks trade different for stocks that have high institutions than stocks that have low institutions.

- Predict what other players are doing
#### Types of players/traders
- **Hedge Funds**: Smart money by rich people / Top Players
- **Retail traders**: trade from home(noobs of the trading world)
    - tend to **buy** on **good news**
- **Portfolo Manager/Mutual funds**: overseas a big fund follows what clients had agreed on. He takes comission.
    - tend to **sell** on **good news**
- **Investors**: keep stocks for a while

### 3 ways of making money
1. Going `long` a stock
    - Buy stock expecting it to go higher
2. Going `short` a stock
    - Betting a stock is going to lose money
    - Borrow stock from broker, sell high buy it back at low price
        - Broker borrows from his other clients 
3. Being flat
    - No position(no trade). Allow other investments to make money if you are unprepared.

Fundimental Analysis
------------------
> Good for **long term** investments
See the company's:
- How many employees do they have
- What is their tax system
- Politial environment
- Revenue
- Expenses
- Depts
- Etc

Technical Analysis
------------------
> Good for **short term** investments
- Different from **Fundimental Analysis**, you only care about:
    - Price of shares
    - Volume of shares
- Day traders use this

### Candlestick Charts
(tradingview.com)
- Like line chart but with more info
- Draw for each day
- Shows for each unit time (day, week, hour, etc):
	- Opening price
    - Close price(Last trade)
    - High price(Highest price)
    - Low price(Lowest price)
- Colors:
    - **Green**: Price went up(from *opening price*)
    - **Red**: Price went down(from *opening price*)

> Don't need to remember the following:
#### Big Candles
- Just big candles (big price change)
#### Dojis
- One of the most useful
- Opens and closes at **same price**
- Nobody won
- Indecision
##### Morning Doji Star
- Candles going *down green* -> Doji -> Candles going *up red*
##### Evening Doji Star
- Candles going *up green* -> Doji -> Candles going *down red*
#### Gravestone and Dragonfly
- `Gravestone` Opens and closes at **same price** which is also was the **lowest**
- `Dragonfly` Opens and closes at **same price** which is also was the **highest**

#### Shooting star and Hammer
- **Shooting star**: Small green candle with a *large highest price*
- **Hammer**: Small green candle with a *large lowest price*
#### Bearish harami and Bullish harami
- Most likely a reversal will happen
- **Bearish harami**: *Small red* candle opens within a *big green* candle 
        - Price will probably start to go **down**
    - **Bearish**: A view that a market is going **down** 
        - **Bear**s attack from up to down
    - **Engulfing Bearish**: *Big green* candle engulfs a *small red* candle
        - Price will probably start to go **up**
- **Bullish harami**: *Small green* candle opens within a *big red* candle
        - Price will probably start to go **up**
    - **Bullish**: A view that a market is going **up** 
        - **Bull**s attack from down to **up**
    - **Engulfing Bullish**: *Big red* candle engulfs a *small green* candle 
        - Price will probably start to go **down**

### Trends
- Uptrends / Downtrend / Sidetrend
- Trends tend to persist
- Trendline: draw on chart(line for lows, line for highs)
- Do not go against the whole trend
    - Go against a short or medium trend
- Best to view during short time frames(ex within a day as there is probably not much change

#### Support and Resistance
- `Support`: Supports the price from going down 
    - You should **buy** when there is **support** 
    - You should **buy** above **support** price 
- `Resistance`: Resists the price from going up
    - You should **sell** when there is **resistance** 
    - You should **sell** below **resistance** price 
- Mostly seen around **round numbers**
    - A lot of people sell at **round numbers** 
- **Role Reversal**

### Chart Patterns
> Check finviz website
- Behavioural Patterns

#### Head and Shoulders
- Graph Mid(left *shoulder*) - Graph low(at `support`) - Graph High (*head*) - Graph low(at `support`) - Graph Mid(right *shoulder*)
- `support` is the neck line

#### Double Top
- A chart that topped at 2 different location
- Shows the `resistance` of a stock
- Way more likely to fall down than break the `resistance`
- Too many sellers / high supply
- Good for shorts
- Better to look short term(ex: within a day) rather through long time spans(ex: months)
- Self fulfilling prophecy

#### Multiple Tops
- Like `Double Top` but with more tops
- Further confirms a `resistance` point
- Good for shorts

#### Double Bottom
- Like `Double Top` but with bottoms
- Way more likely go up than break the `support`

#### Multiple Bottoms
- Like `Double Bottom` but with more bottoms
- Further confirms a `support` point

#### Triangles
#####  Ascending/Descending Triangles
- Flat `support` or `resistance` along with angled `resistance` or `support`
- Price is being wedged
- Probably breaks in favor of the angle

##### Wedge Triangles
> Wedge Down / Wedge Up 
- Angled `support` or `resistance` along with angled `resistance` or `support`
- Once broken it probably becomes new `support` or `resistance` 
- The further the wedge the stronger the explosion

#### Cup and Handles
- Slow down slow up, reconsiliates then breaks

#### Rounding Bottom
- Slow down slow up

### Volume
- **Quantity** of shares that were traded
- `Volume` confirms `price`
- `Volume` precedes `price`
    - A lot of `volume` comes in before `price` moves 

Risk Management & Money Management
----------------------------------
### Expectancy
- Anticipated value for a given investment
- Statistics and probability analysis
- E[X] = Exi pi

