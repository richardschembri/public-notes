Nutrition(栄養)
===============

| kanji      | romaji           | en                                         |
| :--------- | :------------    | :-----------------                         |
| 栄養       | Eiyō             | Nutrition                                  |
| たんぱく質 | Tanpakushitsu    | Protein                                    |
| 熱量       | Netsuryō         | Calories                                   |
| 脂質       | Shishitsu        | Fat/Lipids                                 |
| 炭水化物   | Tansuikabutsu    | Carbohydrate                               |
| 糖質       | Tōshitsu         | Carbohydrate/Sugar                         |
| 食物繊維   | Shokumotsu sen'i | Fiber                                      |
| 食塩相当量 | Shokuen sōtō-ryō | Equivalent amount of salt, Salt equivalent |
