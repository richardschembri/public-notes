# 🪖 Protective Clothing Notes

## 🧤 Protective Gloves

### Protection / Resistance

**Rating:** 0-4 (4 is highest)

https://www.bladerunner.tv/en388-2003-vs-en388-2016/

- Extreme Heat or Cold: Insulation / Ventilation
- 🪨 Abrasion Resistane
- 🥲 Tear Resistane
- 🪡 Puncture Resistane
- 🔨 Impact Protection
  - **P**: Pass
- 🔪 Blade Cut Resistane
  - **OLD:** 🌀 COUP Cut Test
    - Rotating circular blade moves back and forth at a constant speed
    - 5 Newtons while cutting through the fabric
    - Rating from 1 to 5 is calculated based on the total distance traveled by the blade before cutting through
    - ❌ Blade loses sharpness over time
    - ❌ Test results may be less accurate for gloves with higher cut resistance.
  - **NEW:** 🪒 TDM Cut Test - [ISO13997 TDM](https://www.hexarmor.com/posts/changes-to-cut-protection-standards-for-hand-ppe)
    - TDM stands for tomodynamometer, the equipment used for this test.
    - The test uses a straight blade drawn across the sample in a single movement.
    - A new blade is used for each test to ensure accuracy.
    - The "stroke length" before the blade cuts through the sample is recorded for various forces.
    - Graphs are plotted to predict the force needed to cut through the glove in 20mm of travel.
    - The required force is used to calculate a score from A to F, with **F being the highest rating**.

| Specifications | Abrasion Resistane (0-4) | Cut (COUP Test) (0-5) | Cut (TDM-100 Test) (A-F) | Tear Resistane (0-4) | Puncture Resistance (0-4) | Impact Protection (P=Pass) |
| -------------- | :----------------------: | :-------------------: | :----------------------: | :------------------: | :-----------------------: | :------------------------: |
| EN388:2003     |            4             |           5           |            -             |          4           |             4             |             -              |
| EN388:2016     |            3             |           x           |            E             |          4           |             3             |             P              |

#### Specifications

##### EN388: 2003

### 🌊 Weather Resistance

- Water-Resistant or Waterproof

### 🪵 Materials / 材質(ざいしつ)

Excellent protection against abrasions, cuts, and ⁢punctures

- Leather / 革(かわ)
- Nylon / ナイロン
- Kevlar / ケブラー

### Fit and Comfort

- Adjustable wrist closures or straps

#### 📏 Dimensions / 寸法(すんぽう)

- Full Length / 全長(ぜんちょう)
- Middle Finger / 中指(なかゆび)
- Around the palm / 手の平まわり(てのひらまわり)

### Grip and Dexterity

- Textured Palms or Fingers
  - Grip even in wet or slippery conditions
- Pre-Curved and flexible designs

https://offthebeatengrid.net/outdoor-survival-gear/how-to-choose-the-right-survival-gloves/

https://www.yodobashi.com/product/100000001005907664/
https://www.yodobashi.com/product/100000001005907648/
https://www.yodobashi.com/product/100000001005907662/

Show Gloves
https://www.yodobashi.com/product/100000001006276081/
