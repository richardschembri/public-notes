# ☀️👕 Summer Clothing

## 🎩 High Style

- Khaki trousers
- Button down shirt / Sleeved up
- Short sleved henley or Polo

## 🧥 Mid Style

- Stripes/Solid Color T-Shirt

## 🎽 Lower Style

- Graphic T-Shirt
