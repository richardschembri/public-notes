KeePass Notes
=============

[2FA](https://keepassxc.org/docs/KeePassXC_UserGuide#_adding_totp_to_an_entry)
-----
1. Add Entry
2. Right click on entry -> TOTP -> Set up TOTP
3. Paste the secret code from the website
