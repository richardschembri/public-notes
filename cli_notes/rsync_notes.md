# rsync
``` sh
rsync Original/* Backup/
```

## Command arguements
- **Dry Run** : `--dry-run`
- **Verbose** : `-v`
- **Delete files in target directory that are not in source** : `--delete`
