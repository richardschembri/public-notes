OpenSUSE
========

Zypper
------
### List installed packages
```sh
zypper search --installed-only texlive
```

#### Package Names only
```sh
zypper search --installed-only texlive | awk '{print $3}'
```
