# Navigation
<kbd>'</kbd><kbd>'</kbd> '' Switch between two last locations

-----------------------------------------------------------------------

# Misc
**Show hidden dot files :** <kbd>z</kbd><kbd>o</kbd>
**Hide dot files :** <kbd>z</kbd><kbd>m</kbd>
**Enter shell mode :** <kbd>s</kbd>

-----------------------------------------------------------------------

# Panes
**Exchange panes :** <kbd>CTRL</kbd>+<kbd>W</kbd> <kbd>x</kbd>
**Navigate between panes :** <kbd>CTRL</kbd>-<kbd>h/j/k/l</kbd> 
**Move panes :** <kbd>CTRL</kbd>+<kbd>W</kbd> <kbd>H/J/K/L</kbd>

## Change right pane view
> For example to view file contents
``` sh
:view
```

-----------------------------------------------------------------------

# Sort
``` sh
set sort=-size,dir
```
## Sorting Keys
   [+-]ext     - extension of files and directories
   [+-]fileext - extension of files only
   [+-]name    - name (including extension)
   [+-]iname   - name (including extension, ignores case)
   [+-]type    - file type (dir/reg/exe/link/char/block/sock/fifo)
   [+-]dir     - directory grouping (directory < file)
   [+-]gid     - group id (**nix only**)
   [+-]gname   - group name ((**nix only**)
   [+-]mode    - file mode (file type + permissions) in octal (**nix only**)
   [+-]perms   - permissions string (**nix only**)
   [+-]uid     - owner id (**nix only**)
   [+-]uname   - owner name (**nix only**)
   [+-]nlinks  - number of hard links (**nix only**)
   [+-]inode   - inode number (**nix only**)
   [+-]size    - size
   [+-]nitems  - number of items in a directory (zero for files)
   [+-]groups  - groups extracted via regexps from vifm-'sortgroups'
   [+-]target  - symbolic link target (empty for other file types)
   [+-]atime   - time accessed (e.g. read, executed)
   [+-]ctime   - time changed (changes in metadata, e.g. mode)
   [+-]mtime   - time modified (when file contents is changed)

-----------------------------------------------------------------------
