xrandr notes
============

### Position screen
```sh
xrandr --output "HDMI-A-0" --auto --left-of "eDP";; 
```

### Brightness
```sh
xrandr --output "HDMI-A-0" --brightness 0.75;; 
```
