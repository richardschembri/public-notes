# Curl Notes
## [Http Post](https://linuxize.com/post/curl-post-request/)

### With form params
```sh
curl -X POST -F 'name=linuxize' -F 'email=linuxize@example.com' https://example.com/contact.php
```

### Json
```sh
curl -X POST -H "Content-Type: application/json" \
-d '{"name": "linuxize", "email": "linuxize@example.com"}' \
https://example/contact
```

curl -X POST -H "Content-Type: application/json" -d '{"name": "linuxize", "email": "linuxize@example.com"}' https://example/contact

## Download
```sh
curl https://www.bbc.com  > bbc.html
```
