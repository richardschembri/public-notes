Git Notes
=========
[Creating Local And Remote Hosting Server](https://medium.com/swlh/creating-local-and-remote-hosting-server-for-git-5ff47c95cfa6)
------------------------------------------------------------------------------
### Local
- `git init --bare` Initialise an empty Git repository
- Repositories initialised with the `--bare` flag end in `.git`
#### Example
``` sh
git init --bare server.git
```
#### *Cloning* bare repository from *local path*
``` sh
git clone /Users/myuser/projects/local-git-server/server.git
```

#### *Cloning* bare repository from *remote path* via *ssh*
``` sh
ssh://[user@]host.xz[:port]/~[user]/path/to/repo
```

For Example:
``` sh
ssh://jack@jill.com:4242/~/projects/jillweb
```

When logging as different user (presuming you have file permissions):

``` sh
ssh://jill@jill.com:4242/~jack/projects/jillweb
```

Pull
----
### Automatic merge failed
```sh
# (edit <file>)
git add <file>
```

Branch
------
### List branches
#### List all branches in local and remote repositories is
``` sh
git branch -a
```

#### Listing only the remote branches
``` sh
git branch -r
```

#### See the branches and their commits
``` sh
git show-branch
```

### Clone
``` sh
git clone -b <branch> <remote_repo>
```
> Please note this will also get the other branches

#### Single Branch - Get only one branch
``` sh
git clone -b <branch> --single-branch <remote_repo>
```

#### Shallow Clone
``` sh
git clone -b <branch> <remote_repo> --depth 1
```

### Switch branch
``` sh
git branch <branch> 
```
### Branch folder
``` sh
git checkout -b aaa/bbb
```

Config
------
### Get
#### Remote URL
``` sh
git config --get remote.origin.url
```

Remove
------
### Cached files
``` sh
git rm --cached file1 file2 dir/file3
```
### [gitignored files](https://stackoverflow.com/questions/13541615/how-to-remove-files-that-are-listed-in-the-gitignore-but-still-on-the-repositor)
``` sh
git ls-files -i -c --exclude-from=.gitignore | xargs git rm --cached 
```

### Untracked files
``` sh
git clean -f
```
#### and directories / folders
``` sh
git clean -fd
```

`ls-files` // List files
----------------------
### Untracked
``` sh
git ls-files --others --exclude-standard
```
#### Pipe to xargs
``` sh
git ls-files -z -o --exclude-standard | xargs -0 git add
```
 
Submodule
---------
### Add
> Add a git inside a git
``` sh
git submodule add https://github.com/chaconinc/DbConnector
```

#### Add branch
https://stackoverflow.com/questions/1777854/how-can-i-specify-a-branch-tag-when-adding-a-git-submodule

### Update submodule
#### First time
``` sh
git submodule update --init --recursive
```
#### After first time
``` sh
git submodule update --recursive --remote
```

### Remove
``` sh
git submodule deinit mysubmodule
```

------------------------------------------------------------------------

Git Guides
==========
Commit
------
[Guide](https://github.com/RomuloOliveira/commit-messages-guide)

------------------------------------------------------------------------

Github
======

* [README Template](https://gist.github.com/PurpleBooth/109311bb0361f32d87a2)
* [Badges](https://github.com/badges/shields/blob/master/README.md)
* [Versioning](https://semver.org/)

Update forked repository
------------------------
1. Add the remote, call it "upstream":
   ``` sh
   git remote add upstream https://github.com/whoever/whatever.git
   ```

2. Fetch all the branches of that remote into remote-tracking branches,
   such as upstream/master:
   ``` sh
   git fetch upstream
   ```

3. Make sure that you're on your master branch:
   ``` sh
   git checkout master
   ```

4. Rewrite your master branch so that any commits of yours that
   aren't already in upstream/master are replayed on top of that
   other branch:
   ``` sh
   git rebase upstream/master
   ```

Personal Access Tokens
----------------------

<Profile Image>->Settings -> Developer settings -> Personal access tokens

### SourceTree
#### Update token
1. Close SourceTree
2. Delete Users/<username>/AppData/Local/Atlassian/SourceTree/passwd
3. Restart SourceTree

------------------------------------------------------------------------

