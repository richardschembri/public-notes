tmux
====

General Shortcuts
-----------------
| Function                      | Shortcut                  |
| :---------------------------- | :------------------------ |
| **Split vertical**            | `<Ctrl>` + `%`            |
| **Split horizontal**          | `<Ctrl>` +  `"`           |
| **Scroll mode**               | `<Ctrl>` +  `[`           |
| **Close/Kill current window** | `<Ctrl>` + `b` `b`        |
| **Close/Kill current pane**   | `<Ctrl>` + `b` `x`        |
| **Zoom**                      | `<Ctrl>` + `b` `z`        |
| **Resize Pane**               | `<Ctrl>` + `<arrow key>`  |

Resize Panes
------------
| Command                       | Description                         |
| :---------------------------- | :------------------------           |
| `:resize-pane -D`             | Resize **Downward**                 |
| `:resize-pane -U`             | Resize **Upward**                   |
| `:resize-pane -L`             | Resize **Left**                     |
| `:resize-pane -R`             | Resize **Right**                    |
| `:resize-pane -D 10`          | Resize **Downward** by **10 cells** |
| `:resize-pane -U 10`          | Resize **Upward** by **10 cells**   |
| `:resize-pane -L 10`          | Resize **Left** by **10 cells**     |
| `:resize-pane -R 10`          | Resize **Right** by **10 cells**    |


Sessions
--------
### Commands
| Command                       | Description                         |
| :---------------------------- | :------------------------           |
| `:kill-session`             | Close/Kill current session                 |

### Shortcuts
| Function                      | Shortcut                  |
| :---------------------------- | :------------------------ |
| **Detach from session**       | `<Ctrl>` + `b` `d`        |
| **Show all sessions**         | `<Ctrl>` + `b` `s`        |

### Cli Commands
* ### Start a new session
``` sh
tmux new -s mysession
```
``` sh
:new -s mysession
```

--------------------------------------------------------------------------------

tmuxinator
==========
### List all the projects you have configured. Aliased to `l` and `ls`
``` sh
tmuxinator list
```

## Configuration
### Shell Command
```
tmuxinator new [project]
```
### Config Path
``` sh
 ~/.config/tmuxinator/
```
### Config File
> space indentation
```
name: session_name
root: ~/

windows:
  - window_name:
    - shell command
    - shell command2
  - window2_name:
      layout: main-vertical
      panes:
        - pane_name:
	  - shell command
	  - shell command2
	  - shell command3
```
_Example :_
```
name: sample
root: ~/

windows:
  - stats:
    - ssh stats@example.com
    - tail -f /var/log/stats.log
  - logs:
      layout: main-vertical
      panes:
        - logs:
          - ssh logs@example.com
          - cd /var/logs
          - tail -f development.log
```

# References
* (cheatsheet)[https://tmuxcheatsheet.com]
* (kill-session)[https://superuser.com/questions/777269/how-to-close-a-tmux-session]
