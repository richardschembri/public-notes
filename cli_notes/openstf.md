# Setup
## Mac OS
### Links
- **Open STF :** https://github.com/openstf/stf
- **Docker Compose:** https://github.com/nikosch86/stf-poc
- **How to use a USB device in a Docker container on Mac :** https://mil.ad/docker/2018/05/06/access-usb-devices-in-container-in-mac.html
- **Install Virtual Box :** http://biercoff.com/how-to-fix-docker-machine-installation-on-mac-os-x/

### VirtualBox
1. Start VirtualBox installation
2. Wait for it to Fail
3. Open Settings -> Security&Privacy
4. On the bottom there will be a record that System software from 'Oracle' developer was blocked from loading
5. Tap Allow button
6. Install VirtualBox again.

### Docker Machine
1. curl -L https://github.com/docker/machine/releases/download/v0.16.2/docker-machine-`uname -s`-`uname -m` >/usr/local/bin/docker-machine && \
  chmod +x /usr/local/bin/docker-machine
2. `docker-machine create --driver virtualbox default`
3. `eval "$(docker-machine env default)"`

# USB Setup


# Run
1. `cd /Users/tongullman/Git/stf-poc`
2. `docker-machine stop`
3. `docker-machine start`
4. `docker-machine env default`
5. `eval "$(docker-machine env default)"`
6. `docker-compose up -d`

# Notes
`docker-compose exec app sh`
