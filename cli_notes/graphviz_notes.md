# [Graphviz](https://www.graphviz.org/pdf/dotguide.pdf)
``` gv
digraph G {
	size ="4,4";
	main [shape=box]; /* this is a comment */
	main -> parse [weight=8];
	parse -> execute;
	main -> init [style=dotted];
	main -> cleanup;
	execute -> { make_string; printf}
	init -> make_string;
	edge [color=red]; // so is this
	main -> printf [style=bold,label="100 times"];
	make_string [label="make a\nstring"];
	node [shape=box,style=filled,color=".7 .3 1.0"];
	execute -> compare;
}
```

## Arrow Types
``` gv
digraph G {
	size ="4,4";
	main [shape=box]; /* this is a comment */
	main -> parse [weight=8];
	parse -> execute;
	main -> init [style=dotted];
	main -> cleanup;
	execute -> { make_string; printf}
	init -> make_string;
	edge [color=red]; // so is this
	main -> printf [style=bold,label="100 times"];
	make_string [label="make a\nstring"];
	node [shape=box,style=filled,color=".7 .3 1.0"];
	execute -> compare;
}
```

## Node Shapes
- box
- polygon
- elipse
- circle
- point
- egg
- triangle
- plaintext
- diamond
- trapezium
- parallelogram
- house
- hexagon
- octagon
- doublecircle
- doubleoctagon
- tripleoctagon
- invtriangle
- invtrapezium
- invhouse
- Mdiamond
- Msquare
- Mcircle
- none
- record
- Mrecord
