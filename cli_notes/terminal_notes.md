# Terminal Notes

<!--ts-->

- [Terminal Notes](#terminal-notes)
  - [<a href="https://www.linuxhowtos.org/System/procstat.htm" rel="nofollow">/proc/stat</a>](https://www.linuxhowtos.org/System/procstat.htm)
  * [Syntax](#syntax)
    - [loop through files](#loop-through-files)
  * [Launch Terminal](#launch-terminal)
    - [Make bash script executable](#make-bash-script-executable)
  * [Colors](#colors)
    - [<a href="https://stackoverflow.com/questions/54838578/color-codes-for-tput-setf" rel="nofollow">tput</a>](https://stackoverflow.com/questions/54838578/color-codes-for-tput-setf)
  * [<a href="https://www.freecodecamp.org/news/symlink-tutorial-in-linux-how-to-create-and-remove-a-symbolic-link/" rel="nofollow">Symlink</a>](https://www.freecodecamp.org/news/symlink-tutorial-in-linux-how-to-create-and-remove-a-symbolic-link/)
    - [Add Symlink](#add-symlink)
      - [File](#file)
      - [Folder](#folder)
    - [Check if Symlink](#check-if-symlink)
    - [Remove Symlink](#remove-symlink)
    - [Broken Symlink links](#broken-symlink-links)
      - [Find](#find)
      - [Delete](#delete)
  * [General commands](#general-commands)
    - [count words, lines, characters](#count-words-lines-characters)
    - [Login as other user](#login-as-other-user)
    - [diff](#diff)
      - [When using sed on <strong>urls</strong>, use , instead of / as a delimiter](#when-using-sed-on-urls-use--instead-of--as-a-delimiter)
    - [File Management](#file-management)
      - [Show the size of a single directory, in human readable units:](#show-the-size-of-a-single-directory-in-human-readable-units)
      - [List the human-readable sizes of a directory and any subdirectories, up to N levels deep:](#list-the-human-readable-sizes-of-a-directory-and-any-subdirectories-up-to-n-levels-deep)
    - [Check Input code xev](#check-input-code-xev)
      - [Start xev(1) and show only the relevant parts.](#start-xev1-and-show-only-the-relevant-parts)
    - [Set default file manager](#set-default-file-manager)
    - [<a href="https://www.gnu.org/software/coreutils/nohup" rel="nofollow">nohup</a>](https://www.gnu.org/software/coreutils/nohup)
      - [Run a process that can live beyond the terminal:](#run-a-process-that-can-live-beyond-the-terminal)
      - [Launch nohup in background mode:](#launch-nohup-in-background-mode)
      - [Run a shell script that can live beyond the terminal:](#run-a-shell-script-that-can-live-beyond-the-terminal)
      - [Run a process and write the output to a specific file:](#run-a-process-and-write-the-output-to-a-specific-file)
    - [find](#find-1)
      - [List of All Folders and Sub-folders](#list-of-all-folders-and-sub-folders)
    - [Convert text files](#convert-text-files)
  * [Compression / Archiving](#compression--archiving)
    - [unzip](#unzip)
    - [zip](#zip)
      - [Create an encrypted archive (user will be prompted for a password):](#create-an-encrypted-archive-user-will-be-prompted-for-a-password)
        - [Exclude subdirectory](#exclude-subdirectory)
    - [tar](#tar)
      - [To create a .gz archive and exclude all jpg,gif,... from the tgz](#to-create-a-gz-archive-and-exclude-all-jpggif-from-the-tgz)
      - [Extract a (compressed) archive into the current directory:](#extract-a-compressed-archive-into-the-current-directory)
    - [Combine commands](#combine-commands)
  * [<a href="https://www.computerhope.com/unix/scp.htm" rel="nofollow">scp Secure Copy</a>](https://www.computerhope.com/unix/scp.htm)
    - [Local Copy](#local-copy)
    - [Upload](#upload)
      - [Using a ssh private_key](#using-a-ssh-private_key)
    - [Download](#download)
      - [Using a ssh private_key](#using-a-ssh-private_key-1)
    - [Remote Copy](#remote-copy)
      - [Same Server](#same-server)
      - [Cross Server](#cross-server)
    - [Quickly create folders / Directories + Subdirectories](#quickly-create-folders--directories--subdirectories)
      - [Other](#other)
  * [Redo last command but as root](#redo-last-command-but-as-root)
  * [Open an editor to run a command](#open-an-editor-to-run-a-command)
  * [Create a super fast ram disk](#create-a-super-fast-ram-disk)
  * [List files/folders](#list-filesfolders)
    - [Don't add command to history (note the leading space)](#dont-add-command-to-history-note-the-leading-space)
    - [List hidden files/folders](#list-hidden-filesfolders)
  * [Fix a really long command that you messed up](#fix-a-really-long-command-that-you-messed-up)
  * [Tunnel with ssh (local port 3337 -&gt; remote host's 127.0.0.1 on port 6379)](#tunnel-with-ssh-local-port-3337---remote-hosts-127001-on-port-6379)
  * [Intercept stdout and log to file](#intercept-stdout-and-log-to-file)
  * [Bonus: exit terminal but leave all processes running](#bonus-exit-terminal-but-leave-all-processes-running)
    - [Change encoding of file](#change-encoding-of-file)
  * [Navigation](#navigation)
  * [rename](#rename)
    - [Bulk Rename](#bulk-rename)
- [<a href="https://askubuntu.com/questions/420981/how-do-i-save-terminal-output-to-a-file" rel="nofollow">Terminal output to a file</a>](https://askubuntu.com/questions/420981/how-do-i-save-terminal-output-to-a-file)
  - [Output to new file (overwrite)](#output-to-new-file-overwrite)
  - [Output append to file](#output-append-to-file)
  - [Output to new file <strong>with error</strong> stderr (overwrite)](#output-to-new-file-with-error-stderr-overwrite)
  - [Output append to file <strong>with error</strong> stderr](#output-append-to-file-with-error-stderr)
  - [Have both stderr and output displayed on the console and in a file](#have-both-stderr-and-output-displayed-on-the-console-and-in-a-file)
- [Systemd](#systemd)
  - [systemctl](#systemctl)
    - [Services](#services)
- [Tarballs](#tarballs)
  - [Patches](#patches)
    - [Make Patch](#make-patch)
    - [Apply Patch](#apply-patch)
  - [Font](#font)
  - [Convert UNICODE](#convert-unicode)
  - [<a href="https://www.makeuseof.com/how-to-set-date-and-time-linux/" rel="nofollow">Date and Time</a>](https://www.makeuseof.com/how-to-set-date-and-time-linux/)
- [References](#references)

<!-- Created by https://github.com/ekalinin/github-markdown-toc -->
<!-- Added by: richard, at: Sat 10 Aug 13:44:11 JST 2024 -->

<!--te-->

### [`/proc/stat`](https://www.linuxhowtos.org/System/procstat.htm)

- **user:** normal processes executing in user mode
- **nice:** niced processes executing in user mode
- **system:** processes executing in kernel mode
- **idle:** twiddling thumbs
- **iowait:** waiting for I/O to complete
- **irq:** servicing interrupts
- **softirq:** servicing softirqs

## Syntax

### `for` loop

```sh
for i in {2..17}; do; echo ${i}; done
```

#### loop through files

```sh
for filename in *; do echo "put ${filename}"; done
```

## Launch Terminal

Launch terminal and execute command without autoclose

```sh
$TERMINAL --hold -e $SHELL tmux-workbench
```

`-e`: Execute
`--hold`: Do not autoclose

## `chmod` Change file mode

- r: Read
- w: Write
- x: Executable

| user | group | everyone |
| ---- | ----- | -------- |
| rwx  | rwx   | rxw      |
| 421  | 421   | 421      |
| 7    | 7     | 7        |

> Numbers represent bits

### Make bash script executable

```sh
chmod +x filename.sh
```

---

## Colors

| Color        | Code | Color        | Code |
| ------------ | ---: | ------------ | ---: |
| Black        | 0;30 | Dark Gray    | 1;30 |
| Red          | 0;31 | Light Red    | 1;31 |
| Green        | 0;32 | Light Green  | 1;32 |
| Brown/Orange | 0;33 | Yellow       | 1;33 |
| Blue         | 0;34 | Light Blue   | 1;34 |
| Purple       | 0;35 | Light Purple | 1;35 |
| Cyan         | 0;36 | Light Cyan   | 1;36 |
| Light Gray   | 0;37 | White        | 1;37 |

```sh
RED='\033[0;31m'
NC='\033[0m' # No Color
printf "I ${RED}love${NC} Stack Overflow\n"]]'
```

### tput

**[Color codes for tput](https://stackoverflow.com/questions/54838578/color-codes-for-tput-setf)**

```sh
export fgBlack8="$(tput setaf 0)";
export fgRed8="$(tput setaf 1)";
export fgGreen8="$(tput setaf 2)";
export fgYellow8="$(tput setaf 3)";
export fgBlue8="$(tput setaf 4)";
export fgMagenta8="$(tput setaf 5)";
export fgCyan8="$(tput setaf 6)";
export fgWhite8="$(tput setaf 7)";
```

---

## Symlink

- [symlink-tutorial-in-linux](https://www.freecodecamp.org/news/symlink-tutorial-in-linux-how-to-create-and-remove-a-symbolic-link/)

### Add Symlink

```sh
ln -s <path to the file/folder to be linked> <the path of the link to be created>
```

#### File

```sh
ln -s /home/james/transactions.txt trans.txt
```

#### Folder

```sh
ln -s /home/james james
```

### Check if Symlink

```sh
ls -l <path-to-assumed-symlink>
```

### Remove Symlink

```sh
unlink <path-to-symlink>
```

OR

```sh
rm <path-to-symlink>
```

### Broken Symlink links

#### Find

```sh
find /home/james -xtype l
```

#### Delete

```sh
find /home/james -xtype l -delete
```

---

## General commands

### count words, lines, characters

> The counts the lines of the `ls` command's result

```sh
ls -a | wc -l
```

### Login as other user

> The dash redirects you to their home directory

```sh
su - otheruser
```

### mv - Move / Rename

```sh
mv /home/user/oldname /home/user/newname
```

### ss - Investigate Sockets

Check if port is open

```sh
ss -lntu
```

```sh
ss -lntu | grep :80
```

### `diff`

Check diff recursively and just show different files without showing their contents

```sh
diff -rq path/to/dir1 path/to/dir2
```

### `sed` (Find and replace)

#### When using sed on **urls**, use `,` instead of `/` as a delimiter

### File Management

#### `stat` File Info

```sh
stat file_name
```

#### `basename` Get base filename or foldername

> Remove leading directory portions from a path.
> Show only the file name from a path:

```sh
basename path/to/myfile.ext
# myfile.ext
```

Show only the rightmost directory name from a path:

```sh
basename path/to/mydirectory/
# mydirectory
```

Show only the file name from a path, with a extension removed:

```sh
basename path/to/myfile.ext .ext
myfile
```

#### `realpath` Get absolute path

```sh
realpath path/to/myfile.ext
# /abs/path/to/myfile.ext
```

#### `du` Disk Usage

> File size

##### Show the size of a single directory, in human readable units:

```sh
du -sh path/to/directory
```

##### List the human-readable sizes of a directory and any subdirectories, up to N levels deep:

```sh
du -h --max-depth=N path/to/directory
```

### Check Input code `xev`

#### Start xev(1) and show only the relevant parts.

xev | awk -F'[ )]+' '
/^KeyPress/ {
a[NR+2]
}
NR in a {
printf "%-3s %s\n", $5, $8
}
'

### Set default file manager

[`xdg-mime`](https://unix.stackexchange.com/questions/590487/debian-buster-xfce-make-pcmanfm-default-including-for-open-a-directory-as-in)

```sh
xdg-mime default pcmanfm.desktop inode/directory
```

### [`nohup`](https://www.gnu.org/software/coreutils/nohup)

Allows for a process to live when the terminal gets killed. [0/36]

#### Run a process that can live beyond the terminal:

```sh
nohup command command_arguments
```

#### Launch nohup in background mode:

```sh
nohup command command_arguments &
```

#### Run a shell script that can live beyond the terminal:

```sh
nohup path/to/script.sh &
```

#### Run a process and write the output to a specific file:

```sh
nohup command command_arguments > path/to/output_file &
```

### `find`

#### List of All Folders and Sub-folders

```sh
find . -type d
```

### Convert text files

> Below fixes utf-8 encoding (removing all non utf-8 characters)

```sh
iconv -f utf-8 -t utf-8 -c FILE.txt -o NEW_FILE
```

---

## Compression / Archiving

### `unzip`

View zip contents

```sh
unzip -l myzipfile.zip
```

### `zip`

#### Create an encrypted archive (user will be prompted for a password):

```sh
zip -e -r compressed.zip path/to/directory
```

##### Exclude subdirectory

```sh
zip -r zipperall.zip dir -x "dir/subdir/*" "dir/anothersubdir/*"
```

### `tar`

#### To create a .gz archive and exclude all jpg,gif,... from the tgz

```sh
tar czvf /path/to/foo.tgz --exclude=\*.{jpg,gif,png,wmv,flv,tar.gz,zip} /path/to/foo/
```

#### Extract a (compressed) archive into the current directory:

```sh
tar xf source.tar[.gz|.bz2|.xz]
```

---

## `cp` (Copy)

### `-i` Interactive

Asks you if you would like to replace the file

```sh
cp -i /home/levan/kdenlive/untitelds.mpg /media/sda3/SkyDrive/
```

### `-b` Backup

Create a backup of your file

```sh
cp -b /home/levan/kdenlive/untitelds.mpg /media/sda3/SkyDrive
```

### `-a` Improved Recursive(folder)

Preserve all file attributes, and also preserve symlinks.

```sh
cp -a /source/. /dest/
```

The `.` at end of the source path is a specific cp syntax that allow to copy all files and folders, included hidden ones.

### `-v` Explain what is being done

### `-r` Copy directories recursively

```sh
cp -r /source/ /dest/
```

### `-n` Do not override

### Combine commands

```sh
cp -avr /tmp/conf/ /tmp/backup
```

---

## [`scp` Secure Copy](https://www.computerhope.com/unix/scp.htm)

### Local Copy

```sh
cp /home/stacy/images/image*.jpg /home/stacy/archive
```

### Upload

```sh
scp /home/stacy/images/image*.jpg stacy@myhost.com:/home/stacy/archive
```

#### Using a ssh private_key

```sh
cp -i ~/.ssh/private_key local_file stacy@myhost.com:/path/remote_file
```

### Download

```sh
scp stacy@myhost.com:/home/stacy/archive/image*.jpg /home/stacy/downloads
```

#### Using a ssh private_key

```sh
cp -i stacy@myhost.com:/path/remote_file /home/stacy/downloads
```

### Remote Copy

#### Same Server

```sh
scp user@myhost.com:/home/user/dir1/file.txt user@myhost.com:/home/user/dir2
```

#### Cross Server

```sh
scp someuser@alpha.com:/somedir/somefile.txt someuser@beta.com:/anotherdir
```

---

### Quickly create folders / Directories + Subdirectories

#### `-p`, `--parents`

> no error if existing, make parent directories as needed

```sh
mkdir -p folder/{sub1~~ub2}/{sub1~~ub2~~ub3}
```

#### Other

```sh
# 50 directories from sa1 through sa50
mkdir sa{1..50}
# same but each of the directories will hold 50 times sax1 through sax50 (-p will
# create parent directories if they do not exist.
mkdir -p sa{1..50}/sax{1..50}
# 26 directories from a12345 through z12345
mkdir {a-z}12345 
# comma separated list makes dirs 1, 2 and 3
mkdir {1,2,3}
# 10 directories from test01 through test10
mkdir test{01..10}
# same as 4 but with the current date as a directory and 1,2,3 in it.
mkdir -p `date '+%y%m%d'`/{1,2,3} 
# same as 4 but with the current user as a directory and 1,2,3 in it.
mkdir -p $USER/{1,2,3}
```

## Redo last command but as root

```sh
sudo !!
```

## Open an editor to run a command

ctrl+x+e

## Create a super fast ram disk

```sh
mkdir -p /mnt/ram
mount -t tmpfs tmpfs /mnt/ram -o size=8192M
```

## List files/folders

```sh
ls
```

### Don't add command to history (note the leading space)

```sh
ls -l
```

### List hidden files/folders

```sh
ls -la
```

## Fix a really long command that you messed up

```sh
fc
```

## Tunnel with ssh (local port 3337 -> remote host's 127.0.0.1 on port 6379)

```sh
ssh -L 3337:127.0.0.1:6379 root@emkc.org -N
```

## Intercept stdout and log to file

```sh
cat file | tee -a log | cat > /dev/null
```

## Bonus: exit terminal but leave all processes running

```sh
disown -a && exit~~<++>~~<++>~~<++>
```

## `iconv` File Convert

### Change encoding of file

```sh
iconv -f SHIFT-JIS -t UTF-8 <infile> > <outfile>
```

## Navigation

### `pushd` & `popd`

Work like `cd` but `popd` brings you back to the directory where you typed `pushd`

---

## `rename`

### Bulk Rename

```sh
rename -vs 06_count digit *
```

> Output

```sh
'06_count_1.png' renamed to 'digit_1.png'
'06_count_2.png' renamed to 'digit_2.png'
'06_count_3.png' renamed to 'digit_3.png'
'06_count_4.png' renamed to 'digit_4.png'
'06_count_5.png' renamed to 'digit_5.png'
'06_count_6.png' renamed to 'digit_6.png'
'06_count_7.png' renamed to 'digit_7.png'
'06_count_8.png' renamed to 'digit_8.png'
'06_count_9.png' renamed to 'digit_9.png'
```

---

# [Terminal output to a file](https://askubuntu.com/questions/420981/how-do-i-save-terminal-output-to-a-file)

## Output to new file (overwrite)

```sh
SomeCommand > SomeFile.txt
```

## Output append to file

```sh
SomeCommand >> SomeFile.txt
```

## Output to new file **with error** `stderr` (overwrite)

```sh
SomeCommand &> SomeFile.txt
```

## Output append to file **with error** `stderr`

```sh
SomeCommand &>> SomeFile.txt
```

## Have both `stderr` and output displayed on the console and in a file

```sh
SomeCommand 2>&1 | tee SomeFile.txt
```

```sh
          || visible in terminal ||   visible in file   || existing
  Syntax  ||  StdOut  |  StdErr  ||  StdOut  |  StdErr  ||   file   
==========++==========+==========++==========+==========++===========
    >     ||    no    |   yes    ||   yes    |    no    || overwrite
    >>    ||    no    |   yes    ||   yes    |    no    ||  append
          ||          |          ||          |          ||
   2>     ||   yes    |    no    ||    no    |   yes    || overwrite
   2>>    ||   yes    |    no    ||    no    |   yes    ||  append
          ||          |          ||          |          ||
   &>     ||    no    |    no    ||   yes    |   yes    || overwrite
   &>>    ||    no    |    no    ||   yes    |   yes    ||  append
          ||          |          ||          |          ||
 | tee    ||   yes    |   yes    ||   yes    |    no    || overwrite
 | tee -a ||   yes    |   yes    ||   yes    |    no    ||  append
          ||          |          ||          |          ||
 n.e. (*) ||   yes    |   yes    ||    no    |   yes    || overwrite
 n.e. (*) ||   yes    |   yes    ||    no    |   yes    ||  append
          ||          |          ||          |          ||
|& tee    ||   yes    |   yes    ||   yes    |   yes    || overwrite
|& tee -a ||   yes    |   yes    ||   yes    |   yes    ||  append

-----------------------------------------------------------------------
```

# Systemd

## `systemctl`

> Manage services and check statuses and change system states.

### Services

- start
- stop
- restart

_Example :_

```sh
systemctl stop docker.service
```

# Tarballs

## Patches

### Make Patch

```sh
diff -up original-program-directory modified-program-directory > \
           toolname-patchname-RELEASE.diff
```

### Apply Patch

```sh
patch -p1 < path/to/patch.diff
```

---

## Font

```sh
fc-list
```

## Convert UNICODE

```sh
echo -e "\u30e2\u30fc\u30c9"
```

## [Date and Time](https://www.makeuseof.com/how-to-set-date-and-time-linux/)

`timedatectl`

---

# References

- https://github.com/engineer-man/youtube/blob/master/058/commands.sh
