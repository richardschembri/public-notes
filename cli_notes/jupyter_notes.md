Jupyter Notebook notes
======================

[Install](https://jupyter.org/install)
---------
```
pip install jupyterlab
```
### Convert to python
#### Install
```
pip install nbconvert
```
#### Convert
##### Windows
```
jupyter-nbconvert.exe --to script '.\MyScript.ipynb'
```
