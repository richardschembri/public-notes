# Node-RED
## Installation
### Docker
#### [Install](https://www.docker.com/get-started)
``` sh
docker pull nodered/node-red
```

#### Run simplest form
``` sh
docker run -it -p 1880:1880 -v myNodeREDdata:/data --name mynodered nodered/node-red
```
### [Import/Export Flows](https://nodered.org/docs/user-guide/editor/workspace/import-export#exporting-flows)
### [Google Smarthome](https://flows.nodered.org/node/node-red-contrib-google-smarthome)

### Windows
#### [Install](https://nodered.org/docs/getting-started/windows)
- Install NodeJS

### Microsoft Azure
1. Azureコンソールにログイン
2. Ubuntu ServerのVirtual Machineを生成
3. Ubuntuのユーザーとパスを設定
4. node.jsはシングルスレッド何のでマルチコアインスタンスが必要ないです。
5. 設定のステップで「Network security group」オプションを選んで、'Inbound rule'を追加して下記の設定を入力します：
	- *Name:* node-red-editor
	- *Priority:* 1010
	- *Protocol:* TCP
	- *Destination port range:* 1880
6. 設定ページでOKを押すと「deploy new instance」のOKを押します。

#### Node-REDのセットアップ
1. インスタンスにログインしてnode.jsをインストールして、Node-REDをインストールします
	``` sh
	curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
	sudo apt-get install -y nodejs build-essential
	sudo npm install -g --unsafe-perm node-red
	```
2. pm2を使ってnode-REDをスタートアップに設定します
	``` sh
	sudo npm install -g --unsafe-perm pm2
	pm2 start `which node-red` -- -v
	pm2 save
	pm2 startup
	```
## Creating Nodes
### [package.json](https://nodered.org/docs/creating-nodes/packaging)
``` json
{
	"name"         : "node-red-contrib-samplenode",
	"version"      : "0.0.1",
	"description"  : "A sample node for node-red",
	"dependencies": {
	},
	"keywords": [ "node-red" ],
	"node-red"     : {
		"nodes": {
			"sample": "sample/sample.js"
		}
	}
}

```

- *name*: "node-red"の名前を使いたい場合は"node-red-contrib-"の接頭が必要です。
- *keywords*: "node-red"を使わないといけないです。
- *node-red*: このキーにモジュールが何のノードが含めているを入力します。

ライセンスを追加した法がいいです。

投稿リクエストは手動で行う必要があります。
https://flows.nodered.org/

## [Google Home Setup](https://flows.nodered.org/node/node-red-contrib-google-smarthome)


## [Node-RED SSL using Letsencrypt & Certbot](https://discourse.nodered.org/t/node-red-ssl-using-letsencrypt-certbot/17606)

## [Node-RED to IFTTT](https://www.youtube.com/watch?v=iG_KOxtzjuY)

## Logs
``` javascript
pm2 logs
```
--------------------------------------------------------------------------------

## Nodes

### Function Node
#### [Debug](https://discourse.nodered.org/t/how-to-debug-a-function-node/38024)
Side by log `node.warn()`
