Grep
====

--------------------------------------------------------------------------------

[ripgrep](https://github.com/BurntSushi/ripgrep)
=======================================

### [Exclude Files / Glob](https://github.com/BurntSushi/ripgrep/blob/master/GUIDE.md#manual-filtering-globs)
> Exclude `js` and `xml` files
```sh
rg "my text" -g '!*.{js,xml}'
```
