[⚔ Taskwarrior](https://linuxconfig.org/keep-track-of-your-schedule-with-taskwarrior)
===========

➕Add
-----
### # Add task to project with tag
``` sh
$ task add project:mycompany.cards +art draft design
```
- **Project:** `mycompany`
- **Sub Project:** `cards`
- **Tag:** `art`
- **Task:** "draft design"

### 🏆 Priority
Can be shortened to `pri`

- High
- Medium
- Low
- None(default)

uda.priority.values = H,M,L,

### 📅 Dates

``` sh
$ task add Send Dad birthday card
  wait:2017-12-12
  schedule:2017-12-15
  until:2017-12-25
```
-`wait`: Wait until date before displaying task
-`schedule`: Increase priority on scheduled date
-`until`: Delete task on date

#### Dates relative to other dates

``` sh
$ t /conference/ ids
```
``` sh
$ task add Book flights
  schedule: "8.due - 30d"
  until: "8.due"

### Recurring tasks
``` sh
$ task add "a daily recurring task" recur:daily due:eod

$ task add "a weekly recurring task" recur:weekly due:eow

$ task add "a monthly recurring task" recur:monthly due:eom
```

🖋 Annotate
-----------
``` sh
task 1 annotate Anno
```

🗒 List
-------

### List projects

``` sh
task projects
```

## Synchronize with server

``` sh
task sync
```

🔧 Modify
---------

### modify latest

``` sh
+LATEST
```

Configuration `~/.taskrc`
-----------------------
### Load with specific config
``` sh
alias habit="task rc.data.location=~/.habit"
```

Specifies an alternate configuration file.
``` sh
task rc:<path-to-alternate-file> ...
```
### Configure urgency of tags

Set "costly" tag urgency to 3
``` sh
urgency.user.tag.costly.coefficienct = 3.0
```
Set "leisure" tag urgency to 0
``` sh
urgency.user.tag.leisure.coefficienct = 0
```

### Include other config files
``` sh
include ~/themes/solarized-dark-256.theme
include ~/.config/.taskserverrc
```
