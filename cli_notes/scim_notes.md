# (sc-im notes)[https://raw.githubusercontent.com/andmarti1424/sc-im/freeze/src/doc]
<kbd>f</kbd><kbd>r</kbd>: Freeze a row or the rows selected
<kbd>f</kbd><kbd>c</kbd>: Freeze a col or the cols selected
<kbd>f</kbd><kbd>\<</kbd>: Decrement column width
<kbd>f</kbd><kbd>\></kbd>: Increment column width

## Cell Color
`:cellcolor "{key}={arg}"` Change the color of the current cell or range.
_Example:_
`:cellcolor "bg=CYAN fg=WHITE"`
`:cellcolor "fg=RED bold=1 underline=1"`
`:cellcolor A2:A6 "fg=CYAN bold=1 underline=1"`

## Format
`:format "{format_string}"` Set the numeric format for the selected cell or range.
_Example:_
`:format "###,###,000"`
`:format "d%d/%m/%Y"`
`:format "####.####E+3"`


# INSERT MODE
<kbd>=</kbd>: Enter a numeric constant or expression.
<kbd>\<</kbd>: Enter a left justified string or string expression.
<kbd>\\</kbd>: Enter a centered label.
<kbd>\></kbd>: Enter a right justified string or string expression.
