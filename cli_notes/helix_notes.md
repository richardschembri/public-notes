Helix Notes
===========
General
-------
### Quick Notes

#### Run command and paste output
```sh
:insert-output
```

LSP
---
### Quick Notes

#### Check all LSPs
```sh
helix --health
```

#### Check specific LSP
```sh
helix --health marksman
```

### Markdown(marksman)


| Command        | Action           |
| ------------- |:-------------:|
| `gd`      | Go to page |
