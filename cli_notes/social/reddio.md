[Reddio (Reddit CLI)](https://gitlab.com/aaronNG/reddio)
==================================================

Commands
--------
### Print the two hottest submissions of `r/commandline`
```sh
reddio print -l 2 r/commandline
```

