(Reference Guide)[https://rolfschr.github.io/gswl-book/latest.html#assumptions-promises]
# Intro
## Double-entry Accounting
### "Account"
Every type of _expense or income_ and evey _place_ which holds monetary value.
Example Accounts: 
* Groceries
* Bike
* Holidays
* Checking Account of Bank X
* Salary
* Mortgage

### Double-entry
* Track flow of money from one account to another.
* "Double": Adding $1000 from one account means removing $1000 from another. Therefore total balance of all accounts is always zero.
* **Note that** more than two accounts may be involved in one transaction.
#### Examples:
* Buying book online for $15: `Account Creditcard X` -> `Account Books`
* $2000 Salary: `Account Salary` -> `Account Bank`
* Buying grocieries and detergent from supermarket: `Account Creditcard X` -> `Account Groceries` + `Account Household`

#### Main Accounts
##### Expenses
* Groceries
* Taxes
* Gifts Given
* Donations
##### Income
* Paychecks
* Gifts received
* Dividends
* Interest
##### Assets
* Bank Accounts
* Wallet
* Investments
* Loans credited
##### Liabilities
* Mortgage
* Credit Cards
* Loans Owed
* Bank Loans 
##### Receivables
##### Equity

> Level of detail needed for subcatagories(`Expenses` -> `Groceries` -> `Fruits` -> `Bananas`) is up to the requirements.

## Ledger
> Extensive documentation can be found at http://ledger-cli.org
Ledger boils down to _two_ distinct types of _action_:
* Updating the list of transactions (the `journal`)
* View/Interpret that data

### Transaction Structure
* header line: date + meta deta
* List of accounts involved in the transaction
	* Account subcatagories are seperated by a ":"

#### Example of a transaction:
```
2042/02/21 Shopping
    Expenses:Food:Groceries                 $42.00
    Assets:Checking                        -$42.00
```

> Ledger is clever enough to calculate the appropriate amount so it would have been perfectly valid to only write:

```
2042/02/21 Shopping
    Expenses:Food:Groceries                 $42.00
    Assets:Checking
```

### The Jornal file
#### Example of a Journal file
```
; The opening balance sets up your initial financial state.
; This is needed as one rarely starts with no money at all.
; Your opening balance is the first "transaction" in your journal.
; The account name is not special. We only need something convenient here.
2041/12/31 * Opening Balance
    Assets:Checking                       $1000.00
    Equity:OpeningBalances

; The money comes from the employer and goes into the bank account.
2041/01/31 * Salary
    Income:Salary                           -$1337
    Assets:Checking                          $1337

; Groceries were paid using the bank account's electronic cash card
; so the money comes directly from the bank account.
2042/02/15 * Shopping
    Expenses:Food:Groceries                 $42.00
    Assets:Checking

; Although we know the cash sits in the wallet, everything in cash is
; considered as "lost" until recovered (see next transaction and later chapters).
2042/02/15 * ATM withdrawal
    Expenses:Unknown                       $150.00
    Assets:Checking

; Paying food with cash: Moving money from the Expenses:Unknown
; account to the food account.
2042/02/15 * Shopping
    Expenses:Food:Groceries                 $23.00
    Expenses:Unknown

; Ledger automatically reduces 'Expenses:Unknown' by $69.
2042/02/22 * Shopping
    Expenses:Food:Groceries                 $23.00
    Expenses:Clothing                       $46.00
    Expenses:Unknown

; You can use positive (add money to an account) or negative
; (remove money from an account) amounts interchangeably.
2042/02/22 * Shopping
    Expenses:Food:Groceries
    Expenses:Unknown                       -$42.00
```

# Setup
## Sample
Copy or download the text from this (sample file)[https://gist.github.com/rolfschr/318f1f91f8f845864568]  into a text file called `journal.txt` and run the following commands:
```
$ # Usage: ledger -f <journal-file> [...]
$ ledger -f journal.txt balance
$ ledger -f journal.txt balance Groceries
$ ledger -f journal.txt register

# Start an interactive session
# and type "balance", then  press Enter
# (press ctrl+d to quit)
$ ledger -f journal.txt
```

## Folder Structure
_Code_ and _data_ are _split_ up into _two_ seperate _folders_. This allows to only encrypt what is necessary, share common code and more easily enable independent version control.
### ecosystem
Contains the scripts & other stuff to manipulate the journal.
### private
Contains the actual financial data.

> A sample `Ledger` with the above folder structure can be found (here)[https://github.com/rolfschr/GSWL-ecoystem]

## Common Files
### convert.py + report.py
	Integrate external CSV data or interpret the data respectively.
### `alias` file
	BASH script which defines common aliases and functions
## Private Data
* Ledger expects the _journal_ file to be provided with `-f`.
* The _aliases_ defined in the _ecosystem_ assume the file to be named `name.txt`
* `main.txt` in the git repo only has a list of `include` statements.
	```
	; This is the main entry file for ledger.
	; Nothing fancy happens here. All real data is in journal.txt, configuration
	; stuff etc. is to be found in meta.txt.

	; Include the config file.
	include meta.txt

	; Include the actual journal.
	include journal.txt
	```
* _Actual transactions_ are all stored in `journal.txt`

### Meta Data
`meta.txt` should contain all Ledger configuration stuff & any other non-transactional data.

#### `account` 
Lets you predefine any accounts that shall be used by Ledger. Ledger does not require you to do so but it is good practice anyway.
`--pedantic` command line arguement causes Ledger to produce an error when unknown accounts are used.
##### Example `account` definitions:
```
account Assets:Checking
account Expenses:Dining
account Expenses:Groceries
account Income:Salary
```
#### `commodity`
Defines calid commodities in use:
```
commodity $
commodity €
commodity BTC
```
### Other files
#### `alias.local`
Contains local configurations. Sourced automatically by `ledger-ecosystem/alias`.
#### `bankaccounts.yml`, `csv2journal.txt` + `misc.tmp.txt` 
Used to update the journal in an automated fashion.
#### `reports.txt` 
Lists questions repeatedly asked about the financial situaiton.

## Orchestrating ecosystem & private data
* Working with Ledger means working in the private folder.
* To unleash the power of all _scripts_ etc.,one needs to source `ecosystem/alias` from within the _private folder_. This in turn sources `alias.local` from the current working directory.
* The local `alias` file allows to overwrite ecosystem functionality or add new features.
`$ sl` _start ledger_ initiates the custom ledger `alias`es
### Sample commands:
```
$ cd ~/src/GSWL-private && source ~/src/GSWL-ecosystem/alias # See GSWL-private/.bashrc for an alias!
$ which led
$ led bal
$ led reg
$ ledreports # explained later
```
> `led` is an _alias_ for `ledger -f main.txt`

## Tmux & Tmuxinator
Ledger works well with these tools:
* (Tmux)[http://tmux.sourceforge.net/]
* (Tmuxinator)[https://github.com/tmuxinator/tmuxinator]
```
$ cp ~/src/GSWL-private/.tmux.conf ~/ # Optional, only if you've never used tmux
$ mkdir -p ~/.tmuxinator
$ ln -s ~/src/GSWL-private/.tmuxinator.GSWL-private.yml ~/.tmuxinator/GSWL-private.yml
$ mux start GSWL-private # Starts a new Tmux session
```
Within each Tmux session window, `ecosystem/alias` is sourced.
The sample GSLW-private/.bashrc provides some aliases to start/stop the Tmux sessions. You should source this file in your ``~/.bashrc` anyways.

## Reports
A report displays the journal in some meaningful way. The most standard commands for reporting are `balance` and `register`.
### Balance reports
Creates a total balance from all transactions.
The basic command is:
```
$ ledger -f file.txt bal[ance]
# or, in our case
$ led bal # alias defined by ecosystem/alias
```
Sample output:
```
        $2145.00  Assets:Checking
       $-1000.00  Equity:OpeningBalances
         $192.00  Expenses
          $65.00    Food:Groceries
         $127.00    Unknown
       $-1337.00  Income:Salary
 ---------------
               0
```
Restrictions can be specified for a more precise `balance`:
```
# Restrict by date.
$ led (--period|-p) "last 6 months" bal
$ led -p "2042" bal

# Restrict by account name.
$ led bal ^Expenses

# Restrict by account names.
$ led bal ^Expe and not Groceries

# Show all assets in the same currency (this assumes a prices database for conversion, see below).
led bal --exchange $ ^Assets

# Do not show sub accounts.
led --depth 2 bal
led --depth 3 bal # Note how the totals do not change.

# Do not indent accounts.
led --flat bal
```

### Register reports
Register reports display the journal like an old-fashioned register. 
```
# Show the register report.
$ led reg
# (The second last column gives the actual amount, the last column the running sum.)

# Restrict time span.
$ led -p "2041" reg Assets

# Show the running average.
$ led reg -A Restaurant
# (Ignore the "<Adjustment> lines". The 2nd column gives the running average.)

# Group by month.
$ led reg -M Food

# Collapse transactions with multiple postings into one.
$ led reg -M -n Expenses # compare against 'led reg -M Expenses'
```
## Advanced Report Filter
`--limit`(`-l`):  
* Limits the posting to be considered for calculations.
* Filter active whilst going through the `journal` file.

`--display`(`-d`): 
* Limits the postings to be considered for display.
* Filter active _after_ going read the `journal` completely.
### Examples:
```
# Show monthly expenses & average since Nov 2041
$ led -M -n -A --limit "date>=[2041/11/01]" reg ^Expenses
```
vs
```
# Show monthly expenses since Nov 2011 & average monthly expense since the dawn of time
$ led -M -n -A --display "date>=[2041/11/01]" reg ^Expenses
```
both commands can be combined:
```
# Show monthly expenses for Mar 2042 & average monthly expenses since Nov 2011
$ led -M -n -A --limit "date>=[2041/11/01]" --display "date>=[2042/03/01]" reg ^Expenses
```

## Recurring Reports
`reports.py`: Opens the file `reports.txt` in the current working directory and displays predefined reports one by one. The script sources the `alias` file from the ecosystem in the current working directory.
```
$ sl # "start ledger" as defined the .bashrc
$ ledreports
```
## Other Reports

```
$ led stats
$ led accounts
$ led payees
$ led print
$ led xml
```

## Visualization

`-j`/`-J` allow to feed ledger's output to gnuplot (or other tools) to _visualize_ data.
* `-j` The current amount.
* `-J` The running total.
### Current Amount Example:
```
# Output monthly expenses
$ led reg -n --monthly -j Expenses
```
#### Output:
```
2041-09-01 509
2041-10-01 484
2041-11-01 955.49
2041-12-01 809.49
2042-01-01 455.5
2042-02-01 285.5
2042-03-01 882.47
```

### Running Total Example:
```
 Output cumulative monthly expenses
$ led reg -n --monthly -J Expenses
```
#### Output:
```
2041-09-01 509
2041-10-01 993
2041-11-01 1948.49
2041-12-01 2757.98
2042-01-01 3213.48
2042-02-01 3498.98
2042-03-01 4381.45
```
### gnuplot (A portable command-line driven graphing utility )
The ecosystem/alias defines the function ledplot which wraps around gnuplot to visualize some data:
```
ledplot -j -M -n reg Expenses # assuming gnuplot is installed
```

## Updating the journal
Remember the journal keeps track of all financial transactions.
Updating the journal happens in two steps:
	1. Manually adding transactions(everything without electronic record, i.e. cash transactions).
	2. Using ledger's in-built conversion method to automatically add CSV data.

### Cash transactions
* _The `journal.txt` is never manipulated directly_
* Cash transactions should be added to the file `misc.tmp.txt`. (`.tmp.txt` should be in `.gitignore`)
#### `Expenses:Unknown` 
In order to avoid the hassel of tracking every expense, withdrawn cash is considered as gone. 
`Assets:YourBank:Checking` -> `Expenses:Unknown`
> When you know how the money was spent
`Expenses:Unknown` -> **whatever account you want**

#### `misc.tmp.txt` Example:
```
; This file lists all cash transactions that happened *after* the last
; journal update. Once this data has been added to journal, this file
; is emptied. Note how the scheme is always the same: move money from
; Expenses:Unknown to a specific account.

2042/04/10 * Swimming
    Expenses:Sports:Swimming                $10.00
    Expenses:Unknown

2042/04/13 * Cinema
    Expenses:Cinema                         $15.00
    Expenses:Unknown

2042/04/18 * Tails of the City
    Expenses:Books                          $5.00
    Expenses:Unknown
```
### Electronic transactions
* Virtually every financial institution (banks, credit unions, payment service provider, etc.) provides you with a CSV file that lists your transactions.
* Ledger has the built-in command convert which automatically converts CSV files into Ledger's transaction format. The main feature of interest for us is the account recognition based on the transaction's payee. See meta.txt and look for "payee" to get a first feeling how that might work. We'll use this command in combination with an utility script to convert CSV files in a quite efficient way.
* `reckon` and `csv2ledger` are scripts that convert CSV data.

#### The general work flow for electronic transactions
* Download the CSV file from the bank.
* Call the utility script to convert the input data.
* Check for "unknown" (= not yet recognized) transactions; modify meta.txt to match these bank transactions with your Ledger accounts.
* Repeat until done.

