Ledger Notes
============
File types
----------
`.ldg`, `.ledger`, `.dat`

Journal
-------
### Virtual Transaction
``` sh
$ ledger bal checking
               $1000  Assets:Checking
```
``` ledger
2011/12/01 * Establish Emergency Fund
    [Funds:Emergency]                    $500.00
    [Assets:Checking]
```
``` sh
 $ ledger bal checking funds
                 $500  Assets:Checking
                 $500  Funds:Emergency
 --------------------
                $1000
```
``` sh
$ ledger --real bal checking funds
               $1000  Assets:Checking
```

### Automated Envelope Budgeting (Automated Transactions)
``` ledger
= /Income:Salary/
    * Assets:Funds:Water            $50.00
    * Liabilities:Funds:Water      $-50.00

= /Expenses:Water/
    * Liabilities:Funds:Water          1.0
    * Assets:Funds:Water              -1.0

2015/04/02 * Salary
    Assets:Checking              $1,000.00
    Income:Salary

2015/05/02 * Salary
    Assets:Checking              $1,000.00
    Income:Salary

2015/05/03 * Water Bill
    Expenses:Water                  $95.00
    Assets:Checking                $-95.00
```

### [Include](https://www.reddit.com/r/plaintextaccounting/comments/fcx8ls/multifile_ledger_setup/)

Commands
--------
## Get Balance
Gets the balance, that is the total amounts for each **Acount**.
``` sh
ledger -f journal.ldg balance
``` 
### Filter by account
``` sh
ledger -f journal.ldg balance Cash
``` 
## Register / Running toal
Show all transactions and a running total. That is how the **Account**s
have been effected by each transaction.
``` sh
ledger -f journal.ldg register
``` 
### Filter by account
``` sh
ledger -f journal.ldg register Cash
``` 

### Filter by date
```sh
ledger -b oct bal
ledger -b "this oct" bal
ledger -b 2004/10 bal
ledger -b 10 bal
ledger -b last bal
ledger -b "last month" bal
```

### Period
```sh
ledger -p 2020/11/30 bal
```

Plugin URL: https://github.com/ledger/vim-ledger

## VIM
## References
- [Cheatsheet](https://devhints.io/ledger)

