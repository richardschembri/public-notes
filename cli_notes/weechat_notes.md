# WeeChat Notes
## Keybindings
- Bare Display(Click URL and select text mode): <kbd>Alt</kbd>+<kbd>l</kbd>

## Libera Chat Setup
### 1. Open WeeChat
``` sh
weechat
```
### 1. Open WeeChat
``` sh
weechat
```
### 2. Add Libera Chat server
``` sh
/server add liberachat irc.libera.chat/6697 -ssl
```

### 3. Enable SSL/TLS
``` sh
/set irc.server.liberachat.addresses "irc.libera.chat/6697"
/set irc.server.liberachat.ssl on
```

### 4. Connect to server
``` sh
/connect liberachat
```

### 5. Set Nick
``` sh
/nick <nickname>
```

### 6. Register Nick
``` sh
REGISTER <password> youremail@example.com
```

### 7. Configure SASL
``` sh
/set irc.server.liberachat.sasl_mechanism PLAIN
/set irc.server.liberachat.sasl_username <nickname>
/set irc.server.liberachat.sasl_password <password>
/save
```
