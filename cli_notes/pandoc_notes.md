Pandoc
======

Markdown to PDF
---------------
``` sh
pandoc MANUAL.txt --pdf-engine=xelatex -o example13.pdf
```

### CJK
``` sh
pandoc --pdf-engine=xelatex -V CJKmainfont="IPAGothic" presentation.md -o presentation.pdf
```


[Markdown Syntax](https://superuser.com/questions/796565/correctly-sizing-png-images-in-markdown-with-pandoc-for-html-pdf-docx)
--------------------------------------------------------
### Image Size
``` markdown
![my caption](./figures/myimage.png){ width=250px }
![my caption](./figures/myimage.png){ height=256px }
```
