# zip
```
zip -e [archive] [file]
```
```
zip -er [archive] [folder]
```
_Example:_
```
$ zip -er ~/Desktop/encrypted.zip ~/Documents/Confidential/
Enter password:
Verify password:
adding: ~/Documents/Confidential/ (deflated 13%)
```
