ImageMagick
===========
Meta Data
---------
View image metadata
``` sh
identify myimage.png
```
*Output:*
``` sh
myimage.png JPEG 600x600 600x600+0+0 8-bit sRGB 44990B 0.000u 0:00.000
```

Resize
------
``` sh
convert dragon_sm.gif    -resize 64x64  resize_dragon.gif
convert terminal_sm.gif  -resize 64x64  resize_terminal.gif
```

### Ignore Aspect Ratio (`!` flag)
``` sh
convert dragon_sm.gif  -resize 64x64\!  exact_dragon.gif
convert terminal.gif   -resize 64x64\!  exact_terminal.gif
```

### Percentage Resize (`%` flag)
``` sh
convert dragon_sm.gif   -resize 50%  half_dragon.gif
convert terminal.gif    -resize 50%  half_terminal.gif
```

convert
-------
### `gif` to `png`
#### 32-bit color (RGBA):
```sh
convert image.gif PNG32:image.png
```
#### 24-bit color (RGB):
```sh
convert image.gif PNG24:image.png
```
#### 8-bit Palette color:
```sh
convert image.gif PNG8:image.png
```
