# ArchLinux Notes
## Open app from terminal
`xdg-open`
## Instal AUR Packages

(yay Aur Helper)[https://github.com/Jguer/yay]
```
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si
```

## Install Ruby
```
pacman -S ruby
```
```
gem install rdoc --no-document
```

## pacman
Search for package
```
pacman -Ss package_name
```

## Disable hardware beep
`xset -b` 
> add this command in `~/.xprofile` for it to take effect after every boot.

