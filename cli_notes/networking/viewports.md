View Ports
==========

Linux
-----
### [ss](https://linuxize.com/post/check-listening-ports-linux/#check-listening-ports-with-ss)

- `ss -tunlp`: Check used ports

Macintosh
---------
- `sudo lsof -i -P`: Check used ports
