## OpenSUSE
### Firewall Setup
https://en.opensuse.org/SuSEfirewall2

#### Additional SSH security
https://en.opensuse.org/DenyHosts

### openSSH
https://en.opensuse.org/SDB:Configure_openSSH

### Public key authentication
https://en.opensuse.org/SDB:OpenSSH_public_key_authentication

### Yubikey support
https://developers.yubico.com/SSH/
