`arp-scan`
========
> List all IPs in the connected network
```sh
sudo arp-scan --interface=eth0 --localnet
```
