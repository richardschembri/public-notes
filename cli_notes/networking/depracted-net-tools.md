# Depracted Net Tools
> https://linux.die.net/man/

| Depracted Tool | Alternative Tool | Description  |
| -------------- |:----------------:| ------------:|
| arp            | ip n             | Address Resolution Protocol, which is used to find the address of a network neighbor for a given IPv4 address. |
| ifconfig       | ip a             | Displays information about all network interfaces currently in operation. |
| ipmaddr        | ip maddr         | adds, deletes, and displays multicast addresses. |
| iptunnel       | ip tunnel        | creates, deletes, and displays configured tunnels. |
| netstat        | ss               | Print network connections, routing tables, interface statistics, masquerade connections, and multicast memberships. |
| netstat -r     | ip route         | Display the kernel routing tables. |
| netstat -i     | ip -s link       | Display a table of all network interfaces, or the specified iface. |
| route          | ip r             | Show / manipulate the IP routing table. |
| mii-tool       | ethtool          | View, manipulate media-independent interface status |
| nameif         | ifrename         | Name network interfaces based on MAC addresses |
