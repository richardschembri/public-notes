ss - Scan Open Ports
====================

Display listening `TCP` Connections using ss
```sh
ss -tl
```

Display listening `UDP` Connections
```sh
ss -ul
```

Display both `TCP` **and** `UDP` connections
```sh
ss -lntup
```
