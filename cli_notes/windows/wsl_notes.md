Windows Subsystem for Linux (WSL)
=================================

Temporary files
---------------
The follow command creates systemd temporary files for applications like `tmux`
``` sh
systemd-tmpfiles --create
```
Change Shell
------------
``` sh
chsh -s /bin/zsh
```

Change distro version
---------------------
Check distros and their wsl versions
``` sh
wsl -l -v
```
Change distro version
``` sh
wsl --set-version <Distro> 2
```

[Could not find Mount](https://github.com/microsoft/WSL/issues/6174)
-------------------------------------------------
> WSL2 cannot access 'c': Input/output error
``` sh
wsl --shutdown
wsl
```
