Native VIM
==========
- `==`:  Automatic indentation
- `J`:  Remove line break
- `K`:  Help
- `/\c`:  Case insensitive search

- `a` `A` `i` `I` `o` `O`:               keys take counts:
- `9``i` *foo* `Space`/`Esc`:     inserts *foo foo foo foo foo foo foo foo foo*.

**redraw screen :** <kbd>CTRL</kbd>+<kbd>l</kbd>
**Navigate between panes :**<kbd>ctrl</kbd>-<kbd>h/j/k/l</kbd>
**Move panes :**<kbd>ctrl</kbd>+<kbd>H/J/K/L</kbd>
<kbd>;</kbd>: Next instance of <kbd>f</kbd>,<kbd>F</kbd>,<kbd>t</kbd> and <kbd>T</kbd>
<kbd>,</kbd>: Prev instance of <kbd>F</kbd>//<kbd>T</kbd>
**Go to block prev bracket :** <kbd>[</kbd><kbd>bracket</kbd>
**Go to block next bracket :** <kbd>]</kbd><kbd>bracket</kbd>

_number_ + <kbd>G</kbd> go to line number.

- `:e!`: Undo all

Create / Edit file
------------------
`:e` + file name

Spell Check
-----------
`:set spell` Turn on spell-checking

<kbd>CTRL</kbd>+<kbd>X</kbd> <kbd>s</kbd> to find suggestions in `INSERT MODE`.
<kbd>CTRL</kbd>+<kbd>N</kbd> to use the **next** suggestion.
<kbd>CTRL</kbd>+<kbd>P</kbd> to use the **previous** suggestion.

Resize Window
-------------
### Rows
`:resize 60`
`:res +5`
`:res -5`
#### Max
<kbd>Ctrl</kbd>-<kbd>w</kbd> <kbd>\_</kbd>

### Columns
`:vertical resize 80`
`:vertical resize +5`
`:vertical resize -5`
## Resize Split Window(Panes)
### Rows
<kbd>ctrl</kbd>-<kbd>w</kbd> <kbd>+</kbd>
<kbd>ctrl</kbd>-<kbd>w</kbd> <kbd>-</kbd>
### Columns
<kbd>ctrl</kbd>-<kbd>w</kbd> <kbd>\></kbd>
<kbd>ctrl</kbd>-<kbd>w</kbd> <kbd>\<</kbd>
#### Max
<kbd>Ctrl</kbd>-<kbd>w</kbd> <kbd>|</kbd>

Buffers
-------
A `+` sign means that there are unsaved changes in the bugger
### List Buffers
`:buffers` or `:ls`
### Navigate Buffers
`:bn`: buffer next
`:bp`: buffer previous
`:b`+number: Jumps to the buffer of that number
#### Go to alternate buffer
`:b#` 
<kbd>CTRL</kbd>+<kbd>6</kbd>
<kbd>CTRL</kbd>+<kbd>^</kbd>

Search in files / Vim Grep
--------------------------
`:vimgrep /mySearchTerm/g %` or `:vim /mySearchTerm/g %`
* `/g`: Means how line number
* `%`: Means search in current buffer
### Search in all files
* `**/*`: Means search search entire project directory
### Show results in list
`:cope` or ``
### Jump to search result
`:cnext`
`:cprev`
`:cfirst`
`:clast`

Search / Find files
-------------------
> Supports tab completion
`:find *.txt`

Change
------
`ci"`: change in quotes


Fold text
---------
`:set foldmethod` or `:set fmd` to check the fold method
### Manual Fold
`zf`: create fold selected text
`zo`: open fold
`zc`: close fold
`zd`: delete fold
`zR`: unfold all

#### Save / Load folds
> requires ~/.vim/view directory
`:mkview` saves folds in a **view**
`:loadview` loads folds from a **view**

Spell Check
-----------
- `:set spell` – Turn on spell checking
- `:set nospell` – Turn off spell checking
- `]s` – Jump to the next misspelled word
- `[s` – Jump to the previous misspelled word
- `z=` – Bring up the suggested replacements
- `zg` – Good word: Add the word under the cursor to the dictionary
- `zw` – Woops! Undo and remove the word from the dictionary

Find and replace
----------------
| Command        | Description                                                                                      |
| :------------- | :----------------------------------------------------------------------------------------------- |
| `:s/foo/bar/`  | Search for the first occurrence of the string "foo" in the current line and replace it with "bar"|
| `:s/foo/bar/g` | Replace all occurrences of the search pattern in the current line                                |
| `:%s/foo/bar/g`| Search and replace the pattern in the entire file                                                |

-----------------------------------------------------------------------

VIM Plugins
===========
vim-surround (Surround.Vim)
---------------------------
### Add
`ysiw]` add square brackets to `iw`
```
[Hello] world!
```
### Change
`cs"'` change `"` to `'`
#### Before:
```
"Hello world!"
```
#### After:
```
'Hello world!'
```
### Remove:
`ds"` remove `"`
#### Before:
```
"Hello world!"
```
#### After:
```
Hello world!
```

NERDtree
--------
<kbd>m</kbd>: Toggle menu for current node

Vim Wiki
--------
### [Key bindings](https://gist.github.com/drkarl/4c503bccb62558dc85e8b1bc0f29e9cb)
- **Open the default wiki index file:**		<Leader> ww
- **Select and open wiki index file:**		<Leader> ws
- **Delete wiki page:**						<leader> wd
- **Rename wiki page:**						<leader> wr

### Commands
### Search
- **Search Pattern:**	`:VimwikiSearch /pattern/` (the trailing slash is important)
	- **Next:**			`:lnext`
	- **Previous:**		`:lprevious`
	- **All matches:**	`:lope`
- **Search Tags:**	`VimwikiSearchTags :my-tag:`
- **Rename Wiki Page:** `<Leader>wr`	

Packer
------
### Plugin path
```sh
~/.local/share/nvim/site/pack/
```
