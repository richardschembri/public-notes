LaTeX
=====

Installation
------------
### OpenSUSE install
``` sh
sudo zypper in texlive-latex
```
### Archlinx install
``` sh
sudo pacman -S texlive-core
sudo pacman -S texlive-bin
yay texlive-localmanager-git
```

--------------------------------------------------------------------------------

CLI
----
### Generate PDF
``` sh
xelatex myTexFile.tex
```
##### Pass Parameters

``` sh
xelatex "\def\myparam{} \input{myTexFile.tex}"
```
``` sh
xelatex "\def\myotherparam{} \input{myTexFile.tex}"
```

--------------------------------------------------------------------------------

Packager
--------
### Add/Remove Package
> Archlinux uses `tllocalmgr` 
> Use `sudo texhash` in Archlinux after installing a package
``` sh
tlmgr install <package1> <package2> ...
tlmgr remove <package1> <package2> ...
```
### Install from file
``` sh
latex installfile.ins
```
### standalone
PDF is sized according to content
`\documentclass{standalone}`
### pgfgantt
Gantt chart functionality
**Documentation**: ftp://ftp.u-aizu.ac.jp/pub/tex/CTAN/graphics/pgf/contrib/pgfgantt/pgfgantt.pdf
### Fonts
* psnfss
* helvetic
## CJK
Japanese support
``` latex
\documentclass{article}
\usepackage{CJKutf8}
\newenvironment{Japanese}{%
  \CJKfamily{min}%
  \CJKtilde
  \CJKnospace}{}
\begin{document}
\begin{CJK}{UTF8}{}
\begin{Japanese}
\LaTeX で日本語を書きましょう！
\end{Japanese}
\end{CJK}
\end{document}
```
### svg
https://github.com/mrpiggi/svg
### OpenSUSE
``` sh
sudo zypper in texlive-svg
```

--------------------------------------------------------------------------------

Syntax
------
### [General](https://latexref.xyz/Index.html)
#### New line
``` latex
`\newline` Starts a new line
```
#### Variables / Commands
##### Set
``` latex
`\newcommand{\string_name}{ my string text}`: How to store strings
```
##### Reset
``` latex
`\renewcommand{\string_name}{ my string text change}`: Change string value
```

### Font sizes
``` latex
\tiny
\scriptsize
\footnotesize
\small
\normalsize
\large
\Large
\LARGE
\huge
\Huge
```

--------------------------------------------------------------------------------

