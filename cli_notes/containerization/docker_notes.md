Docker
======
Setup
-----
### [Linux](https://www.linux.com/topic/desktop/how-install-and-use-docker-linux/)
#### 1. Install Docker
> OpenSUSE
``` sh
sudo zypper in docker
```
#### 2. Add your user to the docker group
> In order to avoid using root user or sudo
``` sh
sudo usermod -a -G docker $USER
```
#### 3. Add your user to the docker group
``` sh
sudo groupadd docker && sudo gpasswd -a ${USER} docker && sudo systemctl restart docker
```

#### Daemon - Starting, stopping, and enabling
``` sh
sudo systemctl start docker

sudo systemctl enable docker

sudo systemctl stop docker

sudo systemctl restart docker
```

--------------------------------------------------------------------------------

Build
-----
```sh
docker build -t ubuntu-test:latest .
```
Does not work:
```sh
docker build -t ubuntu-test:latest ./Dockerfile
```

Containers
----------
### List all container IDs
`docker ps -q`

### Stop all containers
`docker stop $(docker ps -q)`

--------------------------------------------------------------------------------

Logs
----

- **Ubuntu (old using upstart):**       `/var/log/upstart/docker.log`
- **Ubuntu (new using systemd ):**      `sudo journalctl -fu docker.service`
- **Amazon Linux AMI:**                 `/var/log/docker`
- **Boot2Docker:**                      `/var/log/docker.log`
- **Debian GNU/Linux:**                 `/var/log/daemon.log`
- **CentOS:**                           `/var/log/message | grep docker`
- **CoreOS:**                           `journalctl -u docker.service`
- **Fedora:**                           `journalctl -u docker.service`
- **Red Hat Enterprise Linux Server:**  `/var/log/messages | grep docker`
- **OpenSuSE:**                         `journalctl -u docker.service`
- **OSX:**                              `~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/log/d‌​ocker.log`
- **Windows:**                          `Get-EventLog -LogName Application -Source Docker -After (Get-Date).AddMinutes(-5) | Sort-Object Time`

--------------------------------------------------------------------------------

`docker-compose`
--------------

### Access container
#### `docker`
```sh
docker exec -it 7eb97478c8de bash
```
#### `docker-compose`
```sh
docker-compose exec pm_web sh
```

### User development(non production) configuration
> The following commands will make `docker-compose.dev.yml` to override
> some value of `docker-compose.yml` which make your aims.
```sh
docker-compose -f docker-compose.yml -f docker-compose.dev.yml -d
```

### Remove
```sh
docker-compose rm
```

--------------------------------------------------------------------------------

AWS Deploy
----------

https://linuxhandbook.com/docker-permission-denied/
https://docs.aws.amazon.com/AmazonECS/latest/developerguide/docker-basics.html

```sh
sudo yum update -y
```

```sh
sudo amazon-linux-extras install docker
```

```sh
sudo yum install docker
```

```sh
sudo service docker start
```

```sh
sudo usermod -a -G docker $USER
```

```sh
sudo groupadd docker
```

```sh
sudo chown root:docker /var/run/docker.sock
```

```sh
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "$HOME/.docker" -R
```

### Install `docker-compose`
> Download `docker-compose` (Below example is version **1.29.2**)
```sh
sudo curl -L https://github.com/docker/compose/releases/download/1.29.2/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose   
```
> Make `docker-compose` **executable**
```sh
sudo chmod +x /usr/local/bin/docker-compose
```

--------------------------------------------------------------------------------

Commands
--------

### Prune Network
```sh
docker network prune
```

### Download/Copy file from container
```sh
docker cp [OPTIONS] CONTAINER:SRC_PATH DEST_PATH|-
```
#### Example:
```sh
docker cp  b8ddea5d8fad:/mnt/mydir/ ./localDir
```

### Upload/Copy file to container
Folder:
```sh
docker cp [OPTIONS] SRC_PATH|- CONTAINER:DEST_PATH
```
#### Example:
```sh
docker cp ./localDir/dirToCopy b8ddea5d8fad:/mnt/newDir/
```
