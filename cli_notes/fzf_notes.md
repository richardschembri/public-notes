fzf notes
=========

General Shortcuts
-----------------
| Function                      | Shortcut                  |
| :---------------------------- | :------------------------ |
| **Scroll preview**            | `<Shift>` + `<Arrow Key>` |
