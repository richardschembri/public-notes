Redmine Notes
=============

Setup
-----
### [Docker Containers](https://github.com/sameersbn/docker-redmine/)
> sameersbn/redmine

#### Migration

##### [Create Backup](https://github.com/sameersbn/docker-redmine#creating-backups)
> Use `app:backup:create` argument

1. Stop and remove the running instance
```sh
docker stop redmine && docker rm redmine
```
2. Relaunch the container with `app:backup:create` argument
```sh
docker run --name redmine -it --rm [OPTIONS] \
  sameersbn/redmine:4.2.2-1 app:backup:create
```

##### [Restore Backup](https://github.com/sameersbn/docker-redmine#restoring-backups)
> Use `app:backup:restore` argument

1. Stop and remove the running instance
```sh
docker stop redmine && docker rm redmine
```
1. Relaunch the container with `app:backup:restore` argument

--------------------------------------------------------------------------------

File Paths
----------
### `redmine_redmine` container
- **Git:**          `/var/opt/redmine/repos/git`
- **Attachments:**  `/home/redmine/data/files`

--------------------------------------------------------------------------------

Migration
---------

### MySQL Backup
#### 1. Access MySQL docker
```sh
docker exec -it <db container id> bash
mysqldump -umyuser -pmypass mydb > /path/to/backup_mysql.dump
```

#### 2. Download/from container
```sh
docker cp  <db container id>:/path/to/backup_mysql.dump ./backup_mysql.dump
```

#### 3. Download from server
```sh
scp root@<server ip>:/path/to/backup_mysql.dump /path/to/backup_mysql.dump
```

#### 4. Copy to new redmine mysql db container
```sh
docker cp ./backup_mysql.dump <db container id>:/path/to/backup_mysql.dump
```

#### 5. Restore database in container
```sh
docker exec -it <db container id> bash
mysql -umyuser -pmypass db_redmine < /path/to/backup_mysql.dump
```

### Errors
#### [Template::Error (undefined method `description' for #<Tracker](https://www.redmine.org/boards/2/topics/61810)
> This error occurs when importing MySQL data from an older version of redmine to a new one
```sh
sudo -i -u redmine
cd <redmine dir>
RAILS_ENV=production bundle exec rake db:migrate
exit
sudo apache2ctl restart
```
