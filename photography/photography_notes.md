Photography Notes
=================

Manual Mode
-----------

### Aperture
- Smaller = More light exposure
- Smaller = More depth of field (blurry background)

### Shutter speed
- Slower = Brighter
- Faster = Less motion blur

### ISO
- Higher is brighter but noisier

### [White Balance](https://youtu.be/_VxvCck6dPU)
- Adjust according to lighting conditions
- Calculated in Kalvin
- If you have too much Kalvin the image will get warmer

Links
-----
- [Manual Mode](https://www.youtube.com/watch?v=Z6J5nN45cKk)
- [Manual Focus](https://www.youtube.com/watch?v=fiL7qlOi09I)
