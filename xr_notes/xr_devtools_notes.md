XR Dev Tools
============

[Virtual Reality Peripheral Network (VRPN)](https://github.com/vrpn/vrpn)
-------------------------------------------

A set of classes within a library and a set of servers that are designed to implement
a network-transparent interface between application programs and the set of physical
devices (tracker, etc.) used in a virtual-reality (VR) system.

https://docs.unity3d.com/ScriptReference/ClusterInput.html
https://docs.unity3d.com/ScriptReference/ClusterNetwork.html
