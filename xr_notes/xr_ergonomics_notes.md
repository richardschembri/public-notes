XR Ergonomics
=============
Comfortable Angles
------------------

### Vertical view angles:

| Comfort Max | Max  |
| :---------: | :--- |
| 20°         | 60°  |
| -12°        | -40° |

### Long-term content viewing angles:
> Long-term content viewing angles as illustrated by Ankrum D.R. (1999)
-15° - -50
