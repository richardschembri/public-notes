MySQL Notes
===========

`mysqld`
------
### Stop `mysqld`
https://stackoverflow.com/questions/11091414/how-to-stop-mysqld

### Backup / Migration
- There is no space after `-u`(username) and `-p`(password)
- If you use `-p` without writing out the password, you will be prompted to write it
#### Backup to `dump`
```sh
mysqldump -umyuser -pmypass mydb > ~/backup_mysql.dump
```
#### Restore from `dump`
```sh
mysql -umyuser -pmypass mydb < backup_mysql.dump
```
